//--------------VALIDATION FORM----------------
$("#formpassword").validate({
    rules:{
        username:{
            minlength:4,
        },
        password:{
            minlength:4,
        },
        ulangipassword:{
            minlength:4,
            equalTo: "#password",
        }
    },
    messages:{
        username:{
            minlength:"minimal 4 karakter",
        },
    }, 
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error').removeClass('success');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error').addClass('success');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    }        
});
//---------------VALIDATION ABDIMAS------------
$("#formabdimas").validate({
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    } 
});
//---------------VALIDATION PENELITIAN------------
$("#formpenelitian").validate({
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    }     
});
//---------------VALIDATION SEMINAR------------
$("#formseminar").validate({
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    }     
});
//---------------VALIDATION JURNAL------------
$("#formjurnal").validate({
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    }     
});
//---------------VALIDATION MENGAJAR------------
$("#formmengajar").validate({
     highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    }   
});
//---------------VALIDATION PENULISAN BUKU------------
$("#formpenulisanbuku").validate({
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    }     
});
//---------------VALIDATION HAKI------------
$("#formhaki").validate({
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    }    
});
//---------------VALIDATION KEANGGOTAAN ORGANISASI------------
$("#formorganisasi").validate({
     highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    }    
});
//---------------VALIDATION PENUNJANG------------
$("#formpenunjang").validate({
     highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    }    
});
