-- Adminer 4.6.3 PostgreSQL dump

DROP TABLE IF EXISTS "db_aturpensiun";
DROP SEQUENCE IF EXISTS db_aturpensiun_id_seq;
CREATE SEQUENCE db_aturpensiun_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."db_aturpensiun" (
    "id" integer DEFAULT nextval('db_aturpensiun_id_seq') NOT NULL,
    "nama" character(50),
    "tahun" character(5),
    CONSTRAINT "pk_db_aturpensiun" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "db_aturpensiun" ("id", "nama", "tahun") VALUES
(1,	'Pensiun Cepat                                     ',	'40   '),
(2,	'Pensiun Normal                                    ',	'50   '),
(3,	'Pensiun Terlambat                                 ',	'62   ');

-- 2019-02-18 15:34:35.537002+07
