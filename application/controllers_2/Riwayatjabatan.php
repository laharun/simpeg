<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatjabatan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent:: __construct();
		$this->load->model(array('mriwayatjabatan','Crud'));
		if(!$this->session->userdata('login')){
			redirect(site_url('Login'));
		}
	}

	//DEKLARASI VAR
	private $msg_simpansuccess="Data berhasil disimpan";
	private $msg_hapussuccess="Data berhasil dihapus";
	private $path='./berkas/jabatan/';		
	
	public	function fileupload($path,$file){
		$config=array(
			'upload_path'=>$path,
			'allowed_types'=>'pdf',
			'max_size'=>5000,
			'encrypt_name'=>true,
		);
		$this->load->library('upload',$config);
		return $this->upload->do_upload($file);
	}
	public	function downloadfile($path,$namafile){
		$url=file_get_contents($path.$namafile);
		return force_download($namafile,$url);
	}	
	public function index()
	{
		redirect(site_url('Jafa/simpan_data'));
	}
	public function simpan_data(){
		$path=$this->path;$file='fileupload';
		if($this->fileupload($path,$file)){
			$fileupload=$this->upload->data();
			$idpeg=$this->session->userdata('id_pegawai');
			$data=array(
				'idpeg'=>$idpeg,
				'namagol'=>$this->input->post('golongan'),
				'nomorsk'=>$this->input->post('nomorsk'),
				'tmt'=>$this->input->post('tmt'),
				'file'=>$fileupload['file_name'],
			);
			//print_r($data);
			$query=$this->mriwayatjabatan->simpan_riwayatjabatan($data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_simpansuccess);
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}			
		}else{
			$this->session->set_flashdata('error',$this->upload->display_errors());
			//echo $this->upload->display_errors();
		}		
		redirect(site_url('User/riwayatjabatan'));
	}
	public function hapus_data($id){
		$file=array(
			'select'=>'file',
			'tabel'=>'db_riwayatjabatan',
			'where'=>array('id'=>$id),
		);
		$file=$this->Crud->read($file)->row();		
		$query=$this->mriwayatjabatan->hapus_riwayatjabatan($id);
		if($query){
			unlink($this->path.trim($file->file));
			$this->session->set_flashdata('success',$this->msg_hapussuccess);
		}else{
			$error=$this->db->error();
			$this->session->set_flashdata('error',$error['message']);
		}
		redirect(site_url('User/riwayatjabatan'));			
	}
	public function download($namafile){
		$path=$this->path;
		$this->downloadfile($path,$namafile);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */