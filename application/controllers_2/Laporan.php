<?php
	Class Laporan extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->idpegawai=$this->session->userdata('id_pegawai');
			$this->load->model(array('mseminarpenelitian','mtahunakad','Crud'));
			if($this->session->userdata('login')!=true){
				redirect(site_url('Login/logout'));
			}				
		}
		//DEKLARASI VAR
		//private $master_tabel='serdos';
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $default_url='Laporan';

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000, //5mb
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}			
		function index(){		
			redirect(site_url($this->default_url.'/serkom'));
		}
		public function serkom(){
			$query=array(
				'select'=>'b.idpeg,b.gelardepan,b.nama,b.gelarbelakang,count(a.serkom_id) AS jumlah',
				'tabel'=>'serkom a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.serkom_idpegawai=b.idpeg','jenis'=>'RIGHT')),
				'where'=>array(array('b.kategori'=>'pendidik')),
				'groupby'=>array('b.nama','b.idpeg','b.gelardepan','b.gelarbelakang'),
				'orderby'=>array('kolom'=>'jumlah','orderby'=>'DESC'),				
			);				
			$data=array(
				'menu'=>'laporan',
				'submenu'=>'laporanserkom',
				'headline'=>'laporanserkom',
				'judul'=>'laporan serkom',
				'dosen'=>$this->Crud->join($query)->result(),
			);
			$this->load->view('administrator',$data);				
			//print_r($data['dosen']);			
		}
		/*
		private function get_file($id){
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array('serdos_id'=>$id)),
			);
			$read=$this->Crud->read($query)->row();
			if($read->serdos_file){
				unlink($this->path.$read->serdos_file);
			}
		}
		function hapus($id){
			$this->get_file($id);
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array('serdos_id'=>$id),
			);
			$delete=$this->Crud->delete($query);
			if($delete){
				$this->session->set_flashdata('success','Hapus berhasil');
			}else{
				$mssg='Hapus error, msg : '.$delete;
				$this->session->set_flashdata('error',$msg);
			}
			redirect(site_url($this->default_url));		
		}
		function edit(){
			$id=$this->input->post('id');
			$query=array(
				'tabel'=>'serdos',
				'where'=>array(array('serdos_id'=>$id)),
			);
			$pegawai=array(
				'tabel'=>'db_pegawai',
				'orderby'=>array('kolom'=>'nama','orderby'=>'ASC'),
			);				
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'pegawai'=>$this->Crud->read($pegawai)->result(),
				'seminar'=>$this->mseminarpenelitian->get_seminar_byid($id)->row(),
				'data'=>$this->Crud->read($query)->row(),
			);
			//print_r($data['seminar']);
			$this->load->view('serdosadmin/form_edit',$data);			
		}
		function update(){
			$id=$this->input->post('id');
			$this->form_validation->set_rules('serdos_idpegawai','idpegawai','required');
			if($this->form_validation->run()==true){
				//$path=$this->path;$file='fileupload';
				$data=array(
					'serdos_idpegawai'=>$this->input->post('serdos_idpegawai'),
					'serdos_tahundiperoleh'=>$this->input->post('serdos_tahundiperoleh'),
				);				
				$file='fileupload';		
				if($_FILES[$file]['name']){
					if($this->fileupload($this->path,$file)){
						$uploaddata=$this->upload->data();
						$data['serdos_file']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error',$dt['error']);
						redirect(site_url($this->default_url));
					}
				}	
				$query=array(
					'data'=>$data,
					'tabel'=>$this->master_tabel,
					'where'=>array('serdos_id'=>$id),
				);				
				$insert=$this->Crud->update($query);
				if($insert){
					$this->session->set_flashdata('success','Update berhasil');
				}else{
					$msg='Update gagal, msg :'.$insert;
					$this->session->set_flashdata('error',$msg);
				}
				//print_r($data);
				redirect(site_url($this->default_url));
			}		
		}
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan,silahkan upload ulang');
				redirect(site_url('User/seminarpemakalah'));	
			}						
		}		
		*/
	}
?>