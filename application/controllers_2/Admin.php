<?php
	Class Admin extends CI_Controller{
		function __construct(){
			parent:: __construct();
			$this->load->model(array('mpegawai','crud'));
			if($this->session->userdata('login')!=true){
				redirect(site_url('login/logout'));
			}
		}

		private $pathskmasuk="./berkas/skmasuk/";
		private $pathsktetap="./berkas/sktetap/";
		private $pathkk="./berkas/kk/";
		private $pathktp="./berkas/ktp/";
		private $pathfoto="./berkas/filefoto/";

		##################### ADTIONAL FUNCTION #########################
		private function matauang($value){
			$var=str_replace('Rp ','', $value);
			$var=str_replace('.', '', $var);
			return $var;
		}
		private function fileupload($path,$var){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000, //5mb
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config,$var);
			return $this->$var->do_upload($var);
		}
		private function imageupload($path,$var){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'jpg|jpeg|png',
				'max_size'=>5000, //5mb
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config,$var);
			return $this->$var->do_upload($var);
		}			
		private function downloadfile($path,$namafile){
			$konversispasi=str_replace('%20',' ', $namafile); 
			$url=file_get_contents($path.$konversispasi);
			//MERUBAH SPASI(%20) menjadi ''
			$download=force_download($konversispasi,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan'); 
				redirect(site_url('User/'));				
			}
		
		}		
		//-------------------------------------VARIABEL---------------
		function variabel(){
			$data=array(
				'idpeg'=>$this->session->userdata('id_pegawai'),
				);
			return $data;
		}		
		function index(){
			$data=array(
				'menu'=>'dashboard',
				'headline'=>"Dashboard",
				'jumpegawai'=>$this->mpegawai->get_pegawai_dashboard()->num_rows(),
				'jumtendik'=>$this->mpegawai->get_pegawai_bykategori('kependidikan')->num_rows(),
				'jumpendidik'=>$this->mpegawai->get_pegawai_bykategori('pendidik')->num_rows(),
				'jumpegawaibyhomebase'=>$this->mpegawai->jum_pegawai_byhomebase()->result(),
				'jenjangpendidikan'=>$this->mpegawai->jum_jenjangpendidikan()->result(),
			);
			$this->load->view('administrator',$data);
		}
		//-----------------------------------PEGAWAI-------------------
		function pegawai(){
			$data=array(
				'menu'=>'pegawai',
				'headline'=>"Daftar Pegawai",
				'jumpegawai'=>$this->mpegawai->get_pegawai_dashboard()->num_rows(),
				'jumtendik'=>$this->mpegawai->get_pegawai_bykategori('kependidikan')->num_rows(),
				'jumpendidik'=>$this->mpegawai->get_pegawai_bykategori('pendidik')->num_rows(),				
				'pegawai'=>$this->mpegawai->get_pegawai_dashboard()->result(),
			);
			$this->load->view('administrator',$data);			
		} 
		function add_homebase(){
			$idpeg=$this->input->post('idpeg');
			$data=array(
				'pegawai'=>$this->mpegawai->get_detail_pegawai($idpeg)->row(),
				'homebase'=>$this->mpegawai->get_homebase()->result(),
			);
			$this->load->view('administrator/ajaxhomebase',$data);
		}
		function edit_pegawai($id){
			// add breadcrumbs
			$this->breadcrumbs->push('Pegawai', 'Admin/pegawai');
			$this->breadcrumbs->push('Edit Pegawai', 'Admin/edit_pegawai');		
			$data=array(
				'breadcrumbs'=>$this->breadcrumbs->show(),
				'menu'=>"edit_pegawai",
				'headline'=>"Edit Pegawai",
				'pegawai'=>$this->mpegawai->edit_pegawai($id)->row(),
				'department'=>$this->mpegawai->get_departmen()->result(),
				'golongan'=>$this->mpegawai->get_golongan()->result(),
				);
			//print_r($data['pegawai']);
			$this->load->view('administrator',$data);
		}
		function update_pegawai(){
			$this->form_validation->set_rules('idpeg','idpegawai','required');
			if($this->form_validation->run()==true){
				$id=$this->input->post('idpeg');
				$gajiterakhir=$this->matauang($this->input->post('gajiterakhir'));				
				//$path=$this->path;$file='fileupload';
				$data=array(
					'noskmasuk'=>$this->input->post('noskmasuk'),
					'nosktetap'=>$this->input->post('nosktetap'),
					'nama'=>$this->input->post('nama'),
					'kategori'=>$this->input->post('kategori'),
					'idstatuspeg'=>$this->input->post('status'),
					'idunitkerja'=>$this->input->post('penempatan'),
					'idgolongan'=>$this->input->post('golongan'),
					'jabatan'=>$this->input->post('jabatan'),
					'awalmasuk'=>date('Y-m-d',strtotime($this->input->post('awalmasuk'))),
					'awaltetap'=>date('Y-m-d',strtotime($this->input->post('awaltetap'))),
					'gajiawal'=>$gajiterakhir,
					'tempatlahir'=>$this->input->post('tempatlahir'),
					'tanggallahir'=>date('Y-m-d',strtotime($this->input->post('tanggallahir'))),
					'idjeniskelamin'=>$this->input->post('jeniskelamin'),
					'idagama'=>$this->input->post('agama'),
					'statusnikah'=>$this->input->post('statusnikah'),
					'idgoldarah'=>$this->input->post('goldarah'),
					'nohp'=>trim($this->input->post('nohp')),
					'noktp'=>$this->input->post('noktp'),
					'kodepos'=>$this->input->post('kodepos'),
					'email'=>$this->input->post('email'),
					'nokk'=>$this->input->post('nokk'),
					'nobpjs'=>$this->input->post('nobpjs'),
					'nonpwp'=>$this->input->post('npwp'),
					'alamatjalan'=>$this->input->post('alamat'),
				);				
				$file='fileksmasuk';		
				if($_FILES[$file]['name']){
					if($this->fileupload($this->pathskmasuk,$file)){
						$uploaddata=$this->$file->data();
						$data['fileskmasuk']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error','SK Masuk Msg :,'.$dt['error']);
						redirect(site_url($this->default_url));
					}
				}
				$filekstetap='filekstetap';		
				if($_FILES[$filekstetap]['name']){
					if($this->fileupload($this->pathsktetap,$filekstetap)){
						$uploaddata=$this->$filekstetap->data();
						$data['filesktetap']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error','SK Tetap Msg :,'.$dt['error']);
						redirect(site_url($this->default_url));
					}
				}
				$filekk='filekk';		
				if($_FILES[$filekk]['name']){
					if($this->fileupload($this->pathkk,$filekk)){
						$uploaddata=$this->$filekk->data();
						$data['filekk']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error','SK KK Msg :,'.$dt['error']);
						redirect(site_url($this->default_url));
					}
				}
				$filektp='filektp';		
				if($_FILES[$filektp]['name']){
					if($this->fileupload($this->pathktp,$filektp)){
						$uploaddata=$this->$filektp->data();
						$data['filektp']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error','SK KTP Msg :,'.$dt['error']);
						redirect(site_url($this->default_url));
					}
				}
				$filefoto='filefoto';		
				if($_FILES[$filefoto]['name']){
					if($this->imageupload($this->pathfoto,$filefoto)){
						$uploaddata=$this->$filefoto->data();
						$data['foto']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error','Foto Msg :,'.$dt['error']);
						redirect(site_url($this->default_url));
					}
				}

				$query=array(
					'data'=>$data,
					'tabel'=>'db_pegawai',
					'where'=>array('idpeg'=>$id),
				);				
				$update=$this->crud->update($query);
				if($update){
					$this->session->set_flashdata('success','Update berhasil');
				}else{
					$msg='update gagal, msg :'.$update;
					$this->session->set_flashdata('error',$msg);
				}
				redirect(site_url('Admin/pegawai'));
				//print_r($id);				
			}
		}
		//-----------------------------------PASSWORD-------------------
		function password_user(){
			//$this->breadcrumbs->push('Password', 'Admin/password_user');
			//$this->breadcrumbs->push('Tampil Pengguna', 'Admin/password_user');	
			$query=array(
				'select'=>'a.*,b.nama',
				'tabel'=>'db_userdosen a',
				'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'b.idpeg=a.idpeg','jenis'=>'INNER')),
				//'limit'=>10,
			);
			$user=$this->crud->join($query)->result();
			$administrasi=array();
			$dosen=array();
			foreach($user AS $index => $row){
				if($row->level=='dosen'){
					$dosen[$index]=$row;
				}else{
					$administrasi[$index]=$row;
				}
			}
			//print_r($tendik);
			$data=array(
				'breadcrumbs'=>$this->breadcrumbs->show(),
				'menu'=>'password',
				'pegawai'=>$this->mpegawai->get_data_pegawai()->result(),
				'headline'=>"Daftar Password Pengguna",		
				'dosen'=>$dosen,
				'administrasi'=>$administrasi,
			);
			$this->load->view('administrator',$data);			
			//print_r($data);
		}
		//---------------------------------LOGIN AS-------------------
		function login_as($id){
			$data=$this->mpegawai->get_password_byidpeg($id)->row();
			if(count($data)){
				$username=$data->username;
				$password=$data->password;
				redirect(site_url('Login/login_as/'.$username.'/'.$password));	
				//echo $username."<br>".$password;
			}else{
				$this->session->set_flashdata('error','Data belum diset password');
				redirect(site_url('Admin/pegawai'));
			}
		}
		//--------------------------------DOWNLOAD PANDUAN---------------
		function panduan(){
			// $path="";
			// $namafile="";
			// $url=file_get_contents($path.$namafile);
			// return force_download($namafile,$url);	
			redirect(site_url('Admin/pegawai'));		
		}
		function log(){
            //contoh panggil helper log
            helper_log("add", "menambahkan data");			
		}
	}
?>