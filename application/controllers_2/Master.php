<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {
	public function __construct(){
		//$this->load->model('Crud');
		parent::__construct();
	}

	//DEKLARASI VAR
	private $msg_simpansuccess="Data berhasil disimpan";
	private $msg_hapussuccess="Data berhasil dihapus";		

	protected function cekadmin(){
		if(!$this->session->userdata('login') && ($this->session->userdata('level')==1)){
			redirect(site_url('login/logout'));
		}		
	}
	protected function fileupload($path,$file){
		$config=array(
			'upload_path'=>$path,
			'allowed_types'=>'pdf',
			'max_size'=>5000,
			'encrypt_name'=>true,
		);
		$this->load->library('upload',$config);
		return $this->upload->do_upload($file);
	}
	protected function uploadgambar($path,$file){
		$config=array(
			'upload_path'=>$path,
			'allowed_types'=>'jpg|jpeg',
			'max_size'=>2000,
			'encrypt_name'=>true,
		);
		$this->load->library('upload',$config,$file);
		return $this->$file->do_upload($file);
	}	
	protected function dumpdata($data){
		echo "<pre>";
		print_r($data);
	}
	protected function downloadfile($path,$file){
		$link=$path.$file;
		if(file_exists($link)){
			$url=file_get_contents($link);
			force_download($file,$url);
		}else{
			$this->session->set_flashdata('error','File tidak ditemukan');
		}						
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */