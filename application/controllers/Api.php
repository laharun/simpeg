<?php
Class Api extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model(array('mapi'));
	}

	function index(){
		
	}

	function auth(){
		$sess = array();
		$username = (stripslashes(strip_tags(htmlspecialchars($this->input->post('username'), ENT_QUOTES))));
		$password = (stripslashes(strip_tags(htmlspecialchars($this->input->post('password'), ENT_QUOTES))));
		
		// cek di tabel user [DB simpeg]
		$result_login_simpeg=$this->db->query("SELECT a.username,a.password,b.idpeg,b.nama,b.foto,b.email FROM db_userdosen a JOIN db_pegawai b ON b.idpeg=a.idpeg WHERE a.username='$username' AND a.password='$password'")->row_array();
		
		if (count($result_login_simpeg) > 0) :
			$sess_array = array(
				'log_loginsys_access_identitas' => 'personal',
				'log_loginsys_access_id' => $result_login_simpeg['idpeg'],
				'log_loginsys_access_username' => $result_login_simpeg['username'],
				'log_loginsys_access_nama' => $result_login_simpeg['nama'],
				'log_loginsys_access_level' => '',
				'log_loginsys_access_relasi' => '',
				'log_loginsys_access_subunit' => 0,
			);
			array_push($sess,$sess_array);
		else:
			$data['json'] = json_encode( array('status'=>'true', 'msg'=>'Username atau sandi salah', 'data'=>array()) );
		endif;
		echo $data['json'] = json_encode( array('status'=>'true','msg'=>'Berhasil','data'=>$sess) );	
	}

}