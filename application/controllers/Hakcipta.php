<?php
	Class Hakcipta extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model(array('mhakcipta','mtahunakad'));
		}

		//DEKLARASI VAR
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $path="./berkas/haki/";
		//private $path='http://personalia.akprind.ac.id/simpeg/FilePaten/';

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}			
		function index(){
			$this->form_validation->set_rules('thnakademik','Tahun Akademik','required');
			if($this->form_validation->run()==true){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					//------AMBIL NAMA FILE UPLOAD
					$fileupload=$this->upload->data();						
					$data=array(
						'idpeg'=>$this->input->post('idpeg'),
						'thnakademik'=>$this->input->post('thnakademik'),
						'haki'=>$this->input->post('judul'),
						'tgl'=>date('Y-m-d',strtotime($this->input->post('tgl'))),
						'peran'=>$this->input->post('peran'),
						'jumtim'=>$this->input->post('jumtim'),
						'nomor'=>$this->input->post('nomor'),
						'tempat'=>$this->input->post('tempat'),
						'file'=>$fileupload['file_name'],
					);	
					//print_r($data);	
					$query=$this->mhakcipta->simpan_haki($data);
					if($query==true){
						$this->session->set_flashdata('success',$this->msg_simpansuccess);
						redirect(site_url('User/hakcipta'));
					}				
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/hakcipta'));
				}
			}			
		}
		function edit_data(){
			$id=$this->input->post('id');
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'haki'=>$this->mhakcipta->get_haki_byid($id)->row(),
			);
			//print_r($data['haki']);
			$this->load->view('hakcipta/edit',$data);			
		}
		function update(){
			$id=$this->input->post('id');
			if(!empty($_FILES['fileupload']['name'])){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					unlink($this->path.$this->input->post('filelama'));
					$file=$this->upload->data();
					$fileupload=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/hakcipta'));
				}
			}else{
				$fileupload=$this->input->post('filelama');
			}
			$data=array(
				'idpeg'=>$this->input->post('idpeg'),
				'thnakademik'=>$this->input->post('thnakademik'),
				'haki'=>$this->input->post('judul'),
				'tgl'=>date('Y-m-d',strtotime($this->input->post('tgl'))),
				'peran'=>$this->input->post('peran'),
				'jumtim'=>$this->input->post('jumtim'),
				'nomor'=>$this->input->post('nomor'),
				'tempat'=>$this->input->post('tempat'),
				'file'=>$fileupload,
			);	
			//print_r($data);
			$query=$this->mhakcipta->update_haki($id,$data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_simpansuccess);
				redirect(site_url('User/hakcipta'));	
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
				redirect(site_url('User/hakcipta'));				
			}

		}				
		function hapus($id){
			//AMBIL DATA SEMINAR BY ID SEMINAR
			$row=$this->mhakcipta->get_haki_byid($id)->row();
			//print_r($row->file);	
			
			//FILE DIHAPUS
			$query=$this->mhakcipta->hapus_haki($id);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_hapussuccess);
				unlink($this->path.$row->file);
				redirect(site_url('User/hakcipta'));				
			}else{
				$msg_error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
				redirect(site_url('User/hakcipta'));				
			}						
		}
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan');
				redirect(site_url('User/Hakcipta'));	
			}			
			// $link=$this->path.$file;
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/hakcipta"));	
			// }			
		}		
	}
?>