<?php
	Class abdimas extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model(array('mabdimas','mtahunakad','Crud'));
		}
		//DEKLARASI VARIABEL
		private $msg_success="Data Berhasil Disimpan";
		private $msg_success_hapus="Data Berhasil Dihapus";
		private $msg_success_update="Data Berhasil Diupdate";
		//var $path="../simpeg/FileAbdimas/";
		//var $path="../simpeg/FileAbdimas/";
		
		//var $path="http://php4.akprind.ac.id/simpeg/FileAbdimas/";
		private $path="./berkas/abdimas/";

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}
		function matauang($value){
	        $var=str_replace('Rp ', '', $value);
	        $var2=str_replace('.','', $var);
	        return $var2;			
		}
		function index(){
			//SIMPAN CONTROLLER
			$this->form_validation->set_rules('thnakademik','Tahun Akademik','required');
			if($this->form_validation->run()==true){
				//$path='./uploadfile/';$file='fileupload';
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					$file=$this->upload->data('file_name');
					$data=array(
						'thnakademik'=>$this->input->post('thnakademik'),
						'idpeg'=>$this->input->post('idpeg'),
						'lokasi'=>$this->input->post('lokasi'),
						'asaldana'=>$this->input->post('asaldana'),
						'biaya'=>$this->matauang($this->input->post('biaya')),
						'tema'=>$this->input->post('tema'),
						'tgl'=>date('Y-m-d',strtotime($this->input->post('tglmulai'))),
						'tglselesai'=>date('Y-m-d',strtotime($this->input->post('tglselesai'))),
						'file'=>$file['file_name'],
						'abstrak'=>$this->input->post('abstrak'),
					);
					$query=$this->mabdimas->simpan_abdimas($data);
					if($query==true){
						$this->session->set_flashdata('success',$this->msg_success);
						redirect(site_url('User/abdimas'));						
					}else{
					$msg=$this->db->error();
						$this->session->set_flashdata('error',$msg['message']);
						redirect(site_url('User/abdimas'));						
					}
					//print_r($data);
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/abdimas'));
				}			
			}
		}
		function hapus($id){
			$q=array(
				'select'=>'file',
				'tabel'=>'db_abdimas',
				'where'=>array(array('id'=>$id)),
			);
			$w=$this->Crud->read($q)->row();
			unlink($this->path.$w->file);
			$query=$this->mabdimas->hapus_abdimas($id);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_success_hapus);
				redirect(site_url('User/abdimas'));						
			}else{
				$msg=$this->db->error();
				$this->session->set_flashdata('error',$msg['message']);
				redirect(site_url('User/abdimas'));						
			}
		}
		function edit_data(){
			$id=$this->input->post('id');
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'abdimas'=>$this->mabdimas->get_abdimas_byid($id)->row(),
			);
			$this->load->view('abdimas/edit_data',$data);
		}
		function update_data(){
			$id=$this->input->post('id');
			if(!empty($_FILES['fileupload']['name'])){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					$file=$this->upload->data();
					$fileupload=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/abdimas'));
				}
			}else{
				$fileupload=$this->input->post('filelama');
			}
			$data=array(
				'thnakademik'=>$this->input->post('thnakademik'),
				'idpeg'=>$this->input->post('idpeg'),
				'lokasi'=>$this->input->post('lokasi'),
				'tema'=>$this->input->post('tema'),
				'asaldana'=>$this->input->post('asaldana'),
				'biaya'=>$this->matauang($this->input->post('biaya')),				
				'tgl'=>date('Y-m-d',strtotime($this->input->post('tglmulai'))),
				'tglselesai'=>date('Y-m-d',strtotime($this->input->post('tglselesai'))),
				'file'=>$fileupload,
				'abstrak'=>$this->input->post('abstrak'),
			);
			//print_r($data);
			//echo $id;
			$query=$this->mabdimas->update_data($id,$data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_success_update);
				//redirect(site_url('User/abdimas'));	
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
							
			}
			redirect(site_url('User/abdimas'));	
		}
		function detail(){
			$id=$this->input->post('id');
			$query=array(
				'where'=>array('id'=>$id),
				'tabel'=>'db_abdimas',
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				);
			$this->load->view('abdimas/detail',$data);
		}
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan');
				redirect(site_url('User/abdimas'));	
			}			
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/abdimas"));	
			// }						
		}
	}
?>