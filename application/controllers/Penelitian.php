<?php
	Class Penelitian extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model(array('mpenelitian','mtahunakad','Crud'));
		}
		//DEKLARASI VAR
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $path='./berkas/penelitian/';
		//var $path="http://php4.akprind.ac.id/simpeg/FilePenelitian/";

		//var $path="./uploadfile";

		function konvert($val){
			$var=str_replace('Rp ','',$val);
			$var=str_replace('.','', $var);
			return $var;
		}
		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}		
		function index(){
			$this->form_validation->set_rules('thnakademik','Tahun Akademik','required');
			if($this->form_validation->run()==true){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					$fileupload=$this->upload->data();
					$data=array(
						'tahunakademik'=>$this->input->post('thnakademik'),
						'judulpenelitian'=>$this->input->post('judul'),
						'asaldana'=>$this->input->post('asaldana'),
						'dana'=>$this->konvert($this->input->post('besaranggaran')),
						'idpeg'=>$this->input->post('idpeg'),
						'tim'=>$this->input->post('jumlahtim'),
						'skema'=>$this->input->post('skema'),
						'peran'=>$this->input->post('peran'),
						'keterangan'=>$this->input->post('keterangan'),
						'file'=>$fileupload['file_name'],
						'save_date'=>date('Y-m-d'),
						'abstrak'=>$this->input->post('abstrak'),
						'abstrak'=>$this->input->post('abstrak'),
					);	
					$query=$this->mpenelitian->simpan_penelitian($data);
					if($query==true){
						$this->session->set_flashdata('success',$this->msg_simpansuccess);
						redirect(site_url('User/penelitian'));
					}				
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/penelitian'));
				}
			}
		}
		function hapus($id){
			$row=$this->mpenelitian->get_penelitian_byid($id)->row();
			//$res=unlink('../simpeg/FilePenelitian/'.$row->file);
			$query=$this->mpenelitian->hapus_penelitian($id);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_hapussuccess);
				unlink($this->path.$row->file);
				//redirect(site_url('User/penelitian'));				
			}else{
				$msg_error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
								
			}	
			redirect(site_url('User/penelitian'));			
			//print_r($row->file);			
		}
		function edit_data(){
			$id=$this->input->post('id');
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'penelitian'=>$this->mpenelitian->get_penelitian_byid($id)->row(),
			);
			$this->load->view('penelitian/edit',$data);
			//print_r($data['thnakademik']);
		}
		function update_data(){
			$id=$this->input->post('id');
			if(empty($_FILES['fileupload']['name'])){
				$file=$this->input->post('filelama');
			}else{
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					unlink($path.$this->input->post('filelama'));
					$fileupload=$this->upload->data();
					$file=$fileupload['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/penelitian'));
				}				
			}
			$data=array(
				'tahunakademik'=>$this->input->post('thnakademik'),
				'judulpenelitian'=>$this->input->post('judul'),
				'asaldana'=>$this->input->post('asaldana'),
				'dana'=>$this->konvert($this->input->post('besaranggaran')),
				'idpeg'=>$this->input->post('idpeg'),
				'tim'=>$this->input->post('jumlahtim'),
				'skema'=>$this->input->post('skema'),
				'peran'=>$this->input->post('peran'),
				'keterangan'=>$this->input->post('keterangan'),
				'file'=>$file,
				'abstrak'=>$this->input->post('abstrak'),
				'tempat'=>$this->input->post('tempat'),
			);	
			//print_r($data);
			$query=$this->mpenelitian->update_penelitian($id,$data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_simpansuccess);
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);	
			}
			redirect(site_url('User/penelitian'));			
		}
		function detail(){
			$id=$this->input->post('id');
			$query=array(
				'where'=>array('id'=>$id),
				'tabel'=>'db_penelitian',
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
			);
			//print_r($data['data']);
			$this->load->view('penelitian/detail',$data);
		}		
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan');
				redirect(site_url('User/penelitian'));	
			}				
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/penelitian"));
			// }							
		}		
	}
?>