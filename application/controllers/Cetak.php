<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent:: __construct();
		$this->load->model(array('Crud','mpegawai','mkeluarga','mriwayatpendidikan'));
		if($this->session->userdata('login')!=true){
			redirect(site_url('Login/logout'));
		}
	}
	
	public function prosescetak($data){
		$nama_dokumen=$data['judul']; //Beri nama file PDF hasil.
		require_once('./assets/third_party/mPDF/mpdf.php');
		$mpdf= new mPDF('c','A4-Pa','',0,20,20,20,20);	
		// $mpdf->SetHTMLHeader('
		// <div style="text-align: left; font-weight: bold;">
		//     <img src="./asset/dist/img/avatar6.png" width="60px" height="60px">'.$nama_dokumen.'
		// </div>');
		$mpdf->SetHTMLFooter('
		<table width="100%">
		    <tr>
		        <td width="33%">Cetak: {DATE j-m-Y}</td>
		        <td width="33%" align="center">{PAGENO}/{nbpg}</td>
		        <td width="33%" style="text-align: right;">'.$nama_dokumen.'</td>
		    </tr>
		</table>');		
		$mpdf->WriteHTML($data['view']);
		$mpdf->Output($nama_dokumen.".pdf",'I');		
	}			
	function index(){
		$var=$this->session->userdata('id_pegawai');
		$abdimas=array(
			'tabel'=>'db_abdimas',
			'where'=>array('idpeg'=>$var),
		);
		$seminar=array(
			'tabel'=>'db_seminar',
			'where'=>array('idpeg'=>$var),
		);	
		$penelitian=array(
			'tabel'=>'db_penelitian',
			'where'=>array('idpeg'=>$var),
		);				
		$userdata=array(
			'pegawai'=>$this->mpegawai->get_data_pegawai_byid($var)->row(),
			'pendidikan'=>$this->mriwayatpendidikan->get_pendidikan_byidpeg($var)->result(),
			'abdimas'=>$this->Crud->read($abdimas)->result(),
			'seminar'=>$this->Crud->read($seminar)->result(),
			'penelitian'=>$this->Crud->read($penelitian)->result(),
			);			
		$view=$this->load->view('cetak/user',$userdata,true);
		$data=array(
			'judul'=>'Data diri',
			'view'=>$view,
		);
		$this->prosescetak($data);
	}	
}