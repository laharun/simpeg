<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Registrasi extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model(array('mjafa','Crud'));
		if(!$this->session->userdata('login') && ($this->session->userdata('level')==1)){
			redirect(site_url('login/logout'));
		}	
	}

	//DEKLARASI VAR
	private $msg_simpansuccess="Proses Berhasil";
	private $msg_hapussuccess="Proses gagal";		
	private $path_foto='./berkas/fileFoto';
	private $path_ktp='./berkas/ktp';
	private $default_url='Registrasi/';

	private function notifikasi($query){
		if($query){
			$this->session->set_flashdata('success','Proses Berhasil');
		}else{
			$error=$this->db->error();
			$this->session->set_flashdata('error',$error['message']);
		}		
	}
	private	function fileupload($path,$file){
		$config=array(
			'upload_path'=>$path,
			'allowed_types'=>'pdf',
			'max_size'=>5000,
			'encrypt_name'=>true,
		);
		$this->load->library('upload',$config);
		return $this->upload->do_upload($file);
	}
	public	function uploadgambar($path,$file){
		$config=array(
			'upload_path'=>$path,
			'allowed_types'=>'jpg|jpeg',
			'max_size'=>2000,
			'encrypt_name'=>true,
		);
		$this->load->library('upload',$config,$file);
		return $this->$file->do_upload($file);
	}	
	private function dumpdata($data){
		echo "<pre>";
		print_r($data);
	}
	public	function downloadfile($path,$namafile){
		$url=file_get_contents($path.$namafile);
		return force_download($namafile,$url);
	}	
	public function index()
	{
		if($this->input->post('submit')){
			$file_ktp='pegawai_ktp';
			$file_foto='pegawai_foto';

			$data=array(
				'nama'=>$this->input->post('pegawai_nama'),
				'kategori'=>$this->input->post('pegawai_kategori'),
				'statuspegawai'=>'Aktif',
				'idagama'=>$this->input->post('pegawai_agama'),
				'tempatlahir'=>$this->input->post('pegawai_tempatlahir'),
				'idjeniskelamin'=>$this->input->post('pegawai_jeniskelamin'),
				'tanggallahir'=>date('Y-m-d',strtotime($this->input->post('pegawai_tgllahir'))),
			);
			if($this->uploadgambar($this->path_ktp,$file_ktp)){
				$ktp=$this->$file_ktp->data();
				$data['filektp']=$ktp['file_name'];
			}
			if($this->uploadgambar($this->path_foto,$file_foto)){
				$foto=$this->$file_foto->data();
				$data['foto']=$foto['file_name'];
			}
			if($data['kategori']=='kependidikan'){
				$level='administrasi';
			}else{
				$level='dosen';
			}
			$pegawai=array(
				'tabel'=>'db_pegawai',
				'data'=>$data,
				);			
			$idpeg=$this->Crud->insert_returnid($pegawai);
			$data2=array(
				'idpeg'=>$idpeg,
				'username'=>$this->input->post('pegawai_username'),
				'password'=>$this->input->post('pegawai_password'),
				'level'=>$level,
			);
			$user=array(
				'tabel'=>'db_userdosen',
				'data'=>$data2,
			);
			$user=$this->Crud->insert($user);
			$this->notifikasi($user);
			redirect(site_url($this->default_url));
			//$this->dumpdata($data);
		}else{
			$query=array(
				'tabel'=>'db_pegawai',
				'select'=>'*',
				'order'=>array('kolom'=>'nama','orderby'=>'ASC'),
				'limit'=>5,
			);
			//$pegawai=$this->Crud->read($query)->result();
			$data=array(
				'menu'=>'personalia',
				'submenu'=>'registrasi',
				'headline'=>'Pendaftaran Pegawai',	
				'url'=>$this->default_url,		
			);
			$this->load->view('administrator',$data);
			//$this->dumpdata($pegawai);			
		}

	}
	public function simpan_data(){
		$path=$this->path;
		$file='fileupload';
		if($this->fileupload($path,$file)){
			$fileupload=$this->upload->data();
			$idpeg=$this->session->userdata('id_pegawai');
			$data=array(
				'idpeg'=>$idpeg,
				'namajafa'=>$this->input->post('jafa'),
				'angkakredit'=>$this->input->post('angka_kredit'),
				'tmt'=>$this->input->post('tmt'),
				'file'=>$fileupload['file_name'],
			);
			//print_r($data);
			$query=$this->mjafa->simpan_jafa($data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_simpansuccess);
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}			
		}else{
			$this->session->set_flashdata('error',$this->upload->display_errors());
			//echo $this->upload->display_errors();
		}		
		redirect(site_url('User/jabatanfungsional'));
	}
	public function hapus_data($id){
		$file=array(
			'select'=>'file',
			'tabel'=>'db_jafa',
			'where'=>array('id'=>$id),
		);
		$file=$this->Crud->read($file)->row();
		$query=$this->mjafa->hapus_jafa($id);
		if($query){
			unlink($this->path.trim($file->file));
			$this->session->set_flashdata('success',$this->msg_hapussuccess);
		}else{
			$error=$this->db->error();
			$this->session->set_flashdata('error',$error['message']);
		}
		redirect(site_url('User/jabatanfungsional'));			
	}
	public function download($namafile){
		$path=$this->path;
		$this->downloadfile($path,$namafile);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */