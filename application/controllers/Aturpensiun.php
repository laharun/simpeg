<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Aturpensiun extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model(array('mjafa','Crud'));
		if(!$this->session->userdata('login') && ($this->session->userdata('level')==1)){
			redirect(site_url('login/logout'));
		}	
	}

	//DEKLARASI VAR
	private $msg_simpansuccess="Proses Berhasil";
	private $msg_hapussuccess="Proses gagal";		
	private $path_foto='./berkas/fileFoto';
	private $path_ktp='./berkas/ktp';
	private $default_url='Aturpensiun/';
	private function notifikasi($query){
		if($query){
			$this->session->set_flashdata('success','Proses Berhasil');
		}else{
			$error=$this->db->error();
			$this->session->set_flashdata('error',$error['message']);
		}		
	}
	public function index()
	{
		$this->form_validation->set_rules('nama','tahun','jenis','required');
			if($this->form_validation->run()==true){

			$data=array(
				'nama'=>$this->input->post('nama'),
				'tahun'=>$this->input->post('tahun'),
				'jenis'=>$this->input->post('jenis')
			);
			$pensiun=array(
				'tabel'=>'db_aturpensiun',
				'data'=>$data,
			);
			$res=$this->Crud->insert($pensiun);
			$this->notifikasi($res);
			redirect(site_url($this->default_url));
		}else{
			$query=array(
				'tabel'=>'db_aturpensiun',
				'select'=>'*',
				'order'=>array('kolom'=>'id','orderby'=>'ASC'),
			);
			$data=array(
				'menu'=>'personalia',
				'submenu'=>'aturpensiun',
				'headline'=>'Atur pensiun',	
				'url'=>$this->default_url,
				'data'=>$this->Crud->read($query)->result(),
			);
			$this->load->view('administrator',$data);
		}

	}
	public function update(){
			$id=$this->input->post('id');
			$data=array(
				'id'=>$id,
				'nama'=>$this->input->post('nama'),
				'tahun'=>$this->input->post('tahun'),
				'jenis'=>$this->input->post('jenis')
			);
			$query=array(
					'data'=>$data,
					'tabel'=>'db_aturpensiun',
					'where'=>array('id'=>$id),
				);				
			$insert=$this->Crud->update($query);
			if($insert){
				$this->session->set_flashdata('success','Update berhasil');
			}else{
				$msg='Update gagal, msg :'.$insert;
				$this->session->set_flashdata('error',$msg);
			}		
			redirect(site_url($this->default_url));
	}
	public function hapus($id){
		$query=array(
				'tabel'=>'db_aturpensiun',
				'where'=>array('id'=>$id),
			);
			$delete=$this->Crud->delete($query);
			if($delete){
				$this->session->set_flashdata('success','Hapus berhasil');
			}else{
				$mssg='Hapus error, msg : '.$delete;
				$this->session->set_flashdata('error',$msg);
			}
			redirect(site_url($this->default_url));		
		 	
	}
	function edit(){
			$id=$this->input->post('id');
			$query=array(
				'tabel'=>'db_aturpensiun',
				'where'=>array(array('id'=>$id)),
			);
 			
			$data=array(
				'data'=>$this->Crud->read($query)->row()
			);
			$this->load->view('administrator/aturpensiun/form_edit',$data);			
		}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */