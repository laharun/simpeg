<?php
	Class Jurnal extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model(array('mjurnal','mtahunakad','Crud'));
		}
		
		//DEKLARASI VAR
		var $msg_simpansuccess="Data berhasil disimpan";
		var $msg_hapussuccess="Data berhasil dihapus";
		//var $path='php4.akprind.ac.id/simpeg/FileJurnal/';
		var $path='./berkas/jurnal/';

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}		
		function index(){
			$this->form_validation->set_rules('thnakademik','Tahun Akademik','required');
			if($this->form_validation->run()==true){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					//------AMBIL NAMA FILE UPLOAD
					$fileupload=$this->upload->data();
					$data=array(
						'idpeg'=>$this->input->post('idpeg'),
						'thakademik'=>$this->input->post('thnakademik'),
						'namajurnal'=>$this->input->post('jurnal'),
						'lembaga'=>$this->input->post('lembaga'),
						'issn'=>$this->input->post('issn'),
						'tglterbit'=>date('Y-m-d',strtotime($this->input->post('tglterbit'))),
						'judmakalah'=>$this->input->post('makalah'),
						'jmltim'=>$this->input->post('jmltim'),
						'peran'=>$this->input->post('peran'),
						'keterangan'=>$this->input->post('keterangan'),
						'file'=>$fileupload['file_name'],
						'jenisjurnal'=>$this->input->post('jenisjurnal'),
						'save_date'=>date('Y-m-d'),
						'status'=>$this->input->post('status'),
					);	
					//print_r($data);	
					$query=$this->mjurnal->simpan_jurnal($data);
					if($query==true){
						$this->session->set_flashdata('success',$this->msg_simpansuccess);
						redirect(site_url('User/jurnal'));
					}									
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/jurnal'));
				}

			}
		}
		function edit_data(){
			$id=$this->input->post('id');
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'jurnal'=>$this->mjurnal->get_jurnal_byid($id)->row(),
			);
			//print_r($data['jurnal']);
			$this->load->view('jurnal/edit',$data);			
		}
		// function update(){
		// 	if(empty($_FILES['fileupload']['name'])){
		// 		$file=$this->input->post('filelama');
		// 	}else{
		// 		$file=$_FILES['fileupload']['name'];			
		// 	}			
		// 	echo $file;
		// }		
		function update_data(){
			$id=$this->input->post('id');
			if(empty($_FILES['fileupload']['name'])){
				$file=$this->input->post('filelama');
			}else{
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					unlink($path.$this->input->post('filelama'));
					$fileupload=$this->upload->data();
					$file=$fileupload['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/jurnal'));
				}				
			}
			$data=array(
				'idpeg'=>$this->input->post('idpeg'),
				'thakademik'=>$this->input->post('thnakademik'),
				'namajurnal'=>$this->input->post('jurnal'),
				'lembaga'=>$this->input->post('lembaga'),
				'issn'=>$this->input->post('issn'),
				'tglterbit'=>date('Y-m-d',strtotime($this->input->post('tglterbit'))),
				'judmakalah'=>$this->input->post('makalah'),
				'jmltim'=>$this->input->post('jmltim'),
				'peran'=>$this->input->post('peran'),
				'keterangan'=>$this->input->post('keterangan'),
				'file'=>$file,
				'jenisjurnal'=>$this->input->post('jenisjurnal'),
				'update_date'=>date('Y-m-d'),
				'status'=>$this->input->post('status'),
			);	
			//print_r($data);
			$query=$this->mjurnal->update_jurnal($id,$data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_simpansuccess);
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);	
			}
			redirect(site_url('User/jurnal'));				
		}	
		function hapus($id){
			//AMBIL DATA SEMINAR BY ID SEMINAR
			$row=$this->mjurnal->get_jurnal_byid($id)->row();
			
			$query=$this->mjurnal->hapus_jurnal($id);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_hapussuccess);
				//unlink('../simpeg/FileJurnal/'.$row->file);								
				unlink($this->path.$row->file);	
			}else{
				$msg_error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
			}
			redirect(site_url('User/jurnal'));							
		}
		function detail(){
			$id=$this->input->post('id');
			$query=array(
				'where'=>array('id'=>$id),
				'tabel'=>"db_jurnal",
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
			);
			$this->load->view('jurnal/detail',$data);
		}
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan');
				redirect(site_url('User/Jurnal'));	
			}			
			// $link=$this->path.$file;
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/jurnal"));
			// }
		}
	}
?>