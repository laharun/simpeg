<?php
	Class Karir extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->idpegawai=$this->session->userdata('id_pegawai');
			$this->load->model(array('mseminarpenelitian','mtahunakad','Crud'));
			if($this->session->userdata('login')!=true){
				redirect(site_url('Login/logout'));
			}				
		}
		//DEKLARASI VAR
		private $master_tabel='db_karir';
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $default_url='karir';
		//private $path="./berkas/seminar/";
		//DUMMY
		private $path="./berkas/karir/";
		//private $path="http://php4.akprind.ac.id/simpeg/FileSeminar/";

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000, //5mb
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}			
		function index(){
			$this->form_validation->set_rules('karir_idpegawai','idpegawai','required');
			if($this->form_validation->run()==true){
				//$path=$this->path;$file='fileupload';
				$data=array(
					'karir_idpeg'=>$this->input->post('karir_idpegawai'),
					'karir_karir'=>$this->input->post('karir_karir'),
					'karir_tanggalditerima'=>date('Y-m-d',strtotime($this->input->post('karir_tanggalditerima'))),
					'karir_purnajabatan'=>date('Y-m-d',strtotime($this->input->post('karir_tanggalpurnajabatan'))),
				);				
				$file='fileupload';		
				if($_FILES[$file]['name']){
					if($this->fileupload($this->path,$file)){
						$uploaddata=$this->upload->data();
						$data['karir_file']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error',$dt['error']);
						redirect(site_url($this->default_url));
					}
				}	
				$query=array(
					'data'=>$data,
					'tabel'=>$this->master_tabel,
				);				
				$insert=$this->Crud->insert($query);
				if($insert){
					$this->session->set_flashdata('success','Simpan berhasil');
				}else{
					$msg='Simpan gagal, msg :'.$insert;
					$this->session->set_flashdata('error',$msg);
				}				
				redirect(site_url($this->default_url));
				//print_r($query['data']);
			}else{
				$query=array(
					'tabel'=>$this->master_tabel,
					'where'=>array(array('karir_idpeg'=>$this->idpegawai)),
				);
				//$var=$this->variabel();
				$data=array(
					'menu'=>'karir',
					'data'=>$this->Crud->read($query)->result(),
					'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
					);
				$this->load->view('template',$data);				
			}
		}
		private function get_file($id){
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array('karir_id'=>$id)),
			);
			$read=$this->Crud->read($query)->row();
			if($read->karir_file){
				unlink($this->path.$read->karir_file);
			}
		}
		function hapus($id){
			$this->get_file($id);
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array('karir_id'=>$id),
			);
			$delete=$this->Crud->delete($query);
			if($delete){
				$this->session->set_flashdata('success','Hapus berhasil');
			}else{
				$mssg='Hapus error, msg : '.$delete;
				$this->session->set_flashdata('error',$msg);
			}
			redirect(site_url($this->default_url));		
		}
		function edit(){
			$id=$this->input->post('id');
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array('karir_id'=>$id)),
			);
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'seminar'=>$this->mseminarpenelitian->get_seminar_byid($id)->row(),
				'data'=>$this->Crud->read($query)->row(),
			);
			//print_r($data['seminar']);
			$this->load->view('karir/form_edit',$data);			
		}
		function update(){
			$id=$this->input->post('id');
			$this->form_validation->set_rules('id','id karir','required');
			if($this->form_validation->run()==true){
				//$path=$this->path;$file='fileupload';
				$data=array(
					'karir_idpeg'=>$this->input->post('karir_idpegawai'),
					'karir_karir'=>$this->input->post('karir_karir'),
					'karir_tanggalditerima'=>date('Y-m-d',strtotime($this->input->post('karir_tanggalditerima'))),
					'karir_purnajabatan'=>date('Y-m-d',strtotime($this->input->post('karir_tanggalpurnajabatan'))),
				);					
				$file='fileupload';		
				if($_FILES[$file]['name']){
					if($this->fileupload($this->path,$file)){
						$uploaddata=$this->upload->data();
						$data['karir_file']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error',$dt['error']);
						redirect(site_url($this->default_url));
					}
				}	
				$query=array(
					'data'=>$data,
					'tabel'=>$this->master_tabel,
					'where'=>array('karir_id'=>$id),
				);				
				$insert=$this->Crud->update($query);
				if($insert){
					$this->session->set_flashdata('success','Update berhasil');
				}else{
					$msg='Update gagal, msg :'.$insert;
					$this->session->set_flashdata('error',$msg);
				}
				//print_r($data);
				redirect(site_url($this->default_url));
			}		
		}
		function detail(){
			$id=$this->input->post('id');
			$query=array(
				'where'=>array('id'=>$id),
				'tabel'=>'db_seminar',
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				);
			$this->load->view('seminar/detail',$data);
		}
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan,silahkan upload ulang');
				redirect(site_url('User/seminarpemakalah'));	
			}			
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/seminarpemakalah"));	
			// }			
		}		
	}
?>