<?php
	Class Seminar extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model(array('mseminarpenelitian','mtahunakad','Crud'));
		}

		//DEKLARASI VAR
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $path="./berkas/seminar/";
		//private $path="http://php4.akprind.ac.id/simpeg/FileSeminar/";

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}			
		function index(){
			$this->form_validation->set_rules('thnakademik','Tahun Akademik','required');
			if($this->form_validation->run()==true){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					//----AMBIL NAMA FILE UPLOAD
					$fileupload=$this->upload->data();					
					$data=array(
						'idpeg'=>$this->input->post('idpeg'),
						'tahunakademik'=>$this->input->post('thnakademik'),
						'namaseminar'=>$this->input->post('seminar'),
						'penyelenggara'=>$this->input->post('penyelenggara'),
						'tglpelaksana'=>date('Y-m-d',strtotime($this->input->post('tgl'))),
						'judulmakalah'=>$this->input->post('makalah'),
						'jumlahtim'=>$this->input->post('tim'),
						'peran'=>$this->input->post('peran'),
						'keterangan'=>$this->input->post('keterangan'),
						'file'=>$fileupload['file_name'],
						'save_date'=>date('Y-m-d'),
						'jenis'=>$this->input->post('jenis'),
						'status'=>$this->input->post('status'),
						'isbn'=>$this->input->post('isbn'),
					);	
					//print_r($data);					
					$query=$this->mseminarpenelitian->simpan_seminar($data);
					if($query==true){
						$this->session->set_flashdata('success',$this->msg_simpansuccess);
						redirect(site_url('User/seminarpemakalah'));
					}
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/seminarpemakalah'));
				}
			}
		}
		function hapus($id){
			$row=$this->mseminarpenelitian->get_seminar_byid($id)->row();
			//$res=unlink($this->path.$row->file);			
			$query=$this->mseminarpenelitian->hapus_seminar($id);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_hapussuccess);
				unlink($this->path.$row->file);				
			}else{
				$msg_error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
			}
			redirect(site_url('User/seminarpemakalah'));			
			//print_r($row->file);			
		}
		function edit_data(){
			$id=$this->input->post('id');
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'seminar'=>$this->mseminarpenelitian->get_seminar_byid($id)->row(),
			);
			//print_r($data['seminar']);
			$this->load->view('seminar/edit',$data);			
		}
		function update_data(){
			$id=$this->input->post('id');
			if(!empty($_FILES['fileupload']['name'])){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					$file=$this->upload->data();
					$fileupload=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/seminarpemakalah'));
				}
			}else{
				$fileupload=$this->input->post('filelama');
			}
			$data=array(
				'idpeg'=>$this->input->post('idpeg'),
				'tahunakademik'=>$this->input->post('thnakademik'),
				'namaseminar'=>$this->input->post('seminar'),
				'penyelenggara'=>$this->input->post('penyelenggara'),
				'tglpelaksana'=>date('Y-m-d',strtotime($this->input->post('tgl'))),
				'judulmakalah'=>$this->input->post('makalah'),
				'jumlahtim'=>$this->input->post('tim'),
				'peran'=>$this->input->post('peran'),
				'keterangan'=>$this->input->post('keterangan'),
				'file'=>$fileupload,
				'update_date'=>date('Y-m-d'),
				'jenis'=>$this->input->post('jenis'),
				'status'=>$this->input->post('status'),
				'isbn'=>$this->input->post('isbn'),
			);
			//print_r($data);
			$query=$this->mseminarpenelitian->update_seminar($id,$data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_success_update);
				redirect(site_url('User/seminarpemakalah'));	
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
				redirect(site_url('User/seminarpemakalah'));				
			}			
		}
		function detail(){
			$id=$this->input->post('id');
			$query=array(
				'where'=>array('id'=>$id),
				'tabel'=>'db_seminar',
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				);
			$this->load->view('seminar/detail',$data);
		}
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan,silahkan upload ulang');
				redirect(site_url('User/seminarpemakalah'));	
			}			
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/seminarpemakalah"));	
			// }			
		}		
	}
?>