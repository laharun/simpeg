<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masabakti extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model(array('mjafa','Crud'));
		if(!$this->session->userdata('login')){
			redirect(site_url('Login'));
		}
	}

	//DEKLARASI VAR
	private $msg_simpansuccess="Data berhasil disimpan";
	private $msg_hapussuccess="Data berhasil dihapus";		
	private $path='./berkas/jafa/';

	public	function fileupload($path,$file){
		$config=array(
			'upload_path'=>$path,
			'allowed_types'=>'pdf',
			'max_size'=>5000,
			'encrypt_name'=>true,
		);
		$this->load->library('upload',$config);
		return $this->upload->do_upload($file);
	}
	private function dumpdata($data){
		echo "<pre>";
		print_r($data);
	}
	public	function downloadfile($path,$namafile){
		$url=file_get_contents($path.$namafile);
		return force_download($namafile,$url);
	}
	private function masabaktitendik(){
		$query=array(
			'tabel'=>'db_pegawai',
			'select'=>'idpeg,nama,gelardepan,gelarbelakang,tanggallahir,awaltetap,awalmasuk,statuspegawai',
			'order'=>array('kolom'=>'nama','orderby'=>'ASC'),
			'where'=>array(array('kategori'=>'kependidikan')),
		);
		$pegawai=$this->Crud->read($query)->result();
		$arrayakhir=array();
		$kependidiakn=array();
		if(count($pegawai)<>0){
			foreach ($pegawai as $index => $row) {
				$arrayakhir[$index]=$row;
				$tgltetap = new DateTime($row->awaltetap);
				$tglmasuk = new DateTime($row->awalmasuk);
				$tgllahir = new DateTime($row->tanggallahir);
				$today = new DateTime();
				
				$umur=$today->diff($tgllahir);
				$mkg=$today->diff($tgltetap);
				$mkk=$today->diff($tglmasuk);
	
				$arrayakhir[$index]->umur=array('tahun'=>$umur->y,'bulan'=>$umur->m);					
				$arrayakhir[$index]->mkg=array('tahun'=>$mkg->y,'bulan'=>$mkg->m);	
				$arrayakhir[$index]->mkk=array('tahun'=>$mkk->y,'bulan'=>$mkk->m);						
			}			
		}
		return $arrayakhir;		
	}	
	public function index()
	{
		$query=array(
			'tabel'=>'db_pegawai',
			'select'=>'idpeg,nama,gelardepan,gelarbelakang,tanggallahir,awaltetap,awalmasuk,statuspegawai',
			'order'=>array('kolom'=>'nama','orderby'=>'ASC'),
			'where'=>array(array('kategori'=>'pendidik')),
		);
		$pegawai=$this->Crud->read($query)->result();
		$arrayakhir=array();
		$kependidiakn=array();
		if(count($pegawai)<>0){
			foreach ($pegawai as $index => $row) {
				$arrayakhir[$index]=$row;
				$tgltetap = new DateTime($row->awaltetap);
				$tglmasuk = new DateTime($row->awalmasuk);
				$tgllahir = new DateTime($row->tanggallahir);
				$today = new DateTime();
				
				$umur=$today->diff($tgllahir);
				$mkg=$today->diff($tgltetap);
				$mkk=$today->diff($tglmasuk);
	
				$arrayakhir[$index]->umur=array('tahun'=>$umur->y,'bulan'=>$umur->m);					
				$arrayakhir[$index]->mkg=array('tahun'=>$mkg->y,'bulan'=>$mkg->m);	
				$arrayakhir[$index]->mkk=array('tahun'=>$mkk->y,'bulan'=>$mkk->m);						
			}			
		}
		$data=array(
			'menu'=>'personalia',
			'submenu'=>'masabakti',
			'headline'=>'Masa Bakti',
			'data'=>$arrayakhir,	
			'tendik'=>$this->masabaktitendik(),		
		);
		$this->load->view('administrator',$data);
		//$this->dumpdata($data['tendik']);
	}
	public function simpan_data(){
		$path=$this->path;
		$file='fileupload';
		if($this->fileupload($path,$file)){
			$fileupload=$this->upload->data();
			$idpeg=$this->session->userdata('id_pegawai');
			$data=array(
				'idpeg'=>$idpeg,
				'namajafa'=>$this->input->post('jafa'),
				'angkakredit'=>$this->input->post('angka_kredit'),
				'tmt'=>$this->input->post('tmt'),
				'file'=>$fileupload['file_name'],
			);
			//print_r($data);
			$query=$this->mjafa->simpan_jafa($data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_simpansuccess);
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
			}			
		}else{
			$this->session->set_flashdata('error',$this->upload->display_errors());
			//echo $this->upload->display_errors();
		}		
		redirect(site_url('User/jabatanfungsional'));
	}
	public function hapus_data($id){
		$file=array(
			'select'=>'file',
			'tabel'=>'db_jafa',
			'where'=>array('id'=>$id),
		);
		$file=$this->Crud->read($file)->row();
		$query=$this->mjafa->hapus_jafa($id);
		if($query){
			unlink($this->path.trim($file->file));
			$this->session->set_flashdata('success',$this->msg_hapussuccess);
		}else{
			$error=$this->db->error();
			$this->session->set_flashdata('error',$error['message']);
		}
		redirect(site_url('User/jabatanfungsional'));			
	}
	public function download($namafile){
		$path=$this->path;
		$this->downloadfile($path,$namafile);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */