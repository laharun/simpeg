<?php
	Class mriwayatjabatan extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		function get_riwayatjabatan_byidpeg($id){
			$this->db->where('idpeg',$id);
			return $this->db->get('db_riwayatjabatan');
		}
		function simpan_riwayatjabatan($data){
			if($this->db->insert('db_riwayatjabatan',$data)){
				return true;	
			}else{
				return false;
			}
		}
		function hapus_riwayatjabatan($id){
			$this->db->where('id',$id);
			if($this->db->delete('db_riwayatjabatan')){
				return true;
			}else{
				return false;
			}
		}

	}
?>