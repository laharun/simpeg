<?php
	Class mkursus extends CI_Model{
		public function __construct(){
			parent:: __construct();
		}
		////////////////////////////////////////////////////////////////
		function simpan_data($data){
			if($this->db->insert('db_kursus',$data)){
				return true;
			}else{
				$error=$this->db->error();
				return $error['message'];
			}
		}
		function get_data(){
			$this->db->where('idpeg');
			return $this->db->get('db_kursus');
		}
		function hapus_data($id){
			$this->db->where('id',$id);
			if($this->db->delete('db_kursus')){
				return true;
			}else{
				$error=$this->db->error();
				return $error['message'];
			}
		}
	}
?>