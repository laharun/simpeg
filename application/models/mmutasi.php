<?php
	Class mmutasi extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		function get_mutasi_byidpeg($id){
			$this->db->where('idpeg',$id);
			return $this->db->get('db_mutasi');
		}
		function simpan_mutasi($data){
			if($this->db->insert('db_mutasi',$data)){
				return true;	
			}else{
				return false;
			}
			
		}
		function hapus_mutasi($id){
			$this->db->where('id',$id);
			if($this->db->delete('db_mutasi')){
				return true;
			}else{
				return false;
			}
		}

	}
?>