<?php
	Class mpenulisanbuku extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		function get_penulisan_byidpeg($idpeg){
			$q=$this->db->query("SELECT * FROM db_buku WHERE idpeg='$idpeg' ORDER by thnakademik DESC");
			return $q;
		}
		//-------------------------------SIMPAN DATA--------------------
		function simpan_penulisan($data){
			$this->db->insert('db_buku',$data);
			return true;
		}
		//------------------------------AMBIL NAMA FILE---------------------------
		function get_penulisan_byid($id){
			$row=$this->db->query("SELECT * from db_buku WHERE id='$id'");
			return $row;
		}
		//-----------------------------HAPUS DATA------------------------------
		function hapus_penulisan($id){
			$this->db->where('id',$id);
			if($this->db->delete("db_buku")==true){
				return true;
			}else{
				return false;
			}
		}
		function update_penulisan($id,$data){
			$this->db->where('id',$id);
			$query=$this->db->update('db_buku',$data);
			if($query){
				return true;
			}else{
				return false;
			}
		}		
	}
?>