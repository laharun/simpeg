<section class="content">
<div class="container-fluid">
<div class=" block-header">
	<h2 ><?= ucwords($judul)?></h2>
</div>
<!--KONFIRMASI AKSI-->
<?php
	if($this->session->flashdata('success')){
		echo'
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                '.$this->session->flashdata("success").'
            </div>
		';
	}elseif($this->session->flashdata('error')){
		echo'
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                '.$this->session->flashdata("error").'
            </div>
		';				
	}	
?>
<!--FORM TAMBAH DATA-->
<div class="row clearfix formtambah">
	<div class="col-sm-2">
		<div class="form-group">
			<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
		</div>
	</div>
</div>		
<!--TAMBAH DATA FORM-->		
<div class="row clearfix">
	<?php require_once 'add.php';?>
</div>
<!--EDIT FORM-->
<div class="row clearfix">
	<?php require_once 'edit.php';?>
</div>		
<!--TABEL DATA-->
<div class="row clearfix" id="tampildata">
	<?php require_once 'tabel.php';?>
</div>		
</section>