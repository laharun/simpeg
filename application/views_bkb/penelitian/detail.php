<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="defaultModalLabel">Detail Penelitian</h4>
        </div>
        <div class="modal-body">
            <table class="table table-bordered table-striped" width="100%">
                <tr>
                    <th width="20%">Tahun Akademk</th>
                    <td width="80%"><?= ucwords($data->tahunakademik)?></td>
                </tr>
                <tr>
                    <th>Penelitian</th>
                    <td ><?= ucwords($data->judulpenelitian)?></td>
                </tr>
                <tr>
                    <th>Tempat</th>
                    <td ><?= ucwords($data->tempat)?></td>
                </tr>                 
                <tr>
                    <th>Abstrak</th>
                    <td><?= ucwords($data->abstrak)?></td>
                </tr>                 
                <tr>
                    <th>Dana</th>
                    <td class="price"><?= $data->dana?></td>
                </tr>
                <tr>
                    <th >Asal dana</th>
                    <td ><?= ucwords($data->asaldana)?></td>
                </tr>  
                <tr>
                    <th >Skema</th>
                    <td><?= $data->skema?></td>
                </tr>                                                                               
                <tr>
                    <th >Peran</th>
                    <td ><?= ucwords($data->peran)?></td>
                </tr>
                <tr>
                    <th >Jumlah tim</th>
                    <td ><?= ucwords($data->tim)?></td>
                </tr>                                                                                                                                                                                             
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info waves-effect btn-block" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>  
<script type="text/javascript">
    $(".price").priceFormat({
      prefix:'Rp ',
      thousandsSeparator:'.',
      centsLimit:'0',
    });    
</script>