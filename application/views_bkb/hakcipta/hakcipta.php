<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>HAK CIPTA/KEKAYAAN INTELEKTUAL</h2>
		</div>
		<!--KONFIRMASI AKSI-->
		<?php
			if($this->session->flashdata('success')){
				echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
				';
			}elseif($this->session->flashdata('error')){
				echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
				';				
			}	
		?>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-2">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>		
		<!--TAMBAH DATA FORM-->		
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput" style="display:none">
					<div class="body">
						<form id="formhaki" action="<?php echo base_url('Hakcipta')?>" method="POST" enctype="multipart/form-data">
							<h2 class="card-inside-title">Form Isian HAKI</h2>
							<div class="row clearfix">
								<div class="col-sm-12">
									<div class="form-group form-float hide">
										<div class="form-line">
											<input type="text" class="form-control" name="idpeg" value="<?php echo $this->session->userdata('id_pegawai')?>">
											<label class="form-label">Id Pegawai</label>
										</div>
									</div>								
									<div class="form-group">
	                                    <p>
	                                        <b>Tahun Akademik</b>
	                                    </p>
	                                    <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
	                                    	<?php
	                                    		foreach ($thnakademik as $value) {
	                                    			echo "<option>".$value->tahun."</option>";
	                                    		}
	                                    	?>
	                                    </select>

									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<textarea required rows="4" class="form-control" name="judul"></textarea>
											<label class="form-label">Judul Ciptaan</label>
										</div>
									</div>	                            		
									<div class="form-group form-float">
										<div class="form-line">
											<input required type="text" class="form-control" name="nomor" />
											<label class="form-label">Nomor Pencatatan</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input required name="tgl" type="text" class="form-control datepicker" placeholder="Tanggal Penetapan/diumumkan"/>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<textarea required rows="4" class="form-control" name="tempat"></textarea>
											<label class="form-label">Di Umumkan di</label>
										</div>
									</div>																			
	                            	<div class="form-group">
	                                    <p>
	                                        <b>Peran</b>
	                                    </p>
	                                    <select required class="form-control show-tick" data-live-search="true" title="Pilih Peran" name="peran" required>
	                                        <option value="Ketua">Ketua</option>
	                                        <option value="Anggota">Anggota</option>
	                                    </select>	
	                            	</div>
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input required value="0" type="text" class="form-control" name="jmltim">
	                            			<label class="form-label">Jumlah TIM</label>
	                            		</div>
	                            		<p class="help-info col-red">Jika Peran Sebagai Ketua Isikan 0</p>
	                            	</div>	                            		                            		                            		                            	
									<h2 class="card-inside-title">Upload </h2>
									<div class="form-group form-float">
										<div class="">
											<input type="file" name="fileupload" >
											<p class="help-block col-red">Ukuran Maksimal 5mb</p>
										</div>
									</div>									
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
									<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
								</div>
							</div>							 	
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--EDIT FORM-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card" id="edit_card" style="display:none">
					<div class="header bg-orange">
		                <h2>
                          Edit Data
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!---->
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class="tutup_card">Tutup</a></li>
                                    </ul>
                                </li>
                         </ul>                        				
					</div>				
					<div class="body" id="edit_form">
						<!--AJAX LOAD HERE-->
					</div>
				</div>				
			</div>
		</div>		
		<!--TABEL DATA-->
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header">
		                <h2>
                           Daftar Kepemilikan HAKI
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="5%">Tahun</th>
												<th width="20%">Haki</th>
												<th width="20%">Nomor</th>
												<th width="10%">Diumumkan</th>
												<th width="10%">Peran</th>
												<th class="text-center" width="10%">Aksi</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i=1;
											foreach ($haki as $val) {
												echo "<tr>";
													echo "<td>".$i."</td>";
													echo "<td>".$val->thnakademik."</td>";
													echo "<td>".$val->haki."</td>";
													echo "<td>".$val->nomor."</td>";
													echo "<td>".date('d-m-Y',strtotime($val->tgl))."<br> Diumumkan di :".$val->tempat."</td>";
													echo "<td>".$val->peran."<br>Jumlah Tim :".$val->jumtim."</td>";
													echo "<td class='text-center'>
														<a href='".site_url('Hakcipta/downloadfile/'.$val->file)."' style='width:30px' class='btn btn-xs btn-info waves-effect'><i class='material-icons'>file_download</i></a> 
														<a href='".site_url('Hakcipta/hapus/'.$val->id)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
														<a href='#' id='".$val->id."' link='".site_url('Hakcipta/edit_data')."' class='edit_data btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> 
													</td>";
												echo "</tr>";
												$i++;
											}											
										?>																					
										</tbody>
									</table>
									<p>Keterangan :</p>
									<a href='#' style="width:30px; margin:2px;" class='btn btn-xs btn-info waves-effect'><i class='material-icons'>file_download</i></a> : Download File <br>
									<a href='#' style="width:30px; margin:2px" class='btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> : Tombol Hapus <br>
									<a href='#' style="width:30px;margin:2px" class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> : Tombol Edit <br>								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
</section>