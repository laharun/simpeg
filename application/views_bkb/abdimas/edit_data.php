<form  enctype="multipart/form-data" action="<?php echo site_url('abdimas/update_data')?>" method="POST" >
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group hide">
				<div class="form-line">
					<b>Id</b>
					<input type="text" class="form-control" name="id" value="<?php echo $abdimas->id?>">
				</div>				
			</div>
			<div class="form-group">			
				<div class="form-line">
					<b>Id Pegawai</b>
					<input type="text" class="form-control" name="idpeg" value="<?php echo $abdimas->idpeg?>">
				</div>
			</div>								
			<div class="form-group hide">
	            <p>
	               	<b>Tahun Akademik</b>
	            </p>
	            <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
	                <?php
	                    foreach ($thnakademik as $value) {
	                    echo "<option value='".$value->tahun."'";
	                    if($abdimas->thnakademik==$value->tahun){echo "selected";}
	                    echo ">".$value->tahun."</option>";
	               		}
	                ?>
	            </select>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<textarea class="form-control" required name="tema"><?php echo ucwords($abdimas->tema)?></textarea>
					<label class="form-label">Tema Penelitian</label>
				</div>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
				<p><b>Biaya</b></p>
					<input type="text" name="biaya" class="form-control price" value="<?= $abdimas->biaya?>"> 	
				</div>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<p><b>Asal Dana</b></p>
					<input type="text" name="asaldana" class="form-control" value="<?= $abdimas->asaldana?>">
				</div>
			</div>			
			<div class="form-group">
				<div class="form-line">
					<p>
						<b>Tanggal Mulai</b>
					</p>
					<input type="text" class="form-control datepicker" name="tglmulai" value="<?php echo  $abdimas->tgl;?>">
				</div>
			</div>
			<div class="form-group">
				<div class="form-line">
					<p>
						<b>Tanggal Selesai</b>
					</p>										
					<input type="text" class="form-control datepicker" required name="tglselesai" value="<?php echo  $abdimas->tglselesai;?>">
				</div>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<textarea class="form-control" name="lokasi" rows="3" required><?php echo ucwords($abdimas->lokasi)?></textarea>
					<label class="form-label">Lokasi</label>
				</div>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<textarea class="form-control" name="abstrak" rows="4" required><?php echo ucwords($abdimas->abstrak)?></textarea>
					<label class="form-label">Abstrak</label>
				</div>
			</div>			
			<h2 class="card-inside-title">Upload File Pendukung</h2>
			<div class="form-group hide">
				<div class="form-line">
					<input type="text" class="form-control" name="filelama" value="<?php echo $abdimas->file?>">
				</div>				
			</div>			
			<div class="form-group">
				<input type="file" name="fileupload">
					<p class="help-block col-red">Ukuran Maksimal 5mb</p>
			</div>																											
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-warning btn-lg waves-effect btn-block">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
           $("#edit_card").hide();
    })
    $(".price").priceFormat({
      prefix:'Rp ',
      thousandsSeparator:'.',
      centsLimit:'0',
    });    
</script>