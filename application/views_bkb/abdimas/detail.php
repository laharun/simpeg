<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="defaultModalLabel">Detail Abdimas</h4>
        </div>
        <div class="modal-body">
            <table class="table table-bordered table-striped" width="100%">
                <tr>
                    <th width="20%">Tahun Akademk</th>
                    <td width="80%"><?= ucwords($data->thnakademik)?></td>
                </tr>
                <tr>
                    <th >Tema</th>
                    <td ><?= ucwords($data->tema)?></td>
                </tr> 
                <tr>
                    <th >Tanggal</th>
                    <td ><?= date('d-m-Y',strtotime($data->tgl))." - ".date('d-m-Y',strtotime($data->tglselesai))?></td>
                </tr>
                <tr>
                    <th >Tempat</th>
                    <td ><?= ucwords($data->lokasi)?></td>
                </tr>  
                <tr>
                    <th >Asal dana</th>
                    <td class="price"><?= $data->biaya?></td>
                </tr>                                                                               
                <tr>
                    <th >Asal dana</th>
                    <td ><?= ucwords($data->asaldana)?></td>
                </tr>  
                <tr>
                    <th>Abstrak</th>
                    <td><?= ucwords($data->abstrak)?></td>
                </tr>                                                                                                                                                                          
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info waves-effect btn-block" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>  
<script type="text/javascript">
    $(".price").priceFormat({
      prefix:'Rp ',
      thousandsSeparator:'.',
      centsLimit:'0',
    });    
</script>