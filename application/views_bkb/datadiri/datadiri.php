<section class="content">
	<div class="container-fluid">
        <!--KONFIRMASI AKSI-->
        <?php
            if($this->session->flashdata('success')){
                echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
                ';
            }elseif($this->session->flashdata('error')){
                echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
                ';              
            }   
        ?>	
        <!--ALERT-->
        <div class="alert bg-blue">
            <b>Perhatian !</b> Ada beberapa data yang hanya bisa dirubah oleh Bag.Kepegawaian<br> 
            Terimakasih
        </div>
		<div class="block-header">
			<h2>DATA PERSONALIA</h2>
		</div>
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="body">
						<form id="formpersonalia" action="<?php echo site_url('Pegawai/update_datapersonalia')?>" method="POST" enctype="multipart/form-data">
							<h2 class="card-inside-title">Data Personalia</h2><hr>
	                        <div class="row clearfix">
	                            <div class="col-sm-6">
									<div class="form-group form-float">
										<div class="form-line">
											<input readonly type="text" class="form-control" name="noskmasuk" value="<?php echo $datadiri->noskmasuk?>">
											<label class="form-label col-red ">No SK Masuk</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input readonly type="text" class="form-control" name="nosktetap" value="<?php echo $datadiri->nosktetap?>">
											<label class="form-label col-red">No SK Tetap</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input readonly type="text" class="form-control" name="nik" value="<?php echo $datadiri->nik?>">
											<label class="form-label col-red">NIK</label>
										</div>
									</div>		                            
                                    <div class="form-group">
                                        <b>Kategori</b>
                                        <select required class="form-control show-tick" data-live-search="true" name="kategori">
                                            <option <?php if(trim($datadiri->kategori)=='kependidikan'){echo "selected";}?> value="kependidikan" >Tendik</option>
                                            <option <?php if(trim($datadiri->kategori)=='pendidik'){echo "selected";}?> value="pendidik">Pendidik</option>
                                        </select>
                                    </div>
	                            	<div class="form-group float">
	                            		<div class="form-line">
	                            			<input type="text" class="form-control price" placeholder="" name="gajiawal" value="<?php echo $datadiri->gajiawal?>">
	                            			<label class="form-label">Gaji Awal</label>
	                            		</div>
	                            	</div>  

									<div class="form-group">
	                                    <b>Golongan<?= $datadiri->idgolongan?></b>
	                                    <select required class="form-control show-tick" name="golongan" data-live-search="true" data-size="7">
	                                        <?php foreach($golongan AS $val):?>
	                                        	<option value="<?= $val->pangkat?>" <?= trim($datadiri->idgolongan)==$val->pangkat ? 'selected':''?>><?= $val->pangkat." / ".$val->golongan." ".$val->ruang?></option>
	                                       	<?php endforeach;?>                                           
	                                     </select>										
									</div>									
									<div class="form-group form-float">
										<div class="form-line">
											<p>
												<b>Tanggal Diangkat Menjadi Tetap</b>
											</p>
											<input type="text" class="form-control datepicker" name="tgltetap" value="<?php echo date('Y-m-d',strtotime($datadiri->awaltetap))?>">
										</div>
									</div>
									<div class="form-group form-float">
										<b>Jabatan</b>
                                        <select required class="form-control show-tick" data-live-search="true" name="jabatan">
                                            <option <?php if(trim($datadiri->jabatan)=='Asisten Ahli'){echo "selected";}?> value="Asisten Ahli" >Asisten Ahli</option>
                                            <option <?php if(trim($datadiri->jabatan)=='Lektor'){echo "selected";}?> value="Lektor">Lektor</option>
                                            <option <?php if(trim($datadiri->jabatan)=='Lektor Kepala'){echo "selected";}?> value="Lektor Kepala">Lektor Kepala</option>
                                            <option <?php if(trim($datadiri->jabatan)=='Guru Besar'){echo "selected";}?> value="Guru Besar">Guru Besar</option>
                                        </select>	
									</div>	                            	                              
	                            </div>
	                            <div class="col-sm-6">
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" value="<?php echo trim($datadiri->fingerprintid)?>" name="idfingeprint"class="form-control">
											<label class="form-label">Fingerprint Kode</label>
										</div>
									</div>		                            
                                    <div class="form-group">
                                        <b class="col-red">Status</b>
                                        <select class="form-control show-tick" name="status" data-live-search="true" data-size="7">
                                         <optgroup label="PENDIDIK/DOSEN" disabled="">    
                                            <option <?php if(trim($datadiri->idstatuspeg)=='Dosen Tetap')echo "selected";?> value="Dosen Tetap">Dosen Tetap</option>
                                            <option <?php if(trim($datadiri->idstatuspeg)=='PNS DPK')echo "selected";?> value="PNS DPK">PNS DPK</option>
                                            <option <?php if(trim($datadiri->idstatuspeg)=='Tidak Tetap')echo "selected";?>value="Tidak Tetap">Tidak Tetap</option>   
                                            <option <?php if(trim($datadiri->idstatuspeg)=='DPPK')echo "selected";?> value="DPPK">DPPK</option>                                                                                       
                                        </optgroup>
                                        <optgroup label="KEPENDIDIKAN" disabled="">    
                                            <option <?php if(trim($datadiri->idstatuspeg)=='RT')echo "selected";?> value="RT">RT</option>
                                            <option <?php if(trim($datadiri->idstatuspeg)=='Satpam')echo "selected";?> value="Satpam">Satpam</option>
                                            <option <?php if(trim($datadiri->idstatuspeg)=='Pengemudi')echo "selected";?> value="Pengemudi">Pengemudi</option>
                                            <option <?php if(trim($datadiri->idstatuspeg)=='Laboran')echo "selected";?> value="Laboran">Laboran</option>
                                            <option <?php if(trim($datadiri->idstatuspeg)=='Administrasi')echo "selected";?> value="Administrasi">Administrasi</option>
                                            <option <?php if(trim($datadiri->idstatuspeg)=='Teknisi')echo "selected";?> value="Teknisi">Teknisi</option>                                                  
                                        </optgroup>    
                                        </select>
                                    </div>
							      	<div class="form-group">
								        <p>
								          <b>Homebase</b>
								        </p>
								        <select required class="form-control select show-tick" data-size="5" data-live-search="true"  title="Pilih Homebase" name="homebase">
								          <?php
								            foreach ($homebase as $val) {
								              echo "<option value='".$val->kode."'";
								              if($datadiri->homebase==$val->kode){echo "selected";}
								              echo ">".$val->jurusan." - ".$val->jenjang."</option>";
								            }
								          ?>  
								        </select>
							      	</div>
                            	                           	                                                   	
	                            </div>
	                        </div>
	                        <div class="row clearfix">
		                        <div class="col-sm-6">
		                        	<div class="form-group form-float hide">
		                        		<b>Foto Lama</b>
		                        		<input type="text" class="form-control" name="fotolama" value="<?php echo $datadiri->foto?>">
		                        	</div>
		                        	<div class="form-group">
		                        		<b>Foto</b>
	                                    <input type="file" name="filefoto">
	                                    <p class="pull-left help-info col-red">Ukuran Maksimal 5mb, dengan format JPG/jpeg</p>
	                                </div>		                        	
		                        </div>
	                        </div>	 
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect btn-block">Update</button>
								</div>	
							</div>		
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>