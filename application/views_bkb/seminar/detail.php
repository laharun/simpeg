<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="defaultModalLabel">Detail Seminar</h4>
        </div>
        <div class="modal-body">
            <table class="table table-bordered table-striped" width="100%">
                <tr>
                    <th width="20%">Tahun Akademk</th>
                    <td width="80%"><?= ucwords($data->tahunakademik)?></td>
                </tr> 
                <tr>
                    <th>Jenis</th>
                    <td><?= ucwords($data->jenis)?></td>
                </tr> 
                <tr>
                    <th>Status</th>
                    <td><?= ucwords($data->status)?></td>
                </tr> 
                <tr>
                    <th>Seminar</th>
                    <td><?= ucwords($data->namaseminar)?></td>
                </tr>
                <tr>
                    <th>Makalah</th>
                    <td><?= ucwords($data->judulmakalah)?></td>
                </tr>
                <tr>
                    <th>ISBN</th>
                    <td><?= ucwords($data->isbn)?></td>
                </tr>                
                <tr>
                    <th>Penyelenggara</th>
                    <td><?= ucwords($data->penyelenggara)?></td>
                </tr>
                <tr>
                    <th>Tanggal</th>
                    <td><?= date('d-m-Y',strtotime($data->tglpelaksana))?></td>
                </tr>               
                <tr>
                    <th>Peran</th>
                    <td><?= ucwords($data->peran)?></td>
                </tr>
                <tr>
                    <th>Jumlah Tim</th>
                    <td><?= ucwords($data->jumlahtim)?></td>
                </tr>
                <tr>
                    <th>Keterangan</th>
                    <td><?= ucwords($data->keterangan)?></td>
                </tr>                                                                                                                                                                                                                                                                                                              
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info waves-effect btn-block" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>  