<section class="content">
	<div class="container-fluid">
        <!--ALERT-->
        <div class="alert bg-blue">
            <b>Perhatian !</b> Dimohon untuk menghindari penulisan dengan karakter spesial('' _ &)<br> 
            Untuk keperluan Thidharma silahkan pilih menu <i class="material-icons">assignment_turned_in</i> <b>Data Penunjang</b> <br>
            Terimakasih
        </div>
		<!--KONFIRMASI AKSI-->
		<?php
			if($this->session->flashdata('success')){
				echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
				';
			}elseif($this->session->flashdata('error')){
				echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
				';				
			}	
		?>        			
		<div class="block-header" style="margin:5px;">
			<div class="row">
				<div class="col-sm-6">
					<h2 >PROFIL PEGAWAI</h2>
				</div>
				<div class="col-sm-6">
					<a href="<?php echo site_url('Cetak')?>" target="_blank" type="button" class="pull-right btn btn-warning btn-md waves-effect">Cetak Profil</a>
				</div>			
			</div>
		</div>		
		<!--DATA DIRI PEGAWAI-->
		<div class="row clearfix">
			<div class="col-lg-4 col-md-4">
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="card">
							<div class="body">
								<h2 class="card-inside-title bg-blue" style="padding:10px;">Profil</h2>
								<div class="row clearfix">
									<div class="text-center form-group">
										<?php
											if(empty($pegawai->foto)){
												echo '<img  class="img img-circle" src="'.base_url().'assets/images/avatar.png" width="96" height="96" alt="Foto Not Found" />';
											}else{
												echo '<img class="img img-circle" src="'.base_url('berkas/fileFoto/'.$foto).'" width="96" height="96" alt="User" />';
												//echo $res;
											}											
										?>
									</div>
									<table class="table table-striped">
										<tr>
											<td class="text-capitalize"><b>Id Pegawai</b> <?php echo $this->session->userdata('id_pegawai')?></td>
										</tr>							
										<tr>
											<td class="text-capitalize"><b>Nama</b> 
											<?php if((trim($pegawai->gelardepan)!='-') AND ($pegawai->gelardepan)):?>
												<?=$pegawai->gelardepan?>
											<?php endif;?>
											<?php echo " ".$pegawai->nama?>
											<?php if((trim($pegawai->gelarbelakang)!='-') AND ($pegawai->gelarbelakang)):?>
												<?=', '.$pegawai->gelarbelakang?>
											<?php endif;?>
											</td>
										</tr>
										<tr>
											<td class="text-capitalize"><b>NIK</b> <?php echo $pegawai->nik?></td>
										</tr>
										<tr>
											<td class="text-capitalize"><b>Homebase</b> <?php echo $pegawai->homebase_jurusan.' - '.$pegawai->homebase_program?></td>
										</tr>																		
										<tr>
											<td class="text-capitalize"><b>Jabatan</b> <?php echo $pegawai->jabatan?></td>
										</tr>
										<tr>
											<td class="text-capitalize"><b>Golongan</b> <?php echo $pegawai->idgolongan?></td>
										</tr>
										<tr>
											<td class="text-capitalize"><b>Pegawai</b> <?php echo $pegawai->idstatuspeg?></td>
										</tr>
										<tr>
											<td class="text-capitalize"><b>Kategori</b> <?php echo $pegawai->kategori?></td>
										</tr>
										<tr>
											<td class="text-capitalize"><b>Pendidikan Terakhir</b> <?php echo $pegawai->idpendidikantertinggi?></td>
										</tr>																																										
									</table>					
								</div>						
							</div>
						</div>						
					</div>
					<div class="col-sm-12">
						<div class="card">
							<div class="body">
								<h2 class="card-inside-title bg-blue" style="padding:10px;">Data Berkas</h2>
								<div class="row clearfix">
									<div class="col-sm-12">
										<table class="table table-striped">
											<tr>
												<td><b>No Sk.Masuk</b> <?php echo $pegawai->noskmasuk?></td>
											</tr>
											<tr>
												<td><b>No Sk.Tetap</b> <?php echo $pegawai->nosktetap?></td>
											</tr>																						
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-8">
				<div class="card">
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<h2 class="card-inside-title bg-blue" style="padding:10px;">Data Pribadi</h2>
								<table class="table table-striped">
									<tr>
										<td><b>Tanggal Lahir</b> <?php if(($pegawai->tanggallahir)!='1970-01-01'){echo date('d-m-Y',strtotime($pegawai->tanggallahir));}else{echo "-";}?></td>
									</tr>								
									<tr>
										<td><b>Jenis Kelamin</b> <?php echo $pegawai->idjeniskelamin?></td>
									</tr>
									<tr>
										<td><b>Agama</b> <?php echo $pegawai->idagama?></td>
									</tr>
									<tr>
										<td><b>Status Nikah</b> <?php echo $pegawai->statusnikah?></td>
									</tr>									
									<tr>
										<td><b>Golongan Darah</b> <?php echo $pegawai->idgoldarah?></td>
									</tr>																										
									<tr>
										<td><b>No HP</b> <?php echo $pegawai->nohp?></td>
									</tr>								
									<tr>
										<td><b>No KTP</b> <?php echo $pegawai->noktp?></td>
									</tr>
									<tr>
										<td><b>No KK</b> <?php echo $pegawai->nokk?></td>
									</tr>
									<tr>
										<td><b>No BPJS</b> <?php echo $pegawai->nobpjs?></td>
									</tr>
									<tr>
										<td><b>No NPMW</b> <?php echo $pegawai->nonpwp?></td>
									</tr>																																			
									<tr>
										<td><b>Alamat</b> <?php echo $pegawai->alamatjalan?></td>
									</tr>
									<tr>
										<td><b>Kode POS</b> <?php echo $pegawai->kodepos?></td>
									</tr>
									<tr>
										<td><b>Alamat Email</b> <?php echo $pegawai->email?></td>
									</tr>																		
								</table>
								<h2 class="card-inside-title bg-light-blue" style="padding:10px;">Berkas Pribadi</h2>
								<table class="table table-striped">
									<tr>
										<td><b>Kartu Keluarga</b> <?php if(trim($pegawai->filekk)!=""){echo $pegawai->filekk ."<br> <a href=\"".site_url('Pegawai/downloadkk/'.trim($pegawai->filekk))."\" ><span class=\"label label-success\">Download</span></a>";}else{echo "Belum Upload";}?></td>
									</tr>
									<tr>
										<td><b>Kartu Tanda Penduduk</b> <?php if(trim($pegawai->filektp)!=""){echo $pegawai->filektp."<br> <a href=\"".site_url('Pegawai/downloadktp/'.trim($pegawai->filektp))."\" ><span class=\"label label-success\">Download</span></a>";;}else{echo "Belum Upload";}?></td>
									</tr>
									<tr>
										<td><b>SK Masuk</b> <?php if(trim($pegawai->fileskmasuk)!=""){echo $pegawai->fileskmasuk ."<br> <a href=\"".site_url('Pegawai/downloadskmasuk/'.trim($pegawai->fileskmasuk))."\" ><span class=\"label label-success\">Download</span></a>";}else{echo "Belum Upload";}?></td>
									</tr>									
									<tr>
										<td><b>SK Diangkat Tetap</b> <?php if(trim($pegawai->filesktetap)!=""){echo $pegawai->filesktetap ."<br> <a href=\"".site_url('Pegawai/downloadsktetap/'.trim($pegawai->filesktetap))."\" ><span class=\"label label-success\">Download</span></a>";}else{echo "Belum Upload";}?></td>
									</tr>																																				
								</table>	
							</div>							
						</div>
					</div>
				</div>				
			</div>			
		</div>
		<!--DATA KELUARGA-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card">
					<div class="body">				
						<h2 class="card-inside-title">Data Keluarga</h2>
						<div class="table responsive">
							<table class="table table-striped table-hover tabelsimple">
                                <thead>
                                   <tr>
                                   		<th width="50px">No</th>
                                   		<th width="200px">Hubungan</th>
                                    	<th width="300px">Nama</th>
                                        <th>Pekerjaan</th>
                                        <th>Tanggal Lahir</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                		$i=1;
                                		foreach ($keluarga as $val) {
                                			echo "<tr>";
                                				echo "<td>".$i."</td>";
                                				echo "<td>".$val->hubungan."</td>";
                                				echo "<td>".$val->anggota."</td>";
                                				echo "<td>".$val->pekerjaan."</td>";
                                				echo "<td>".$val->tgl_lahir."</td>";	
                                			echo "</tr>";
                                			$i++;
                                		}
                                	?>
                                </tbody>								
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--DATA RIWAYAT PENDIDIKAN-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card">
					<div class="body">				
						<h2 class="card-inside-title">Data Riwayat Pendidikan</h2>
						<div class="table responsive">
							<table class="table table-striped table-hover tabelsimple">
                                <thead>
                                   <tr>
                                   		<th width="50px">No</th>
                                   		<th width="200px">Jenjang</th>
                                    	<th width="300px">Sekolah</th>
                                        <th>Nomor Ijazah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php
                                		$i=1;
                                		foreach ($pendidikan as $val) {
                                			echo "<tr>";
                                				echo "<td>".$i."</td>";
                                				echo "<td>".$val->idjenjang."</td>";
                                				echo "<td>".$val->namasekolah."</td>";
                                				echo "<td>".$val->noijazah."</td>";	
                                			echo "</tr>";
                                			$i++;
                                		}
                                	?>
                                </tbody>								
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</section>