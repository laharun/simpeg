<section class="content">
	<div class="container-fluid">		
		<div class="block-header">
			<h2><?php echo $headline ?></h2>
		</div>
        <!--KONFIRMASI AKSI-->
        <?php
            if($this->session->flashdata('success')){
                echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
                ';
            }elseif($this->session->flashdata('error')){
                echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
                ';              
            }   
        ?> 
        <div class="row clearfix">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">people </i>
                    </div>
                    <div class="content">
                        <div class="text">Total Pegawai</div>
                        <div class="number count" data-from="0" data-to="<?php echo $jumpegawai ?>" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>                
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">tag_faces</i>
                    </div>
                    <div class="content">
                        <div class="text">Tenaga Kependidikan</div>
                        <div class="number count" data-from="0" data-to="<?php echo $jumtendik ?>" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div> 
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box bg-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">tag_faces</i>
                    </div>
                    <div class="content">
                        <div class="text">Tenaga Pendidik</div>
                        <div class="number count" data-from="0" data-to="<?php echo $jumpendidik?>"  data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>                                  
        </div>                       		
		<div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                     <div class="header bg-light-blue">
                        <h2>
                            <i class="material-icons">tag_faces</i> Pegawai <small>Daftar Pegawai</small>
                        </h2>
                    </div>
                    <div class="body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-col-pink" role="tablist">
                            <li role="presentation" class="active"><a href="#tendik" data-toggle="tab">Dosen/Tendik
                            </a></li>
                            <li role="presentation"><a href="#kependidikan" data-toggle="tab">Kependidikan</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="tendik">
                            <!--PEGAWAI TENDIK/DOSEN-->
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover tabelsimple" style="width:1200px">
                                        <thead>
                                            <tr>
                                                <th width="20px">No</th>
                                                <th width="20px">Id</th>
                                                <th>NIK</th>
                                                <th>Nama</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Email</th>
                                                <th class="text-center" width="150">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $i=1;
                                                foreach ($pegawai as $val) {
                                                    if(trim($val->kategori)=='pendidik'){
                                                        echo "<tr>";
                                                            echo "<td>".$i."</td>";
                                                            echo "<td>".$val->idpeg."</td>";
                                                            echo "<td>".$val->nik."</td>";
                                                            echo "<td>".ucwords($val->nama)."<br>";
                                                                if(!empty($val->homebase)){
                                                                    echo  "<label class='label label-success'>".$val->jenjang."-".$val->homebase."</label>";
                                                                }
                                                            echo "</td>";
                                                            echo "<td>".$val->idjeniskelamin."</td>";
                                                            echo "<td>".$val->email."</td>";
                                                            echo "<td class='text-center'>
                                                                <div class='btn-group'>
                                                                    <a href='#' link='".site_url('Admin/add_homebase')."' id='".$val->idpeg."'class='homebase btn btn-xs btn-warning waves-effect'><i class='material-icons'>class</i></a>
                                                                    <a href='".site_url('Admin/hapus_pegawai/'.$val->idpeg)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
                                                                    <a href='".site_url('Admin/edit_pegawai/'.md5($val->idpeg))."' class='btn btn-xs btn-info waves-effect'><i class='material-icons'>mode_edit</i></a>
                                                                    <a href='".site_url('Admin/login_as/'.$val->idpeg)."' class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>account_box</i></a>  
                                                                </div>
                                                            </td>";                                
                                                        echo "</tr>";
                                                        $i++;
                                                    }
                                                }
                                            ?>
                                        </tbody>                                            
                                    </table>
                                    <p>Keterangan :</p>
                                    <a href="#" class="btn btn-xs btn-warning waves-effect" style="width=25px"><i class='material-icons'>class</i></a> : Pilih Homebase
                                    <br>
                                    <a href="#" class="btn btn-xs btn-danger waves-effect" style="width=25px"><i class='material-icons'>delete</i></a> : Hapus Data
                                    <br>
                                    <a href="#" class="btn btn-xs btn-info waves-effect" style="width=25px"><i class='material-icons'>mode_edit</i></a> : Edit Data
                                    <br>
                                    <a href='#' class='btn btn-xs btn-warning waves-effect' style="width=25px"><i class='material-icons'>account_box</i></a> : Login Sebagai
                                </div>
                            </div>
                            <!--PEGAWAI KEPENDIDIKAN-->
                            <div role="tabpanel" class="tab-pane fade" id="kependidikan">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover tabelsimple" style="width:1200px">
                                        <thead>
                                            <tr>
                                                <th width="20px">No</th>
                                                <th>NIK</th>
                                                <th>Nama</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Email</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $i=1;
                                                foreach ($pegawai as $val) {
                                                    if(trim($val->kategori)=='kependidikan'){
                                                        echo "<tr>";
                                                            echo "<td>".$i."</td>";
                                                            echo "<td>".$val->nik."</td>";
                                                            echo "<td>".ucwords($val->nama)."</td>";
                                                            echo "<td>".$val->idjeniskelamin."</td>";
                                                            echo "<td>".$val->email."</td>";
                                                            echo "<td class='text-center'>
                                                                <div class='btn-group'>
                                                                    <a href='#' link='".site_url('Admin/add_homebase')."' id='".$val->idpeg."'class='homebase btn btn-xs btn-warning waves-effect'><i class='material-icons'>class</i></a>
                                                                    <a href='".site_url('Admin/hapus_pegawai/'.$val->idpeg)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
                                                                    <a href='".site_url('Admin/edit_pegawai/'.md5($val->idpeg))."' class='btn btn-xs btn-info waves-effect'><i class='material-icons'>mode_edit</i></a>
                                                                    <a href='".site_url('Admin/login_as/'.$val->idpeg)."' class='hide btn btn-xs btn-warning waves-effect'><i class='material-icons'>account_box</i></a>  
                                                                </div>
                                                            </td>";                                
                                                        echo "</tr>";
                                                        $i++;
                                                    }
                                                }
                                            ?>
                                        </tbody>                                            
                                    </table>
                                    <p>Keterangan :</p>
                                    <a href="#" class="btn btn-xs btn-warning waves-effect" style="width=25px"><i class='material-icons'>class</i></a> : Pilih Homebase
                                    <br>
                                    <a href="#" class="btn btn-xs btn-danger waves-effect" style="width=25px"><i class='material-icons'>delete</i></a> : Hapus Data
                                    <br>
                                    <a href="#" class="btn btn-xs btn-info waves-effect" style="width=25px"><i class='material-icons'>mode_edit</i></a> : Edit Data
                                    <br>
                                    <a href='#' class='btn btn-xs btn-warning waves-effect' style="width=25px"><i class='material-icons'>account_box</i></a> : Login Sebagai                              
                                </div>
                            </div>
                    </div>
                </div>               
           	</div>                      			
		</div>
		<!--SAMPAI DISINI-->
	</div>
    <!--DETAIL SEMINAR-->
    <div id="homebase" class="modal fade" role="dialog">
    </div>    
</section>