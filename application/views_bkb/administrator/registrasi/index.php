<section class="content">
	<div class="container-fluid">		
		<div class="block-header">
				<h2><?php echo $headline ?>
				<small>Pendaftaran pegawai ke sistem</small>
			</h2>
		</div>
        <!--KONFIRMASI AKSI-->
		<?php include 'notifikasi.php'; ?>                    		
		<div class="row clearfix">
            <div class="col-lg-12 col-md-12">
	            <div class="card">
                    <div class="header bg-red">
                        <h2>
                            <?= ucwords($headline)?>
                        </h2>
                    </div>	            
	                <div class="body">
	                	<form method="POST" action="<?= base_url($url)?>"  enctype="multipart/form-data">
							<div class="row">
								<div class="col-sm-6">
		                            <div class="form-group">
		                            	<label>Nik</label>
		                                <div class="form-line">
		                                    <input type="text" class="form-control" name="pegawai_nik" />
		                                </div>
		                                <i>Silahkan dikosongkan jika belum punya</i>
		                            </div>
		                            <div class="form-group">
		                            	<label>Kategori</label>
		                                <div class="form-line">
		                                    <select  required class="form-control show-tick" data-live-search="true" name="pegawai_kategori">
		                                    	<option value="" disabled="disabled" selected="selected">Pilih Kategori</option>
		                                    	<!---->
		                                    	<option value="kependidikan">Kependidikan</option>
		                                    	
		                                    	<option value="pendidik">Pendidik</option>
		                                    </select>
		                                </div>
		                            </div>	
		                            <div class="form-group">
		                            	<label>Username</label>
		                                <div class="form-line">
		                                    <input required type="text" class="form-control" name="pegawai_username"/>
		                                </div>
		                            </div>	
		                            <div class="form-group">
		                            	<label>Password</label>
		                                <div class="form-line">
		                                    <input required type="password" class="form-control" name="pegawai_password"/>
		                                </div>
		                            </div>		                            	                            	                            		                            
		                            <div class="form-group">
		                            	<label>Nama</label>
		                                <div class="form-line">
		                                    <input required type="text" class="form-control" name="pegawai_nama"/>
		                                </div>
		                            </div>	
									<div class="row">
										<div class="col-sm-5">
				                            <div class="form-group">
				                            	<label>Tempat dan Tanggal Lahir</label>
				                                <div class="form-line">
				                                    <input required type="text" class="form-control" name="pegawai_tempatlahir"/>
				                                </div>	                                
				                            </div>												
										</div>
										<div class="col-sm-7">
				                            <div class="form-group">
				                            	<label>&nbsp</label>
				                                <div class="form-line">
				                                    <input required type="text" class="datepicker form-control" name="pegawai_tgllahir"/>
				                                </div>	                                
				                            </div>												
										</div>										
									</div>	
									<div class="form-group">
		                            	<label>Jenis Kelamin</label><br>
			                                <input type="radio" id="laki" class="filled-in" value="1" name="pegawai_jeniskelamin">
			                                <label for="laki">Laki - laki</label>
			                                <input type="radio" id="perempuan" class="filled-in" value="0" name="pegawai_jeniskelamin">
			                                <label for="perempuan">Perempuan </label>			                                									
									</div>
		                            <div class="form-group">
		                            	<label>Agama</label>
		                                <div class="form-line">
		                                    <select  class="form-control show-tick" data-live-search="true" name="pegawai_agama">
		                                    	<option value="" disabled="disabled" selected="selected">Pilih Agama</option>
		                                    	<option value="Budha">Budha</option>
		                                    	<option value="Hindu">Hindu</option>
		                                    	<option value="Islam">Islam</option>
		                                    	<option value="Kristen">Kristen</option>
		                                    	<option value="Katholik">Katholik</option>
		                                    </select>
		                                </div>
		                            </div>
		                            <div class="form-group">
		                            	<label>Email</label>
		                                <div class="form-line">
		                                    <input required type="text" class="form-control" name="pegawai_email"/>
		                                </div>
		                            </div>	
		                            <div class="form-group">
		                            	<label>Alamat</label>
		                                <div class="form-line">
		                                    <textarea required name="pegawai_alamat" class="form-control" rows="4"></textarea>
		                                </div>
		                            </div>		                            		                            									                            								
								</div>
								<div class="col-sm-6">
		                            <div class="form-group">
		                            	<label>No HP</label>
		                                <div class="form-line">
		                                    <input required type="text" class="form-control" name="pegawai_nohp"/>
		                                </div>
		                            </div>
		                            <div class="form-group">
		                            	<label>Scan KTP</label>
		                            	<input required type="file" name="pegawai_ktp"/>
		                            	<i>Ukuran maksimal 2Mb, format jpg</i>
		                            </div>
		                            <div class="form-group">
		                            	<label>Foto</label>
		                            	<input required type="file" name="pegawai_foto"/>
		                            	<i>Ukuran maksimal 2Mb, format jpg</i>
		                            </div>	                            		                            
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
		                            <div class="form-group">
		                            	<button type="submit" value="submit" name="submit" class="btn btn-block btn-lg btn-primary">Simpan</button>
		                            </div>										
								</div>
							</div>	                		
	                	</form>
	               	</div>   	     
	            </card>                        
           	</div>                      			
		</div>
		<!--SAMPAI DISINI-->
	</div>  
</section>
<?php include 'action.php';?>