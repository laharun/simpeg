<div class="modal-dialog">  
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Update Homebase</h4>
    </div>
    <form method="POST" action="<?php echo site_url('Pegawai/update_homebase')?>">
    <div class="modal-body">  
      <label class="form-label">Idpeg</label>           
      <div class="form-group hide">
        <div class="form-line">
          <input type="text" class="form-control" value="<?php echo ucwords(trim($pegawai->idpeg))?>" name="idpeg">
        </div>
      </div>     
      <label class="form-label">Nama</label>           
      <div class="form-group">
        <div class="form-line">
          <input type="text" readonly class="form-control" value="<?php echo ucwords(trim($pegawai->nama))?>" name="nama">
        </div>
      </div>                     
      <div class="form-group">
        <p>
          <b>With Search Bar</b>
        </p>
        <select required class="form-control select show-tick" data-size="10" data-live-search="true"  title="Pilih Homebase" name="homebase">
          <?php
            foreach ($homebase as $val) {
              echo "<option value=".$val->kode.">".$val->jurusan." - ".$val->jenjang." Kode ".$val->kode."</option>";
            }
          ?>  
        </select>

      </div>                                   
    </div>                                                                         
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
    </div>
    </form>
  </div>
</div> 
