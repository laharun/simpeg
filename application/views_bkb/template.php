<!--FRONT END USER(DOSEN/PEGAWAI)-->
<?php
include(APPPATH.'views/coreUI/header.php');
?>
<style type="text/css">
    /* DISABLE A HREF*/
    .disabled {
    pointer-events: none;
    opacity:0.5;
    }
    .theme-red .navbar {
        background-color: #03A9F4;
    }    
</style>
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Mohon Tunggu...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo base_url('User')?>">SIMPEG</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                     -->
                    <!-- #END# Call Search -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">input</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <?php
                            //LOGIN AS
                            if($this->session->userdata('fromadmin')==true){
                                echo '
                                    <li><a href="'.site_url('login/fromadmin').'"><i class="material-icons">input</i>Admin</a></li>
                                ';
                            }else{
                                echo '
                                    <li><a href="'.site_url('login/logout').'"><i class="material-icons">input</i>Keluar</a></li>
                                ';                                
                            }
                            ?>
                        </ul>                                          
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php
                        $foto=$this->session->userdata('foto');
                        $res=file_exists(FCPATH.'./berkas/fileFoto'.$foto);
                        // if(file_exists('./berkas/fileFoto'.trim($foto))==true){
           
                        // }else{
                        //     echo '<img  class="img img-circle" src="'.base_url().'assets/images/avatar.png" width="50" height="50" alt="Foto Not Found" />';
                        //     echo $res;
                        // }
                        if(empty($foto)){
                            echo '<img  class="img img-circle" src="'.base_url().'assets/images/avatar.png" width="50" height="50" alt="Foto Not Found" />';
                        }else{
                            echo '<img class="img img-circle" src="'.base_url('berkas/fileFoto/'.$foto).'" width="50" height="50" alt="User" />';
                        }                        
                    ?>                
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b><?php echo ucwords($this->session->userdata('nama'))?></b></div>
                    <div class="email"><?php echo $this->session->userdata('email')?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <?php
                            //LOGIN AS
                            if($this->session->userdata('fromadmin')==true){
                                echo '
                                    <li><a href="#"><i class="material-icons">input</i>Admin</a></li>
                                ';
                            }else{
                                echo '
                                    <li><a href="'.site_url('Login/logout').'"><i class="material-icons">input</i>Keluar</a></li>
                                ';                                
                            }
                            ?>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li <?php if($menu=='profil'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User')?>" >
                            <i class="material-icons">home</i>
                            <span>Profil</span>
                        </a>
                    </li>
                    <li <?php if($menu=='datapribadi' OR $menu=='personalia' OR $menu=='datakeluarga' OR $menu=='riwayatpendidikan'){echo "class='active'";}?>>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">face</i>
                            <span>Data Diri</span>
                        </a>
                        <ul class="ml-menu">
                            <li <?php if($menu=='datapribadi'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('User/datadiri')?>" >
                                    <span>Data Personalia</span>
                                </a>
                            </li>
                            <li <?php if($menu=='personalia'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('User/personalia')?>" >
                                    <span>Data Diri</span>
                                </a>
                            </li>
                            <!---->                            
                            <li <?php if($menu=='datakeluarga'){echo "class='active'";}?>>    
                                <a href="<?php echo site_url('User/datakeluarga')?>">
                                    <span>Keluarga</span>
                                </a>
                            </li>
                            <li <?php if($menu=='riwayatpendidikan'){echo "class='active'";}?>>    
                                <a href="<?php echo site_url('User/riwayatpendidikan')?>">
                                    <span>Riwayat Pendidikan</span>
                                </a>
                            </li>
                            <!--
                            <li <?php if($menu=='kursus'){echo "class='active'";}?>>    
                                <a href="<?php echo site_url('User/kursus')?>">
                                    <span>Kursus</span>
                                </a>                                                                
                            </li>
                            -->
                        </ul>                        
                    </li>
                    <!--
                    <li <?php if($menu=='Keluarga'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User/keluarga')?>">
                            <i class="material-icons">people</i>
                            <span>Keluarga</span>
                        </a>
                    </li>                    
                    <li <?php if($menu=='pendidikan'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User/pendidikan')?>">
                            <i class="material-icons">local_library</i>
                            <span>Pendidikan</span>
                        </a>
                    </li>                    
                    <li <?php if($menu=='kursus'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User/kursus')?>">
                            <i class="material-icons">bookmark</i>
                            <span>Kursus</span>
                        </a>
                    </li>    
                    -->    
                    <li <?php if($menu=='kursus'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User/kursus')?>">
                            <i class="material-icons">explore</i>
                            <span>Kursus</span>
                        </a>
                    </li>
                    <li <?php if($menu=='karir'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('karir')?>">
                            <i class="material-icons">explore</i>
                            <span>Karir</span>
                        </a>
                    </li>                                                      
                    <li <?php if($menu=='mutasi'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User/kursus')?>">
                            <i class="material-icons">compare_arrows</i>
                            <span>Mutasi</span>
                        </a>
                    </li>
                    <div  class="<?= $this->session->userdata('level')=='99' ? 'hide':''?>">   
                    <li <?php if($menu=='jabatanfungsional'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User/jabatanfungsional')?>">
                            <i class="material-icons">card_membership</i>
                            <span>Jabatan Fungsional</span>
                        </a>
                    </li> 
                    </div>
                    <li <?php if($menu=='riwayatjabatan'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User/riwayatjabatan')?>">
                            <i class="material-icons">explore</i>
                            <span>Riwayat Jabatan</span>
                        </a>
                    </li>
                    <div class="<?= $this->session->userdata('level')=='2' ? 'hide':''?>">
             <!--        <li <?php if($menu=='serkom' OR $menu=='pelatihan' ){echo "class='active'";}?>>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">face</i>
                            <span>Sertifiaksi</span>
                        </a>
                        <ul class="ml-menu">
                            <li <?php if($menu=='serkom'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('serkom2')?>" >
                                    <span>Kompetensi</span>
                                </a>
                            </li>
                            <li <?php if($menu=='pelatihan'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('pelatihan2')?>" >
                                    <span>Pelatihan</span>
                                </a>
                            </li>                            
                        </ul>
                     </li>     -->                           
                    </div>
                    <div  class="<?= $this->session->userdata('level')=='99' ? 'hide':''?>">                       
                    <li <?php if($menu=='sitasi'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User/sitasi')?>">
                            <i class="material-icons">card_membership</i>
                            <span>Sitasi</span>
                        </a>
                    </li>                                  
                    <li <?php if($menu=='abdimas' OR $menu=='penunjang'OR $menu=='keanggotaanorganisasi' OR $menu=='hakcipta' OR $menu=='penulisanbuku' OR $menu=='riwayatmengajar' OR $menu=='jurnal' OR $menu=='penelitian' OR $menu=='serdos' OR $menu=='serkom' OR $menu=='seminarpemakalah'){echo "class='active'";}?>>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">assignment_turned_in</i>
                            <span>Data Penunjang</span>
                        </a>
                        <ul class="ml-menu">
                            <li <?php if($menu=='serdos'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('Serdos')?>">
                                    <span>Serdos</span>
                                </a>
                            </li>
                            <li <?php if($menu=='serkom'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('Serkom')?>">
                                    <span>Serkom</span>
                                </a>
                            </li>
                            <li <?php if($menu=='pelatihan'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('pelatihan2')?>" >
                                    <span>Pelatihan</span>
                                </a>
                            </li> 
                            <li <?php if($menu=='abdimas'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('User/abdimas')?>">
                                    <span>Abdimas</span>
                                </a>
                            </li>
                            <li <?php if($menu=='penelitian'){echo "class='active'";}?>>    
                                <a href="<?php echo site_url('User/penelitian')?>">
                                    <span>Penelitian</span>
                                </a>
                            </li>  
                            <li <?php if($menu=='seminarpemakalah'){echo "class='active'";}?>>  
                                <a href="<?php echo site_url('User/seminarpemakalah')?>">
                                    <span>Seminar Penelitian</span>
                                </a>
                            </li>
                            <li <?php if($menu=='jurnal'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('User/jurnal')?>">
                                    <span>Tulisan Jurnal</span>
                                </a>
                            </li>
                            <li <?php if($menu=='riwayatmengajar'){echo "class='active'";}?>>    
                                <a href="<?php echo site_url('User/riwayatmengajar')?>">
                                    <span>Riwayat Mengajar</span>
                                </a>
                            </li>
                            <li <?php if($menu=='penulisanbuku'){echo "class='active'";}?>>    
                                <a href="<?php echo site_url('User/penulisanbuku')?>">
                                    <span>Penulisan Buku</span>
                                </a>
                            </li>
                            <li <?php if($menu=='hakcipta'){echo "class='active'";}?>>    
                                <a href="<?php echo site_url('User/hakcipta')?>">
                                    <span>Kekayaan Intelektual</span>
                                </a> 
                            </li>
                            <li <?php if($menu=='keanggotaanorganisasi'){echo "class='active'";}?>>    
                                <a href="<?php echo site_url('User/keanggotaanorganisasi')?>">
                                    <span>Keanggotaan Organisasi</span>
                                </a>
                            </li>
                            <li <?php if($menu=='penunjang'){echo "class='active'";}?>>    
                                <a href="<?php echo site_url('User/penunjang')?>">
                                    <span>Penunjang</span>
                                </a>                                                                 
                            </li>
                        </ul>                        
                    </li>
                    </div>
                    <li <?php if($menu=='password'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('User/password')?>">
                            <i class="material-icons">lock</i>
                            <span>Password</span>
                        </a>
                    </li>  
                    <li <?php if($menu=='notfound'){echo "class='active'";}?>>
   
                    </li>                                                                                                                     
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 <a href="javascript:void(0);">p3si@akprind.ac.id</a>.
                </div>
                <div class="version">
                    <?= 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>'?><br>
                     <b>Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <!--CORE UNTUK TAMPILAN KONTEN ADA DIBAWAH-->
    <?php
		$menu=$menu;
		switch ($menu) {
            //---------------------------------PROFIL-------------------------------
			case 'profil':
				include(APPPATH.'views/dashboard/dashboard.php');
				break;
            //---------------------------------DATA PERSONALIA-----------------------    
			case 'datapribadi':
				include(APPPATH.'views/datadiri/datadiri.php');
				break;	
            case 'personalia':
                include(APPPATH.'views/personalia/personalia.php');
                break;                  
			case 'datakeluarga':
				include(APPPATH.'views/keluarga/datakeluarga.php');
				break;
			case 'riwayatpendidikan':
				include(APPPATH.'views/riwayatpendidikan/riwayatpendidikan.php');
				break;	
			case 'kursus':
				include(APPPATH.'views/kursus/kursus.php');
				break;
            case 'mutasi';
                include(APPPATH.'views/mutasi/mutasi.php');
                break; 
            case 'jabatanfungsional';
                include(APPPATH.'views/jafa/jafa.php');
                break; 
            case 'riwayatjabatan';
                include(APPPATH.'views/riwayatjabatan/riwayatjabatan.php');
                break; 
            case 'sitasi';
                include(APPPATH.'views/sitasi/index.php');
                break;                 
            //-------------------------------TRIDHARMA------------------------------
            case 'serdos';
                include(APPPATH.'views/serdos/index.php');
                break;
            case 'serkom';
                include(APPPATH.'views/serkom/index.php');
                break;                             
            case 'abdimas';
                include(APPPATH.'views/abdimas/abdimas.php');
                break;  
            case 'penelitian';
                include(APPPATH.'views/penelitian/penelitian.php');
                break;
            case 'seminarpemakalah';
                include(APPPATH.'views/seminar/seminar.php');
                break; 
            case 'jurnal';
                include(APPPATH.'views/jurnal/jurnal.php');
                break; 
            case 'riwayatmengajar';
                include(APPPATH.'views/riwayatmengajar/riwayatmengajar.php');
                break;     
            case 'penulisanbuku';
                include(APPPATH.'views/penulisanbuku/penulisanbuku.php');
                break;
            case 'hakcipta';
                include(APPPATH.'views/hakcipta/hakcipta.php');
                break; 
            case 'karir';
                include(APPPATH.'views/karir/index.php');
                break;                 
            case 'keanggotaanorganisasi';
                include(APPPATH.'views/keanggotaanorganisasi/keanggotaanorganisasi.php');
                break; 
            case 'penunjang';
                include(APPPATH.'views/penunjang/penunjang.php');
                break;  
            //----------------------------PASSWORD----------------------
            case 'password';
                include(APPPATH.'views/password/password.php');
                break; 
            case 'notfound';
                include(APPPATH.'views/notfound.php');
                break;											
			default:
				redirect(site_url('Login/logout'));
				break;
		}
    ?>
<?php
include(APPPATH.'views/coreUI/footer.php');
?>    






