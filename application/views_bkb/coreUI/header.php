<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Informasi Kepegawaian</title>
    <!-- Favicon-->
    <link rel="icon" href="#" type="image/x-icon">
    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.css')?>" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Waves Effect Css -->
    <link href="<?php echo base_url()?>assets/plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="<?php echo base_url()?>assets/plugins/animate-css/animate.css" rel="stylesheet" />  
    <!-- Sweetalert Css -->
    <link href="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />  
    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Wait Me Css -->
    <link href="<?php echo base_url()?>assets/plugins/waitme/waitMe.css" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url()?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />    
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url()?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
    <!-- Datepicker -->
    <link href="<?= base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />    
    <!-- Custom Css -->
    <link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url()?>assets/css/themes/all-themes.css" rel="stylesheet" />  
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url()?>assets/plugins/jquery/jquery.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.validate.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url()?>assets/plugins/bootstrap/js/bootstrap.js"></script>
    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- Select Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script> 
    <!-- Moment Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/momentjs/moment.js"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <script src="<?php echo base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  
    <!--Price Format-->
    <script src="<?php echo base_url()?>assets/plugins/priceformat/jquery.priceformat.min.js"></script>     
    <!-- Jquery DataTable Plugin Js --> 
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/pages/tables/jquery-datatable.js"></script>
    <!---->
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <!-- SweetAlert Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>    
    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/node-waves/waves.js"></script>  
    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/jquery-countto/jquery.countTo.js"></script> 
    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/jquery-validation/jquery.validate.js"></script>
    <!-- Validasi FORM -->
    <script src="<?php echo base_url()?>assets/plugins/jquery-validation/validasiform.js"></script>         
</head>
