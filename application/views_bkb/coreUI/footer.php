    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>   
    <!-- Custom Js -->
    <script src="<?php echo base_url()?>assets/js/admin.js"></script>    
    <!-- Demo Js -->
    <script src="<?php echo base_url()?>assets/js/demo.js"></script>   
</body>   
<script>
    $(document).ready(function(){
        $('.count').countTo();
        //--------------PRICE FORMAT----------------
        $(".price").priceFormat({
          prefix:'Rp ',
          thousandsSeparator:'.',
          centsLimit:'0',
        });
        //---------------DATE PICKER-----------------
        $('.datepicker').bootstrapMaterialDatePicker({
          time:false,
          format: "DD-MM-YYYY",
        });
        $('.datepicker2').bootstrapMaterialDatePicker({
            format: "YYYY",
            //year:true,
            time:false,
            //date:true,
            //monthPicker:true
        });        
        //---------------TOMBOL ADD------------------
        $(".tomboltambah").click(function(){
            $(".forminput").toggle();
            $("#tampildata").toggle();
            $("#edit_card").hide();            
        });
        $(".tutup").click(function(){
            $(".forminput").fadeToggle();
        });
        //----------------AJAX EDIT/DETAIL---------------
        $('.homebase').click(function(){
          var link=$(this).attr('link');
          var idpeg=$(this).attr('id');
          // alert(link);
          $.ajax({
            method:"POST",
            url:link,
            data:{idpeg:idpeg},
            success:function(ajaxData){
              $("#homebase").html(ajaxData);
              $("#homebase").modal("show",{backdrop:'true'});
              $('.select').selectpicker({
                size: 4
              });             
            }
          })
        });
        //----------------ALERT HAPUS---------------
        $('.hapus').click(function(){
          var link=$(this).attr('href');
          //alert(link);
          swal({
            title:'Perhatian',
            type: "warning",
            text:'Hapus Data ?',
            html:true,
            confirmButtonColor:'#d9534f',
            showCancelButton:true,
          },function(){
            window.location.href=link
          });
          return false
        });

        //---------------EDIT NON MODAL----------------------
        $(".edit_data").click(function(){
          var id=$(this).attr('id');
          var link=$(this).attr('link');
          //alert(link);
          $.ajax({
            type:'POST',
            url:link,
            data:{id:id},
            success:function(data){
              $(".forminput").hide(); 
              $("#tampildata").hide();             
              $("#edit_card").toggle();
              $("#edit_form").html(data);
              $('.datepicker').bootstrapMaterialDatePicker({
                time:false,
              });  
              $('select:not(.ms)').selectpicker(); 
              $('.form-control').focus(function () {
                  $(this).parent().addClass('focused');
              });
              //On focusout event
              $('.form-control').focusout(function () {
                  var $this = $(this);
                  if ($this.parents('.form-group').hasClass('form-float')) {
                      if ($this.val() == '') { $this.parents('.form-line').removeClass('focused'); }
                  }
                  else {
                      $this.parents('.form-line').removeClass('focused');
                  }
              });
              //On label click
              $('body').on('click', '.form-float .form-line .form-label', function () {
                  $(this).parent().find('input').focus();
              });
              //Not blank form
              $('.form-control').each(function () {
                  if ($(this).val() !== '') {
                      $(this).parents('.form-line').addClass('focused');
                  }
              });                                       
            }
          });
          return false;
        })
        //---------------EDIT WITH MODAL---------------------
        $(".edit").click(function(){
          var id=$(this).attr('id');
          var link=$(this).attr('link');
          //alert(link);
          $.ajax({
            type:'POST',
            url:link,
            data:{id:id},
            success:function(data){
              $("#edit").html(data);
              $("#edit").modal('show',{backdrop:'true'});
              $("#edit_password").validate({
                  rules:{
                      username:{
                          minlength:4,
                      },
                      password:{
                          minlength:4,
                      },
                      ulangipassword:{
                          minlength:4,
                          equalTo: "#editpassword",
                      }
                  },
                  messages:{
                      username:{
                          minlength:"minimal 4 karakter",
                      },
                  }, 
                  highlight: function (input) {
                      $(input).parents('.form-line').addClass('error').removeClass('success');
                  },
                  unhighlight: function (input) {
                      $(input).parents('.form-line').removeClass('error').addClass('success');
                  },
                  errorPlacement: function (error, element) {
                      $(element).parents('.form-group').append(error);
                  }        
              });              
            }
          });
          return false;
        }); 
        //---------------DETAIL WITH MODAL---------------------
        $(".detail").click(function(){
          var id=$(this).attr('id');
          var url=$(this).attr('url');
          //alert(url);
          $.ajax({
            type:'POST',
            url:url,
            data:{id:id},
            success:function(data){
              $("#detail").html(data);
              $("#detail").modal('show',{backdrop:'true'});             
            }
          });
          return false;
        });     
        //---------------TUTUP CARD--------------------------
        $(".tutup_input").click(function(){
          $(".forminput").hide(); 
          $("#tampildata").show();
        }) 
        //---------------TABEL----------------------
        $(".tabelsimple").DataTable({
          "paging": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });  
      $("#hubungan").change(function(){
        var dt=$(this).val();
        if(dt=="Anak Kandung"){
          $('#anakke').show();
          $('#tglmenikah').hide();
        }else if((dt=='Istri')||(dt=='Suami')){
          //alert('istri / suami');
          $('#tglmenikah').show();
          $('#anakke').hide();
        }else{
          //alert('lainnya');
          $('#anakke').hide();
          $('#tglmenikah').hide();
        }
      })                         
    });
</script>
</html>
