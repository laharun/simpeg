<form  enctype="multipart/form-data" action="<?php echo site_url('Kursus/edit')?>" method="POST" >
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group hide">
				<div class="form-line">
					<b>Id</b>
					<input type="text" class="form-control" name="id" value="<?php echo $data->id?>">
				</div>				
			</div>
        	<div class="form-group form-float">
                <div class="form-line">
                    <input type="text" class="form-control" name="tahun" value="<?= $data->tahun?>" />
                    <label class="form-label">Tahun</label>
                 </div>
        	</div>								
            <div class="form-group form-float">
                <div class="form-line">
                    <textarea class="form-control no-resize" name="kursus" rows="4"><?= $data->namakursus?></textarea>
                    <label class="form-label">Kursus/Keterampilan</label>
                 </div>
            </div>
            <div class="form-group form-float">
               	<div class="form-line">
                    <input type="text" class="form-control" name="penyelenggara" value="<?= $data->penyelenggara?>" />
                    <label class="form-label">Penyelenggara/lembaga</label>
                </div>
            </div>
        	<div class="form-group">
                <p>
                    <b>Level</b>
                </p>
                <select name="level" class="form-control show-tick" data-live-search="true">
                    <option <?= $data->level=="Lanjut" ? 'selected':'';?>>Lanjut</option>
                    <option <?= $data->level=="Menengah" ? 'selected':'';?>>Menengah</option>
                    <option <?= $data->level=="Dasar" ? 'selected':'';?>>Dasar</option>
                </select>	
        	</div>		        																			
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-warning btn-lg waves-effect btn-block" name="submit" value="submit">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
           $("#edit_card").hide();
    })
</script>