<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>JABATAN FUNGSIONAL</h2>
		</div>
		<!--KONFIRMASI AKSI-->
		<?php
			if($this->session->flashdata('success')){
				echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
				';
			}elseif($this->session->flashdata('error')){
				echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
				';				
			}	
		?>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-2">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>
		<!--TAMBAH DATA FORM-->					
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput" style="display:none">
					<div class="body">
						<form action="<?php echo site_url('Jafa/simpan_data')?>" method="POST"  enctype="multipart/form-data">
							<h2 class="card-inside-title">Riwayat Jabatan Fungsional Pegawai</h2>
							<div class="row clearfix">
								<div class="col-sm-12">
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="jafa" required>
											<label class="form-label">Nama Jabatan Fungsional</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="angka_kredit" required>
											<label class="form-label">Angka Kredit</label>
										</div>
									</div>
									<div class="form-group">
										<div class="form-line">
											<b>
												Tanggal Mulai Ditetapkan
											</b>
											<input required type="text" class="form-control datepicker" name="tmt">
										</div>
									</div>
									<h2 class="card-inside-title">Upload </h2>
									<div class="form-group form-float">
										<div class="">
											<input type="file" name="fileupload" required >
											<p class="help-block col-red">Ukuran Maksimal 5mb</p>
										</div>
									</div>																			
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
								</div>
							</div>							 	
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--TABEL DATA-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header">
		                <h2>
                           Data Jabatan Fungsional
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!--
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                    -->
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple " style="width:1200px">
										<thead>
											<tr>
												<th width="15px">No</th>
												<th>Jabatan</th>
												<th>Angka Kreadit</th>
												<th>Tanggal</th>
												<th class="text-center">Option</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$i=1;
												foreach ($jafa as $val) {
													echo "<tr>";
														echo "<td>".$i."</td>";
														echo "<td>".$val->namajafa."</td>";
														echo "<td>".$val->angkakredit."</td>";
														echo "<td>".date('d-m-Y',strtotime($val->tmt))."</td>";
													echo "<td class='text-center'>
														<div class='btn-group'>
															<a href='".site_url('Jafa/hapus_data/'.$val->id)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
															<a href='".site_url('Jafa/download/'.$val->file)."' class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>archive</i></a> 
														</div>
													</td>";;
													echo "</tr>"; 
													$i++;
												}
											?>
										</tbody>
									</table>
									<p>Keterangan :</p>
									<a href='#' style="width:30px" class='btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> : Tombol Hapus <br>
									<a href='#' style="width:30px" class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>archive</i></a> : Download <br>										
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>			
	</div>
</section>