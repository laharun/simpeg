<!--
//////////////////////////////////////////BACKEND UNTUK ADMIN///////////////////////////////////////////////
-->
<?php
include(APPPATH.'views/coreUI/header.php');
?>
<style type="text/css">
    .disabled {
    pointer-events: none;
    opacity:0.5;
    }
    .theme-red .navbar {
        background-color: #03A9F4;
    }     
</style>
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Mohon Tunggu...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo base_url()?>">SIMPEG</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                     -->
                    <!-- #END# Call Search -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">line_weight</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?php echo site_url('login/logout')?>"><i class="material-icons">input</i>Keluar</a></li>
                        </ul>                                          
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php
                        $foto=$this->session->userdata('foto');
                        $res=file_exists('http://personalia.akprind.ac.id/simpeg/fileFoto/'.$foto);
                        if(file_exists('http://personalia.akprind.ac.id/simpeg/fileFoto/'.$foto)){
                            // if(empty($foto)){
                            //     echo '<img  class="img img-circle" src="'.base_url().'assets/images/avatar.png" width="50" height="50" alt="Foto Not Found" />';
                            //  }else{
                                
                            // }   
                            echo '<img class="img img-circle" src="http://personalia.akprind.ac.id/simpeg/fileFoto/'.$foto.'" width="50" height="50" alt="User" />';        
                        }else{
                            echo '<img  class="img img-circle" src="'.base_url().'assets/images/avatar.png" width="50" height="50" alt="Foto Not Found" />';
                        }
                    ?>                
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b><?php echo ucwords($this->session->userdata('username'))?></b></div>
                    <div class="email"><?php echo $this->session->userdata('email')?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="<?php echo site_url('login/logout')?>"><i class="material-icons">input</i>Keluar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li <?php if($menu=='dashboard'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('Admin')?>" >
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li <?php if($menu=='pegawai' || $menu=='edit_pegawai'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('Admin/pegawai')?>">
                            <i class="material-icons">compare_arrows</i>
                            <span>Pegawai</span>
                        </a>
                    </li>                 
                    <li class="<?php if($headline=='Pendaftaran Pegawai' || $headline=='Masa Bakti'){echo "active";}?>">
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">assignment_turned_in</i>
                            <span>Personalia</span>
                        </a>
                        <ul class="ml-menu">
                            <li <?php if($headline=='Pendaftaran Pegawai'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('Registrasi')?>">
                                    <span>Pendaftaran Pegawai</span>
                                </a>
                            </li>                        
                            <li <?php if($headline=='Masa Bakti'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('Masabakti')?>">
                                    <span>Masa Bakti</span>
                                </a>
                            </li>
                        </ul>                        
                    </li>
                    <li <?php if($headline=='serkom' || $headline=='serdos' ){echo "class='active'";}?>>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">face</i>
                            <span>Data Penunjang</span>
                        </a>
                        <ul class="ml-menu">
                            <li <?php if($headline=='serkom'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('serkomadmin')?>" >
                                    <span>Serkom</span>
                                </a>
                            </li>
                            <li <?php if($headline=='serdos'){echo "class='active'";}?>>
                                <a href="<?php echo site_url('serdosadmin')?>" >
                                    <span>serdos</span>
                                </a>
                            </li>                                
                        </ul>
                    </li>  
<!--                     <li <?php if($menu=='password'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('Admin/password_user')?>">
                            <i class="material-icons">lock</i>
                            <span>Password</span>
                        </a>
                    </li> -->                                     
                    <li <?php if($menu=='password'){echo "class='active'";}?>>
                        <a href="<?php echo site_url('Admin/password_user')?>">
                            <i class="material-icons">lock</i>
                            <span>Password</span>
                        </a>
                    </li>                   
                    <li>
                        <a href="<?php echo site_url('Admin/panduan')?>">
                            <i class="material-icons">book</i>
                            <span>Panduan</span>
                        </a>
                    </li>                                                                                                                      
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 <a href="javascript:void(0);">p3si@akprind.ac.id</a>.
                </div>
                <div class="version">
                    <?= 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>'?><br>
                     <b>Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <?php
		$menu=$menu;
		switch ($menu) {
            //---------------------------------PROFIL-------------------------------
			case 'dashboard':
				include(APPPATH.'views/administrator/dashboard.php');
				break;
            //-----------------------------DATA PEGAWAI-----------------------------    
			case 'pegawai':
				include(APPPATH.'views/administrator/pegawai.php');
				break;																		
            case 'edit_pegawai':
                include(APPPATH.'views/administrator/edit_pegawai.php');
                break;
            case 'personalia':
                if($submenu=='masabakti'){
                    include(APPPATH.'views/administrator/masabakti/index.php');
                }elseif($submenu=='registrasi'){
                    include(APPPATH.'views/administrator/registrasi/index.php');
                }else{
                    echo 'Halaman tidak ditemukan';
                }
            break;
            case 'datapenunjang':
                if($submenu=='serdos'){
                    include(APPPATH.'views/serdosadmin/index.php');
                    //include(APPPATH.'views/serkomadmin/index.php');
                }elseif($submenu=='serkom'){
                    include(APPPATH.'views/serkomadmin/index.php');
                }else{
                    echo 'Halaman tidak ditemukan';
                    exit();
                } 
             break;                  
            //----------------------------PASSWORD----------------------------------
            case 'password':
                include(APPPATH.'views/administrator/tampil_password.php');
                break;                
            default:
				// redirect(site_url('login/logout'));
                echo "View not found";
                break;
        }
    ?>
<!--AJAX-->
<div id="edit" class="modal fade"  tabindex="-1" role="dialog">
    <!--MENAMPILKAN KODE AJAX DISINI-->
</div>    
<?php
include(APPPATH.'views/coreUI/footeradmin.php');
?>    






