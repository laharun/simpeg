<section class="content">
	<div class="container-fluid">
        <!--KONFIRMASI AKSI-->
        <?php
            if($this->session->flashdata('success')){
                echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
                ';
            }elseif($this->session->flashdata('error')){
                echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
                ';              
            }   
        ?>	
		<div class="block-header">
			<h2>DATA DIRI</h2>
		</div>
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12">
				<div class="card">
					<div class="body">
						<form id="formdatadiri" method="POST" action="<?php echo site_url('Pegawai/update_datadiri')?>"  enctype="multipart/form-data">
							<h2 class="card-inside-title">Data Diri Pegawai</h2><hr>
							<div class="row clearfix">
								<div class="col-sm-12">
	                                <div class="form-group form-float">
	                                    <div class="form-line">
	                                        <input type="text" class="form-control" name="nama" value="<?php echo ucwords(trim($personalia->nama))?>" />
	                                     	<label class="form-label ">Nama</label>
	                                     </div>
	                                </div>								
									<div class="form-group">
										<div class="form-line">
											<p>
												<b>Tanggal Lahir</b>
											</p>
											<input type="text" class="form-control datepicker" name="tgllahir" value="<?php echo date('Y-m-d',strtotime($personalia->tanggallahir))?>">
										</div>
									</div> 
	                            	<div class="form-group">
	                            		<b>Tampat Lahir</b>
	                            		<div class="form-line">
	                            			<textarea type="text" class="form-control" name="tempatlahir" rows="2"><?php echo $personalia->tempatlahir?></textarea>
	                            		</div>
	                            	</div>									                           		                                     							
									<div class="form-group form-float">
										<b>Agama</b>
                                        <select required class="form-control show-tick" data-live-search="true" name="agama">
                                            <option <?php if(trim($personalia->idagama)=='Budha'){echo "selected";}?> value="Budha" >Budha</option>
                                            <option <?php if(trim($personalia->idagama)=='Islam'){echo "selected";}?> value="Islam">Islam</option>
                                            <option <?php if(trim($personalia->idagama)=='Hindu'){echo "selected";}?> value="Hindu">Hindu</option>
                                            <option <?php if(trim($personalia->idagama)=='Katolik'){echo "selected";}?> value="katolik">Katolik</option>
                                            <option <?php if(trim($personalia->idagama)=='Kristen'){echo "selected";}?> value="Kristen">Kristen</option>
                                        </select>										
									</div>
									<div class="form-group form-float">
										<b>Golongan Darah</b>
                                        <select required class="form-control show-tick" data-live-search="true" name="goldarah">
                                            <option <?php if(trim($personalia->idgoldarah)=='A'){echo "selected";}?> value="A" >A</option>
                                            <option <?php if(trim($personalia->idgoldarah)=='B'){echo "selected";}?> value="B">B</option>
                                            <option <?php if(trim($personalia->idgoldarah)=='O'){echo "selected";}?> value="O">O</option>
                                            <option <?php if(trim($personalia->idgoldarah)=='AB'){echo "selected";}?> value="AB">AB</option>
                                        </select>											
									</div>								
									<div class="form-group form-float">
										<b>Status</b>
                                        <select required class="form-control show-tick" data-live-search="true" name="statusnikah">
                                            <option <?php if(trim($personalia->statusnikah)=='Belum Nikah'){echo "selected";}?> value="Belum Nikah" >Belum Nikah</option>
                                            <option <?php if(trim($personalia->statusnikah)=='Nikah'){echo "selected";}?> value="Nikah">Nikah</option>
                                            <option <?php if(trim($personalia->statusnikah)=='Janda/Duda'){echo "selected";}?> value="Janda/Duda">Janda/Duda</option>
                                            <option <?php if(trim($personalia->statusnikah)=='Cerai Mati'){echo "selected";}?> value="Cerai Mati">Cerai Mati</option>
                                        </select>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="email" value="<?php echo $personalia->email?>">
											<label class="form-label">Email</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="noktp" value="<?php echo $personalia->noktp?>">
											<label class="form-label">Nomor KTP</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="nokk" value="<?php echo trim($personalia->nokk)?>">
											<label class="form-label">Nomor KK</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="kodepos" value="<?php echo trim($personalia->kodepos)?>">
											<label class="form-label">Kode POS</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="kota" value="<?php echo trim($personalia->kota)?>">
											<label class="form-label">Kota</label>
										</div>
									</div>
									<div class="form-group form-float">
										<div class="form-line">
											<input type="text" class="form-control" name="nohp" value="<?php echo trim($personalia->nohp)?>">
											<label class="form-label">Nomor Tlp</label>
										</div>
									</div>																																																																				
								</div>
							</div>
	                        <h2 class="card-inside-title">Data Akademik</h2><hr>	
	                        <div class="row clearfix">
	                        	<div class="col-sm-6">
	                        		<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input type="text" class="form-control" name="gelardepan" value="<?php echo $personalia->gelardepan?>">
	                            			<label class="form-label ">Gelar Depan</label>
	                            		</div>	                        			
	                        		</div>
	                        		<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input type="text" class="form-control" name="gelarbelakang" value="<?php echo $personalia->gelarbelakang?>">
	                            			<label class="form-label ">Gelar Belakang</label>
	                            		</div>	                        			
	                        		</div>	                        		
	                        	</div>
	                        	<div class="col-sm-6">
	                        		<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input type="text" class="form-control" name="prodi" value="<?php echo $personalia->prodi?>">
	                            			<label class="form-label ">Prodi Lulusan</label>
	                            		</div>	                        			
	                        		</div>
	                        		<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input type="text" class="form-control" name="tahunprodi" value="<?php echo $personalia->tahunprodi?>">
	                            			<label class="form-label">Prodi Lulusan</label>
	                            		</div>	                        			
	                        		</div>	                        			                        		
	                        	</div>
	                        </div>								
	                        <h2 class="card-inside-title">Berkas Upload</h2><hr>
	                        <div class="row clearfix">    	
	   							<div class="col-sm-6">
		                        	<div class="form-group">
		                        		<b>Upload SK Masuk</b>
	                                    <input type="file" name="fileskmasuk">
	                                    <p class="pull-left help-info col-red">Ukuran Maksimal 10mb, dengan format PDF</p>
	                                </div>
	                                <input type="text" class="form-control hide" name="fileskmasuklama" value="<?php echo $personalia->fileskmasuk?>">
		                        	<div class="form-group">
		                        		<b>Upload SK Tetap</b>
	                                    <input type="file" name="filesktetap">
	                                    <p class="pull-left help-info col-red">Ukuran Maksimal 10mb, dengan format PDF</p> 
	                                </div>
	                                <input type="text" class="form-control hide" name="filesktetaplama" value="<?php echo $personalia->filesktetap?>">
		                        	<div class="form-group">
		                        		<b>Upload KTP</b>
	                                    <input type="file" name="filektp">
	                                    <p class="pull-left help-info col-red">Ukuran Maksimal 10mb, dengan format PDF</p>
	                                </div>
	                                <input type="text" class="form-control hide" name="filektplama" value="<?php echo $personalia->filektp?>">
		                        	<div class="form-group">
		                        		<b>Upload KK</b>
	                                    <input type="file" name="filekk">
	                                    <p class="pull-left help-info col-red">Ukuran Maksimal 10mb, dengan format PDF</p>
	                                </div>
	                                <input type="text" class="form-control hide" name="filekklama" value="<?php echo $personalia->filekk?>">	                                	                                	                                									  
	                            </div>                                                                                                                                                                  
	                        </div>
	                        <div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg m-t-15 waves-effect btn-block">Update</button>
								</div>
	                        </div>						
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>