<form action="<?= base_url('riwayatpendidikan/edit_data')?>" method="POST" enctype="multipart/form-data">
	<h2 class="card-inside-title">Riwayat Pendidikan</h2>
	<div class="row clearfix">
		<div class="col-sm-12">
            <div class="form-group form-float">
               	<div class="form-line">
                    <input type="text" class="form-control" name="id" value="<?= trim($data->idriwayatpen)?>" />
                    <label class="form-label">Id</label>
                </div>
            </div>								
            <div class="form-group form-float ">
                <div class="form-line">
                    <select required name="idjenjang" class="form-control show-tick" data-size="5" data-live-search="true" title="Jenjang">
                    	<option value="SD" <?= trim($data->idjenjang)=='SD' ? 'selected':'';?>>SD</option>
                    	<option value="SMP" <?= trim($data->idjenjang)=='SMP' ? 'selected':'';?>>SMP</option>
                    	<option value="SMA/SMK" <?= trim($data->idjenjang)=='SMA/SMK' ? 'selected':''?>>SMA/SMK</option>
                    	<option value="D3" <?= trim($data->idjenjang)=='D3' ? 'selected':'';?>>D3</option>
                    	<option value="S1" <?= trim($data->idjenjang)=='S1' ? 'selected':'';?>>S1</option>
                    	<option value="S2" <?= trim($data->idjenjang)=='S2' ? 'selected':'';?>>S2</option>
                    	<option value="S3" <?= trim($data->idjenjang)=='S3' ? 'selected':'';?>>S3</option>
                    </select>	                                        
                    
                 </div>
            </div>
            <div class="form-group form-float">
               	<div class="form-line">
                    <input type="text" class="form-control" name="namasekolah" value="<?= trim($data->namasekolah)?>" />
                    <label class="form-label">Asal Sekolah</label>
                </div>
            </div>
            <div class="form-group form-float">
               	<div class="form-line">
                    <input type="text" class="form-control" name="namajur" value="<?=trim($data->namajur)?>"/>
                    <label class="form-label">Jurusan/Prodi</label>
                </div>
            </div>
        	<div class="form-group form-float" id="anak">
        		<div class="form-line">
        			<input type="text" class="form-control" name="noijazah" value="<?= trim($data->noijazah)?>" />
        			<label class="form-label">Nomor Ijazah</label>
        		</div>
        	</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input type="text" class="datepicker form-control" name="tanggalijazah" value="<?= $data->tanggalijazah?>" />
        			<label class="form-label">Tanggal Ijazah</label>
        		</div>
        	</div>
        	<div class="form-group form-float">
				<input type="file" name="fileijazah" />
        		<p class="help-block">Format PDF, ukuran max 5 mb</p>
        	</div>	                            			                            		                            	                            	 									
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" name="submit" value="submit" class="btn btn-primary btn-lg  m-t-15 waves-effect btn-block">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>	
	</div>
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
           $("#edit_card").hide();
    })
</script>