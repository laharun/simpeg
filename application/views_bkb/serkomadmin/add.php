<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="card forminput" style="display:none">
        <div class="header bg-light-blue">
            <h2>
                Form Sertifikat Kompetensi Dosen
            </h2>
        </div>        
		<div class="body">
			<form  action="<?php echo base_url('serkomadmin')?>" method="POST" enctype="multipart/form-data">
				<div class="row clearfix">
					<div class="col-sm-12">							
						<div class="form-group">
                            <p>
                                <b>Tahun Akademik</b>
                            </p>
                            <select required name="serkom_tahunakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                            	<?php
                            		foreach ($thnakademik as $value) {
                            			echo "<option>".$value->tahun."</option>";
                            		}
                            	?>
                            </select>
						</div>
                        <div class="form-group">
                            <label>Nama Pegawai</label>
                            <div class="form-line">
                                <select class="form-control" name="serkom_idpegawai" data-size="10" data-live-search="true">
                                    <?php foreach($pegawai AS $row):?>
                                        <option value="<?=$row->idpeg?>"><?=$row->nama?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Tahun Keluar</label>
                        	<div class="form-line">
                        	<input required type="text" name="serkom_tahunkeluar" class="form-control datepicker2">
                        	
                        	</div>

                        </div>
                        <div class="form-group">
                            <label class="form-label">Tahun Kadaluarsa</label>
                            <div class="form-line">
                            <input required type="text" name="serkom_tahunkadaluarsa" class="form-control datepicker2">
                            
                            </div>
                        </div>                        
                        <div class="form-group form-float">
                        	<div class="form-line">
                        	<textarea type="text" name="serkom_lembaga" class="form-control" rows="3"></textarea>
                        	<label class="form-label">Lembaga</label>
                        	</div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                            <textarea type="text" name="serkom_kompetensi" class="form-control" rows="3"></textarea>
                            <label class="form-label">Kompetansi</label>
                            </div>
                        </div>                        
                        <div class="form-group">
                        	<label>Upload File</label>
                        	<input type="file" name="fileupload">
                        	<p class="help-block">Format file pdf, ukuran max 5mb</p>                           
                        </div>                                                                              										
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-12">
						<button type="submit" value="submit" name="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
						<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
					</div>
				</div>							 	
			</form>
		</div>
	</div>
</div>