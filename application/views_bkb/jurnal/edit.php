<form action="<?php echo base_url('Jurnal/update_data')?>" method="POST" enctype="multipart/form-data">
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" name="id" value="<?= $jurnal->id?>">
					<label class="form-label">Id</label>
				</div>
			</div>		
			<div class="form-group form-float">
				<div class="form-line">
					<input type="text" readonly class="form-control" name="idpeg" value="<?= $jurnal->idpeg?>">
					<label class="form-label">Id Pegawai</label>
				</div>
			</div>								
			<div class="form-group">
                <p>
                    <b class="text-danger">Tahun Akademik</b>
                </p>
                <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                	<?php
                		foreach ($thnakademik as $value) {
                			echo "<option value='".$value->tahun."'";
                				if($jurnal->thakademik==$value->tahun){ echo "selected";}
                			echo ">".$value->tahun."</option>";
                		}
                	?>
                </select>
			</div>
            <div class="form-group">
                <b class="text-danger">Status</b>
                <select required name="status" class="form-control show-tick" data-size="5" data-live-search="true" title="Status">
                    <option value="nasional tidak terakreditasi" <?= strtolower($jurnal->status)=='nasional tidak terakreditasi' ? 'selected':''?> >Nasional tidak terakreditasi</option>
                    <option value="nasional terakreditasi" <?= strtolower($jurnal->status)=='nasional terakreditasi' ? 'selected':''?>>Nasional terakreditasi</option>
                    <option value="nasional berreputasi" <?= strtolower($jurnal->status)=='nasional berreputasi' ? 'selected':''?>>Nasional berreuptasi</option>
                    <option value="internasional" <?= strtolower($jurnal->status)=='internasional' ? 'selected':''?>>Internasional</option>
                </select>
            </div>              
        	<div class="form-group">
        		<b class="text-danger">Jenis Jurnal</b>
                <select required class="form-control show-tick" data-live-search="true" title="Pilih Jenis Jurnal" name="jenisjurnal" required>
                    <option value="jurnal penelitian" <?php if($jurnal->jenisjurnal=='jurnal penelitian'){ echo "selected";}?>>Jurnal Penelitian</option>
                    <option value="jurnal abdimas" <?php if($jurnal->jenisjurnal=='jurnal abdimas'){ echo "selected";}?>>Jurnal Abdimas</option>
                </select>	
        	</div>			
			<div class="form-group form-float">
				<div class="form-line">
					<textarea required rows="4" class="form-control" name="jurnal"><?= ucwords($jurnal->namajurnal)?></textarea>
					<label class="form-label">Nama Jurnal</label>
				</div>
			</div>	                            		
			<div class="form-group form-float">
				<div class="form-line">
					<textarea required rows="4" class="form-control" name="makalah"><?= ucwords($jurnal->judmakalah)?></textarea>
					<label class="form-label">Judul Makalah</label>
				</div>
			</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input required type="text" class="form-control" name="issn" value="<?= $jurnal->issn ?>">
        			<label class="form-label">ISSN/Volume/Edisi</label>
        		</div>
        	</div>
        	<div class="form-group">
        		<p>Tanggal Terbit</p>
        		<div class="form-line">
        			<input required type="text" class="form-control datepicker" placeholder="Tanggal Terbit" name="tglterbit" value="<?= date('d-m-Y',strtotime($jurnal->tglterbit))?>">
        		</div>
        	</div>	
        	<div class="form-group form-float">
        		<div class="form-line">
        			<textarea required rows="4" class="form-control" name="lembaga"><?= $jurnal->lembaga?></textarea>
        			<label class="form-label">Lembaga</label>
        		</div>
        	</div>	                            	                            										
        	<div class="form-group">
                <p>
                    <b>Peran</b>
                </p>
                <select required class="form-control show-tick" data-live-search="true" title="Pilih Peran" name="peran" required>
                    <option value="Ketua" <?php if($jurnal->peran=='Ketua'){ echo 'selected';}?>>Ketua</option>
                    <option value="Anggota" <?php if($jurnal->peran=='Anggota'){ echo 'selected';}?>>Anggota</option>
                </select>	
        	</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input required value="<?= $jurnal->jmltim?>" type="text" class="form-control" name="jmltim">
        			<label class="form-label">Jumlah TIM</label>
        		</div>
        		<p class="help-info col-red">Jika Peran Sebagai Ketua Isikan 0</p>
        	</div>								
        	<div class="form-group form-float">
        		<div class="form-line">
        			<textarea class="form-control" rows="4" name="keterangan"><?= $jurnal->keterangan?></textarea>
        			<label class="form-label">Keterangan Tambahan</label>
        		</div>
        	</div>
        	<div class="form-group form-float hide">
        		<div class="form-line">
        			<input value="<?= $jurnal->file?>" type="text" class="form-control" name="filelama">
        			<label class="form-label">File Lama</label>
        		</div>
        	</div>	        	                	
			<h2 class="card-inside-title">Upload</h2>
			<div class="form-group form-float">
				<div class="">
					<input type="file" name="fileupload" >
					<p class="help-block col-red">Ukuran Maksimal 5mb, format PDF</p>
				</div>
			</div>												
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>							 	
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
        $("#edit_card").hide();
        $("#tampildata").show();
    })
</script>