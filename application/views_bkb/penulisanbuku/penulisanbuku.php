<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>PENULISAN BUKU</h2>
		</div>
		<!--KONFIRMASI AKSI-->
		<?php
			if($this->session->flashdata('success')){
				echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
				';
			}elseif($this->session->flashdata('error')){
				echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
				';				
			}	
		?>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-3">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>		
		<!--TAMBAH DATA FORM-->		
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput" style="display:none">
					<div class="body">
						<form id="formpenulisanbuku" action="<?php echo base_url('Penulisanbuku')?>" method="POST" enctype="multipart/form-data">
							<h2 class="card-inside-title">Form Penulisan Buku</h2>
							<div class="row clearfix">
								<div class="col-sm-12">
									<div class="form-group form-float hide">
										<div class="form-line">
											<input type="text" class="form-control" name="idpeg" value="<?php echo $this->session->userdata('id_pegawai')?>">
											<label class="form-label">Id Pegawai</label>
										</div>
									</div>								
									<div class="form-group">
	                                    <b class="text-danger">Tahun Akademik</b>
	                                    <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
	                                    	<?php
	                                    		foreach ($thnakademik as $value) {
	                                    			echo "<option>".$value->tahun."</option>";
	                                    		}
	                                    	?>
	                                    </select>
									</div>
									<div class="form-group">
	                                    <b class="text-danger">Jenis</b>
	                                    <select required name="jenis" class="form-control show-tick" data-size="5" data-live-search="true" title="Jenis">
	                                    	<option value="buku luaran penelitian">Buku Luaran Penelitian</option>
	                                    	<option value="buku luaran abdimas">Buku Luaran Abdimas</option>
	                                    	<option value="bahan ajar">Bahan Ajar</option>
	                                    </select>
									</div>																		
									<div class="form-group form-float">
										<div class="form-line">
											<textarea required rows="4" class="form-control" name="judul"></textarea>
											<label class="form-label">Judul Buku</label>
										</div>
									</div>	                            		
									<div class="form-group form-float">
										<div class="form-line">
											<textarea required rows="4" class="form-control" name="penerbit"></textarea>
											<label class="form-label">Penerbit</label>
										</div>
									</div>
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input required type="text" class="form-control" name="isbn">
	                            			<label class="form-label" >ISBN</label>
	                            		</div>
	                            	</div>
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input required type="text" class="form-control datepicker" name="tglterbit" placeholder="Tanggal terbit">
	                            		</div>
	                            	</div>									                            		                            		                            		                            	
									<h2 class="card-inside-title">Upload </h2>
									<div class="form-group form-float">
										<div class="">
											<input type="file" name="fileupload">
											<p class="help-block col-red">Ukuran Maksimal 5mb, format PDF</p>
										</div>
									</div>									
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
									<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
								</div>
							</div>							 	
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--EDIT FORM-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card" id="edit_card" style="display:none">
					<div class="header bg-orange">
		                <h2>
                          Edit Data
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!---->
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class="tutup_card">Tutup</a></li>
                                    </ul>
                                </li>
                         </ul>                        				
					</div>				
					<div class="body" id="edit_form">
						<!--AJAX LOAD HERE-->
					</div>
				</div>				
			</div>
		</div>			
		<!--TABEL DATA-->
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header">
		                <h2>
                           Daftar Buku 
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!--
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                    -->
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" width="100%">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="5%">Tahun</th>
												<th width="30%">Buku</th>
												<th width="20%">Penerbit</th>
												<th width="15$">ISBN</th>
												<th width="10%">Terbit</th>
												<th class="text-center" width="15%">Aksi</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i=1;
											foreach ($penulis as $val) {
												echo "<tr>";
													echo "<td>".$i."</td>";
													echo "<td>".$val->thnakademik."</td>";
													echo "<td><p class='small'>Jenis :".ucwords($val->jenis)."</p>".$val->judulbuku."</td>";
													echo "<td>".$val->penerbit."</td>";
													echo "<td>".$val->isbn."</td>";
													echo "<td>".date('d-m-Y',strtotime($val->tglterbit))."</td>";
													echo "<td class='text-center'>";
														if(!empty($val->file)){
															echo "<a href='".site_url('Penulisanbuku/downloadfile/'.$val->file)."' style='width:30px' class='btn btn-xs btn-info waves-effect'><i class='material-icons'>file_download</i></a> ";
														}else{
															echo "<a href='#' class='btn btn-xs btn-default waves-effect'><i class='material-icons'>file_download</i></a> ";
														};													
													echo "<a href='".site_url('Penulisanbuku/hapus/'.$val->id)."' class=' hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
														<a href='#' id='".$val->id."' link='".site_url('Penulisanbuku/edit_data')."' class='edit_data btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> 
													</td>";
												echo "</tr>";
												$i++;
											}											
										?>											
										</tbody>
									</table>
									<p>Keterangan :</p>
									<a href='#' style="width:30px; margin:2px;" class='btn btn-xs btn-info waves-effect'><i class='material-icons'>file_download</i></a> : Download File <br>
									<a href='#' style="width:30px; margin:2px" class='btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> : Tombol Hapus <br>
									<a href='#' style="width:30px;margin:2px" class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> : Tombol Edit <br>							
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
</section>