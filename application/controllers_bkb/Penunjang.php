<?php
	Class Penunjang extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model(array('mpenunjang','mtahunakad'));
		}
		//DEKLARASI VAR
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $path='./berkas/penunjang/';
		//private $path='http://personalia.akprind.ac.id/simpeg/FilePenunjang/';

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}		
		function index(){
			$this->form_validation->set_rules('thnakademik','Tahun Akademik','required');
			if($this->form_validation->run()==true){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					//------AMBIL NAMA FILE UPLOAD
					$fileupload=$this->upload->data();						
					$data=array(
						'idpeg'=>$this->input->post('idpeg'),
						'thnakademik'=>$this->input->post('thnakademik'),
						'namakeg'=>$this->input->post('kegiatan'),
						'tglmulai'=>date('Y-m-d',strtotime($this->input->post('tglmulai'))),
						'tglselesai'=>date('Y-m-d',strtotime($this->input->post('tglselesai'))),
						'file'=>$fileupload['file_name'],
					);	
					//print_r($data);	
					$query=$this->mpenunjang->simpan_penunjang($data);
					if($query==true){
						$this->session->set_flashdata('success',$this->msg_simpansuccess);
						redirect(site_url('User/penunjang'));
					}				
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/penunjang'));
				}

			}
		}
		function edit_data(){
			$id=$this->input->post('id');
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'penunjang'=>$this->mpenunjang->get_penunjang_byid($id)->row(),
			);
			//print_r($data['penunjang']);
			$this->load->view('penunjang/edit',$data);			
		}	
		function update(){
			$id=$this->input->post('id');
			if(!empty($_FILES['fileupload']['name'])){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					unlink($this->path.$this->input->post('filelama'));
					$file=$this->upload->data();
					$fileupload=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/penunjang'));
				}
			}else{
				$fileupload=$this->input->post('filelama');
			}
			$data=array(
				'idpeg'=>$this->input->post('idpeg'),
				'thnakademik'=>$this->input->post('thnakademik'),
				'namakeg'=>$this->input->post('kegiatan'),
				'tglmulai'=>date('Y-m-d',strtotime($this->input->post('tglmulai'))),
				'tglselesai'=>date('Y-m-d',strtotime($this->input->post('tglselesai'))),
				'file'=>$fileupload,
			);	
			//print_r($data);
			$query=$this->mpenunjang->update_penunjang($id,$data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_simpansuccess);
				redirect(site_url('User/penunjang'));	
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
				redirect(site_url('User/penunjang'));				
			}

		}					
		function hapus($id){
			//AMBIL DATA SEMINAR BY ID SEMINAR
			$row=$this->mpenunjang->get_penunjang_byid($id)->row();
			$query=$this->mpenunjang->hapus_penunjang($id);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_hapussuccess);
				unlink($this->path.trim($row->file));
				redirect(site_url('User/penunjang'));				
			}else{
				$msg_error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
				redirect(site_url('User/penunjang'));				
			}			
			//CEK KETERSEDIAN FILE
			// if(file_exists('../simpeg/FilePenunjang/'.$row->file)){
			// 	//HAPUS FILE
			// 	$res=unlink('../simpeg/FilePenunjang/'.$row->file);
			// 	if($res!=true){
			// 		//FILE TIDAK TERHAPUS
			// 		$this->session->set_flashdata('error','File tidak terhapus');
			// 		redirect(site_url('User/penunjang'));						
			// 	}else{
			// 		//FILE DIHAPUS
			// 		$query=$this->mpenunjang->hapus_penunjang($id);
			// 		if($query==true){
			// 			$this->session->set_flashdata('success',$this->msg_hapussuccess);
			// 			redirect(site_url('User/penunjang'));				
			// 		}else{
			// 			$msg_error=$this->db->error();
			// 			$this->session->set_flashdata('error',$msg_error['message']);
			// 			redirect(site_url('User/penunjang'));				
			// 		}
			// 	}				
			// }else{
			// 	//DATA TIDAK DI TEMUKAN
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url('User/penunjang'));	
			// }
			//print_r($row->file);				
		}
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan');
				redirect(site_url('User/penunjang'));	
			}			
			// $link=$this->path.$file;
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/keanggotaanorganisasi"));	
			// }			
		}				
	}
?>