<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kursus extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->Model(array('Mkursus','Crud'));
		if(!$this->session->userdata('login')){
			redirect(site_url('Login'));
		}		
	}
	public function alertmessage(){
		$data=array(
			'simpansuccess'=>'Data berhasil disimpan',
			'simpanerror'=>'Data tidak berhasil disimpan',
			'hapussuccess'=>'Hapus berhasil',
			'hapuserror'=>'Hapus gagal',
			);
		return $data;
	}
	public function index()
	{
		redirect(site_url('User/kursus'));
	}
	public function simpan_data(){
		$alert=$this->alertmessage();
		$data=array(
			'tahun'=>$this->input->post('tahun'),
			'penyelenggara'=>$this->input->post('penyelenggara'),
			'namakursus'=>$this->input->post('kursus'),
			'level'=>$this->input->post('level'),
			'idpeg'=>$this->input->post('idpeg'),
		);
		//print_r($data);
		$res=$this->Mkursus->simpan_data($data);
		if($res==1){
			$this->session->set_flashdata('success',$alert['simpansuccess']);
		}else{
			$this->session->set_flashdata('error',$res);
		}
		redirect(site_url('User/kursus'));
	}
	function hapus($id){	
		$alert=$this->alertmessage();
		$res=$this->Mkursus->hapus_data($id);
		if($res==1){
			$this->session->set_flashdata('success',$alert['hapussuccess']);
		}else{
			$this->session->set_flashdata('error',$res);
		}
		redirect(site_url('User/kursus'));
	}
	function edit(){
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			$data=array(
				'tahun'=>$this->input->post('tahun'),
				'penyelenggara'=>$this->input->post('penyelenggara'),
				'namakursus'=>$this->input->post('kursus'),
				'level'=>$this->input->post('level'),
			);
			$query=array(
				'where'=>array('id'=>$id),
				'data'=>$data,
				'tabel'=>'db_kursus',
				);
			$res=$this->Crud->update($query);
			$alert=$this->alertmessage();
			if($res==1){
				$this->session->set_flashdata('success',$alert['simpansuccess']);
			}else{
				$this->session->set_flashdata('error',$res);
			}
			redirect(site_url('User/kursus'));
		}else{
			$query=array(
				'where'=>array('id'=>$id),
				'tabel'=>'db_kursus',
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				);
			$this->load->view('kursus/edit',$data);			
		}
		//print_r($data['data']);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */