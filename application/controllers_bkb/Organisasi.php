<?php
	Class Organisasi extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model(array('mkeanggotaan','mtahunakad'));
		}

		//DEKLARASI VAR
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $path='./berkas/keanggotaan/';
		//private $path='http://personalia.akprind.ac.id/simpeg/FileKeanggotaan/';

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}			
		function index(){
			$this->form_validation->set_rules('thnakademik','Tahun Akademik','required');
			if($this->form_validation->run()==true){
				$path=$this->path;
				$file='fileupload';
				if($this->fileupload($path,$file)){
					//------AMBIL NAMA FILE UPLOAD
					$fileupload=$this->upload->data();						
					$data=array(
						'idpeg'=>$this->input->post('idpeg'),
						'thnakademik'=>$this->input->post('thnakademik'),
						'organisasi'=>$this->input->post('organisasi'),
						'masaberlaku'=>date('Y-m-d',strtotime($this->input->post('tglberlaku'))),
						'file'=>$fileupload['file_name'],
					);	
					//print_r($data);	
					$query=$this->mkeanggotaan->simpan_keanggotaan($data);
					if($query==true){
						$this->session->set_flashdata('success',$this->msg_simpansuccess);
						redirect(site_url('User/keanggotaanorganisasi'));
					}				
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/keanggotaanorganisasi'));
				}
			}			
		}
		function edit_data(){
			$id=$this->input->post('id');
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'organisasi'=>$this->mkeanggotaan->get_keanggotaan_byid($id)->row(),
			);
			//print_r($data['organisasi']);
			$this->load->view('keanggotaanorganisasi/edit',$data);			
		}	
		function update(){
			$id=$this->input->post('id');
			if(!empty($_FILES['fileupload']['name'])){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					unlink($this->path.$this->input->post('filelama'));
					$file=$this->upload->data();
					$fileupload=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/keanggotaanorganisasi'));
				}
			}else{
				$fileupload=$this->input->post('filelama');
			}
			$data=array(
				'idpeg'=>$this->input->post('idpeg'),
				'thnakademik'=>$this->input->post('thnakademik'),
				'organisasi'=>$this->input->post('organisasi'),
				'masaberlaku'=>date('Y-m-d',strtotime($this->input->post('tglberlaku'))),
				'file'=>$fileupload,
			);	
			//print_r($data);
			$query=$this->mkeanggotaan->update_keanggotaan($id,$data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_simpansuccess);
				redirect(site_url('User/keanggotaanorganisasi'));	
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
				redirect(site_url('User/keanggotaanorganisasi'));				
			}

		}				
		function hapus($id){
			//AMBIL DATA SEMINAR BY ID SEMINAR
			$row=$this->mkeanggotaan->get_keanggotaan_byid($id)->row();
			unlink($this->path.$row->file);
			//FILE DIHAPUS
			$query=$this->mkeanggotaan->hapus_keanggotaan($id);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_hapussuccess);
				redirect(site_url('User/keanggotaanorganisasi'));				
			}else{
				$msg_error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
				redirect(site_url('User/keanggotaanorganisasi'));				
			}			
			//print_r($row->file);				
		}
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan, silahkan upload ulang');
				redirect(site_url('User/keanggotaanorganisasi'));	
			}			
			// $link=$this->path.$file;
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/keanggotaanorganisasi"));	
			// }			
		}				
	}
?>