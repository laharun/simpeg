<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent:: __construct();
		$this->load->model(array('mkursus',
			'mpegawai','mkeluarga','mriwayatpendidikan',
			'mabdimas','mtahunakad','mpenelitian','mseminarpenelitian',
			'mjurnal','mmengajar','mpenulisanbuku','mhakcipta','mkeanggotaan','mpenunjang',
			'mmutasi','mjafa','mriwayatjabatan','Crud'));

		if($this->session->userdata('login')!=true){
			redirect(site_url('Login/logout'));
		}
	}
	//------------------VARIABEL---------------
	function variabel(){
		$data=array(
			'idpeg'=>$this->session->userdata('id_pegawai'),
			);
		return $data;
	}
	function atribut(){
		$data=array(
			'tambah'=>"Tambah Data",
			'edit'=>"Edit Data",
			'data'=>"Tampil Data",
			);
	}
	//-----------------DAHSBOARD----------------
	public function index()
	{
		$var=$this->variabel();
		$data=array(
			'menu'=>'profil',
			'pegawai'=>$this->mpegawai->get_data_pegawai_byid($var['idpeg'])->row(),
			'keluarga'=>$this->mkeluarga->get_keluarga_byidpeg($var['idpeg'])->result(),
			'pendidikan'=>$this->mriwayatpendidikan->get_pendidikan_byidpeg($var['idpeg'])->result(),
			);	
		$this->load->view('template',$data);
		//print_r($data['pegawai']);
		//phpinfo();
	}
	public function datadiri(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'datapribadi',
			'datadiri'=>$this->mpegawai->get_data_pegawai_byid($var['idpeg'])->row(),
			'homebase'=>$this->mpegawai->get_homebase()->result(),
			'golongan'=>$this->mpegawai->get_golongan()->result(),
			);	
		//print_r($data['datadiri']);
		$this->load->view('template',$data);		
		
	}
	function personalia(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'personalia',
			'personalia'=>$this->mpegawai->get_data_pegawai_byid($var['idpeg'])->row(),

			);
		$this->load->view('template',$data);		
		//print_r($data['personalia']);
	}
	function datakeluarga(){
		$var=$this->variabel();
		$query=array(
			'where'=>array(array('idpeg'=>$var['idpeg'])),
			'tabel'=>'db_keluarga1',
			'order'=>array('kolom'=>'id','orderby'=>'asc'),
			);		
		$data=array(
			'menu'=>'datakeluarga',
			'data'=>$this->Crud->read($query)->result(),
			);
		$this->load->view('template',$data);
	}
	function riwayatpendidikan(){
		$var=$this->variabel();
		$query=array(
			'where'=>array(array('idpeg'=>$var['idpeg'])),
			'tabel'=>'db_riwayatpendidikan',
			'order'=>array('kolom'=>'idriwayatpen','orderby'=>'DESC'),
			);
		$data=array(
			'menu'=>'riwayatpendidikan',
			'data'=>$this->Crud->read($query)->result(),
		);
		$this->load->view('template',$data);		
		//print_r($data['data']);
	}
	function kursus(){
		$var=$this->variabel();
		$query=array(
			'tabel'=>'db_kursus',
			'where'=>array(array('idpeg'=>$var['idpeg'])),
			);
		$data=array(
			'menu'=>'kursus',
			'val'=>$this->Crud->read($query)->result(),
			);
		$this->load->view('template',$data);		
		//print_r($data['val']);
	}
	function mutasi(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'mutasi',
			'pegawai'=>$this->mpegawai->get_detail_pegawai($var['idpeg'])->row(),
			'department'=>$this->mpegawai->get_departmen()->result(),
			'mutasi'=>$this->mmutasi->get_mutasi_byidpeg($var['idpeg'])->result(),
			);
		$this->load->view('template',$data);
	}
	function jabatanfungsional(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'jabatanfungsional',
			'jafa'=>$this->mjafa->get_jafa_byidpeg($var['idpeg'])->result(),
			);
		$this->load->view('template',$data);
	}
	function sitasi(){
		//ID Pegawai
		$var=$this->variabel();
		$id=$var['idpeg'];
		$query=array(
			'tabel'=>'db_kategorisitasi',
			);
		// $sitasi=array(
		// 	'select'=>'a.sitasi_id,a.sitasi_idpegawai AS idpegawai,b.kategorisitasi_kategori AS kategori, a.sitasi_url AS url',
		// 	'tabel'=>'db_sitasi a',
		// 	'join'=>array(array('tabel'=>'db_kategorisitasi b','ON'=>'b.kategorisitasi_id::int=a.sitasi_kodekategori','jenis'=>'inner'),),
		// 	'where'=>array('a.sitasi_idpegawai'=>$var['idpeg']),	
		// 	);
		$sitasi=$this->db->query("SELECT a.sitasi_id,a.sitasi_idpegawai AS idpegawai,b.kategorisitasi_kategori AS kategori, a.sitasi_url AS url
			FROM db_sitasi a JOIN db_kategorisitasi b ON b.kategorisitasi_id::text=a.sitasi_kodekategori WHERE a.sitasi_idpegawai=$id")->result();
		$data=array(
			'menu'=>'sitasi',
			'kategorisitasi'=>$this->Crud->read($query)->result(),
			'sitasi'=>$sitasi,
		);
		$this->load->view('template',$data);
		//print_r($sitasi);
	}	
	function riwayatjabatan(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'riwayatjabatan',
			'golongan'=>$this->mpegawai->get_golongan()->result(),
			'riwayatjabatan'=>$this->mriwayatjabatan->get_riwayatjabatan_byidpeg($var['idpeg'])->result(),
			);
		$this->load->view('template',$data);
	}
	//----------------------------------------MENU TRIDHARMA----------------------------
	//
	//
	//-------------------------------------------------------------------------------------
	function abdimas(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'abdimas',
			'abdimas'=>$this->mabdimas->get_abdimas_byidpeg($var['idpeg'])->result(),
			'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
			);
		$this->load->view('template',$data);		
	}
	function penelitian(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'penelitian',
			'penelitian'=>$this->mpenelitian->get_penelitian_byidpeg($var['idpeg'])->result(),
			'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
			);
		$this->load->view('template',$data);		
	}
	function seminarpemakalah(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'seminarpemakalah',
			'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
			'seminar'=>$this->mseminarpenelitian->get_seminar_byidpeg($var['idpeg'])->result(),
			);
		$this->load->view('template',$data);		
	}
	function jurnal(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'jurnal',
			'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
			'jurnal'=>$this->mjurnal->get_jurnal_byidpeg($var['idpeg'])->result(),
			);
		$this->load->view('template',$data);		
	}
	function riwayatmengajar(){
		$var=$this->variabel();
		$data=array(
			'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
			'mengajar'=>$this->mmengajar->get_mengajar_byidpeg($var['idpeg'])->result(),
			'menu'=>'riwayatmengajar',
			);
		$this->load->view('template',$data);		
	}
	function penulisanbuku(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'penulisanbuku',
			'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
			'penulis'=>$this->mpenulisanbuku->get_penulisan_byidpeg($var['idpeg'])->result(),
			);
		$this->load->view('template',$data);			
	}
	function hakcipta(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'hakcipta',
			'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
			'haki'=>$this->mhakcipta->get_haki_byidpeg($var['idpeg'])->result(),
			);
		$this->load->view('template',$data);			
	}
	function keanggotaanorganisasi(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'keanggotaanorganisasi',
			'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
			'keangotaan'=>$this->mkeanggotaan->get_keanggotaan_byidpeg($var['idpeg'])->result(),
			);
		$this->load->view('template',$data);		
	}
	function penunjang(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'penunjang',
			'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
			'penunjang'=>$this->mpenunjang->get_penunjang_byidpeg($var['idpeg'])->result(),
			);
		$this->load->view('template',$data);		
	}
	//------------------PASSWORD----------------------------------------
	function password(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'password',
			'data'=>$this->mpegawai->get_password($var['idpeg'])->row(),
			);
		$this->load->view('template',$data);
	}
	function notfound(){
		$var=$this->variabel();
		$data=array(
			'menu'=>'notfound',
			);
		$this->load->view('template',$data);
	}	
	//-----------------FUNGSI DIBAWAH TIDAK DIPAKAI---------------------								
	function test(){
		$this->load->view('text');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */