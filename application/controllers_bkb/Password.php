<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Password extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model(array('mpegawai','crud'));
		if(!$this->session->userdata('login')){
			redirect(site_url('Login'));
		}
	}
	public function alertmessage(){
		$data=array(
			'simpansuccess'=>'Data berhasil disimpan',
			'simpanerror'=>'Data tidak berhasil disimpan',
			'hapussuccess'=>'Hapus berhasil',
			'hapuserror'=>'Hapus gagal',
			);
		return $data;
	}	
	///////////////////////////////////////////////////////////////////////////////////

	//--------------------------------USER FUNCTION----------------------------------

	//////////////////////////////////////////////////////////////////////////////////	
	public function index()
	{
		$this->load->view('welcome_message');
	}	
	//SIMPAN PASSWORD, SALAH TULIS FUNGSI
	public function update_data(){
		$idpeg=$this->session->userdata('id_pegawai');
		$alert=$this->alertmessage();
		$data=array(
			'username'=>$this->input->post('username'),
			'password'=>$this->input->post('password'),
			);
		//print_r($data);
		$query=$this->mpegawai->update_password($idpeg,$data);
		if($query==true){
			$this->session->set_flashdata('success',$alert['simpansuccess']);
		}else{
			$this->session->set_flashdata('error','Data gagal di update');
		}
		redirect(site_url('User/password'));
	}
	// public function simpan_password(){
	// 	$data=array(
	// 		'idpeg'=>$this->input->post('idpeg'),
	// 		'username'=>$this->input->post('username'),
	// 		'password'=>$this->input->post('password'),
	// 	);
	// 	//print_r($data);
	// 	$query=$this->mpegawai->simpan_password($data);
	// 	if($query==true){
	// 		$this->session->set_flashdata('success','Simpan berhasil');
	// 	}else{
	// 		$this->session->set_flashdata('error','Data gagal di simpan');
	// 	}
	// 	redirect(site_url('User/password'));
	// }
	///////////////////////////////////////////////////////////////////////////////////

	//----------------------------ADMINISTRATOR FUNCTION------------------------------

	//////////////////////////////////////////////////////////////////////////////////
	public function edit_password(){
		$query=array(
			'tabel'=>'db_pegawai',
		);
		$id=$this->input->post('id');
		$password=array(
			'tabel'=>'db_userdosen',
			'where'=>array(array('id'=>$id)),
		);
		$data=array(
			'pegawai'=>$this->crud->read($query)->result(),
			'data'=>$this->crud->read($password)->row(),
			);
		//print_r($data['data']);
		$this->load->view('administrator/edit_password',$data);
	}
	public function update_password(){
		$id=$this->input->post('id');
		$data=array(
			'username'=>$this->input->post('username'),
			'idpeg'=>$this->input->post('idpeg'),
			'password'=>$this->input->post('password'),
			'level'=>$this->input->post('user_level'),
		);
		$query=array(
			'tabel'=>'db_userdosen',
			'data'=>$data,
			'where'=>array('id'=>$id),
		);
		$update=$this->crud->update($query);
		if($update){
			$this->session->set_flashdata('success','Simpan berhasil');
		}else{
			$msg='Update gagal, msg: '.$update;
			$this->session->set_flashdata('error',$msg);
		}
		// $idpeg=$this->session->userdata('id_pegawai');
		// $alert=$this->alertmessage();
		// $data=array(
		// 	'username'=>$this->input->post('username'),
		// 	'password'=>$this->input->post('password'),
		// 	);
		// //print_r($data);
		// $query=$this->mpegawai->update_password($idpeg,$data);
		// if($query==true){
		// 	$this->session->set_flashdata('success',$alert['simpansuccess']);
		// }else{
		// 	$this->session->set_flashdata('error','Data gagal di update');
		// }
		redirect(site_url('Admin/password_user'));
	}
	public function simpan_passwordx(){
		$data=array(
			'idpeg'=>$this->input->post('idpeg'),
			'username'=>$this->input->post('username'),
			'password'=>$this->input->post('password'),
			'level'=>$this->input->post('user_level'),
		);
		//CEK USER DULU, BIAR GA KE DOBLE
		$cek_user=array(
			'tabel'=>'db_userdosen',
			'where'=>array(array('idpeg'=>$data['idpeg'])),
		);
		$cek=$this->crud->read($cek_user)->num_rows();
		$cek_user=array(
			'tabel'=>'db_userdosen',
			'where'=>array(array('username'=>$data['username'])),
		);
		$cek_usr=$this->crud->read($cek_user)->num_rows();
		//echo $cek;
		if($cek >= 1){
			$this->session->set_flashdata('error','Simpan gagal, data sudah ada');
		}else{
			if($cek_usr >= 1){
				$this->session->set_flashdata('error','Simpan gagal, username tersebut sudah ada');	
			}else{
				$query=$this->mpegawai->simpan_password($data);
				if($query==true){
					$this->session->set_flashdata('success','Simpan berhasil');
				}else{
					$this->session->set_flashdata('error','Data gagal di simpan');
				}
			}
		}
		redirect(site_url('Admin/password_user'));
		//print_r($data);
	}		
	public function hapus_password($id){
		$query=array(
			'tabel'=>'db_userdosen',
			'where'=>array('id'=>$id),
		);
		$delete=$this->crud->delete($query);
		if($delete){
			$this->session->set_flashdata('success','Hapus berhasil');
		}else{
			$msg='Hapus gagal, msg :'.$delete;
			$this->session->set_flashdata('error',$msg);
		}
		redirect(site_url('Admin/password_user'));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */