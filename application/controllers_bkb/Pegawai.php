<?php
	Class Pegawai extends CI_Controller{
		function __construct(){
			parent:: __construct();
			$this->load->model(array('mpegawai'));
			if($this->session->userdata('login')!=true){
				redirect(site_url('Login'));
			}
		}
		//DEKLARASI VAR
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $msg_updatesuccess="Data berhasil diupdate";

		private $pathfoto='./berkas/fileFoto/';
		private $pathsktetap='./berkas/sktetap/';
		private $pathskmasuk='./berkas/skmasuk/';
		private $pathktp='./berkas/ktp/';
		private $pathkk='./berkas/kk/';

		//--------------------------------------EXTENDED FUNCTION------------------------------------------
		function matauang($value){
			$var=str_replace('Rp ','', $value);
			$var=str_replace('.', '', $var);
			return $var;
		}
		function uploadfile($path,$var){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'jpg|jpeg',
				'max_size'=>10000,
				'encrypt_name'=>TRUE,
			);
			$this->load->library('upload',$config,$var);
			return $this->$var->do_upload($var);
		}
		function uploadberkas($path,$var){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>10000,
				'encrypt_name'=>TRUE,
			);
			$this->load->library('upload',$config,$var);
			return $this->$var->do_upload($var);
		}	
		function downloadfile($path,$namafile){
			$konversispasi=str_replace('%20',' ', $namafile); 
			$url=file_get_contents($path.$konversispasi);
			//MERUBAH SPASI(%20) menjadi ''
			$download=force_download($konversispasi,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan'); 
				redirect(site_url('User/'));				
			}
		
		}		
		function index(){

		}	
		function update_homebase(){
			$idpeg=$this->input->post('idpeg');
			$data=array(
				'homebase'=>$this->input->post('homebase'),
			);
			$query=$this->mpegawai->update_homebase($data,$idpeg);
				if($query==true){
					$this->session->set_flashdata('success',$this->msg_updatesuccess);
					redirect(site_url('Admin/pegawai'));				
				}else{
					$msg_error=$this->db->error();
					$this->session->set_flashdata('error',$msg_error['message']);
					redirect(site_url('Admin/pegawai'));				
				}
		}
		//-------------------------------EDIT DATA--------------------
		//------------------------EDIT DATA OLEH ADMIN
		function update_data(){
			$id=$this->input->post('idpeg');
			$gajiterakhir=$this->matauang($this->input->post('gajiterakhir'));
			if(empty($_FILES['filefoto']['name'])){
				$filefoto=$this->input->post('fotolama');
			}else{
				$path="../simpeg/fileFoto/";$var='filefoto';
				$uploadfile=$this->uploadfile($path,$var);
				if($uploadfile==true){
					$file=$this->$var->data();
					$filefoto=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->$var->display_errors());
					redirect(site_url('Admin/edit_pegawai/'.md5($id)));
				}
			}
			$data=array(
				'noskmasuk'=>$this->input->post('noskmasuk'),
				'nosktetap'=>$this->input->post('nosktetap'),
				'nama'=>$this->input->post('nama'),
				'kategori'=>$this->input->post('kategori'),
				'idstatuspeg'=>$this->input->post('status'),
				'idunitkerja'=>$this->input->post('penempatan'),
				'idgolongan'=>$this->input->post('golongan'),
				'jabatan'=>$this->input->post('jabatan'),
				'gajiawal'=>$gajiterakhir,
				'foto'=>$filefoto,
			);
			// print_r($data);
			$query=$this->mpegawai->update_pegawai($id,$data);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_updatesuccess);
				redirect(site_url('Admin/pegawai'));
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
				redirect(site_url('Admin/edit_pegawai'));	
			}
		}	
		//-----------------------UPDATE DATADARI----------------------
		function update_datadiri(){
			//CEK FILE SK MASUK
			if(empty($_FILES['fileskmasuk']['name'])){
				$fileskmasuk=$this->input->post('fileskmasuklama');
			}else{
				$path=$this->pathskmasuk;$var='fileskmasuk';
				$uploadfile=$this->uploadberkas($path,$var);
				if($uploadfile==true){
					$file=$this->$var->data();
					$fileskmasuk=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->$var->display_errors());
					//nama view kebalik dengan peseonalia
					redirect(site_url('User/personalia'));
				}
			}
			//CEK FILE SK TETAP
			if(empty($_FILES['filesktetap']['name'])){
				$filesktetap=$this->input->post('filesktetaplama');
			}else{
				$path=$this->pathsktetap;$var='filesktetap';
				$uploadfile=$this->uploadberkas($path,$var);
				if($uploadfile==true){
					$file=$this->$var->data();
					$filesktetap=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->$var->display_errors());
					//nama view kebalik dengan peseonalia
					redirect(site_url('User/personalia'));
				}
			}
			//CEK FILE UPLOAD KTP
			if(empty($_FILES['filektp']['name'])){
				$filektp=$this->input->post('filektplama');
			}else{
				$path=$this->pathktp;$var='filektp';
				$uploadfile=$this->uploadberkas($path,$var);
				if($uploadfile==true){
					$file=$this->$var->data();
					$filektp=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->$var->display_errors());
					//nama view kebalik dengan peseonalia
					redirect(site_url('User/personalia'));
				}
			}
			//CEK FILE KK
			if(empty($_FILES['filekk']['name'])){
				$filekk=$this->input->post('filekklama');
			}else{
				$path=$this->pathkk;$var='filekk';
				$uploadfile=$this->uploadfile($path,$var);
				if($uploadfile==true){
					$file=$this->$var->data();
					$filekk=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->$var->display_errors());
					//nama view kebalik dengan peseonalia
					redirect(site_url('User/personalia'));
				}
			}									
			$idpeg=$this->session->userdata('id_pegawai');
			$gajiterakhir=$this->matauang($this->input->post('gajiawal'));			
			$data=array(
					'nama'=>$this->input->post('nama'),
					'tanggallahir'=>$this->input->post('tgllahir'),
					'tempatlahir'=>$this->input->post('tempatlahir'),
					'idagama'=>$this->input->post('agama'),
					'idgoldarah'=>$this->input->post('goldarah'),
					'statusnikah'=>$this->input->post('statusnikah'),
					'email'=>$this->input->post('email'),
					'noktp'=>$this->input->post('noktp'),
					'nokk'=>$this->input->post('nokk'),
					'kodepos'=>$this->input->post('kodepos'),
					'kota'=>$this->input->post('kota'),
					'nohp'=>$this->input->post('nohp'),
					'gelarbelakang'=>$this->input->post('gelarbelakang'),
					'gelardepan'=>$this->input->post('gelardepan'),
					'prodi'=>$this->input->post('prodi'),
					'tahunprodi'=>$this->input->post('tahunprodi'),
					'fileskmasuk'=>$fileskmasuk,
					'filesktetap'=>$filesktetap,
					'filektp'=>$filektp,
					'filekk'=>$filekk,
			);
			//print_r($data);
			//echo $idpeg;
			$query=$this->mpegawai->update_pegawai($idpeg,$data);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_updatesuccess);
				redirect(site_url('User/personalia'));
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
				redirect(site_url('User/personalia'));	
			}
		}
		//-----------------------------------UPDATE DARA PERSONALIA-------------
		//** kebalik di di view namanya data diri		
		function update_datapersonalia(){
			if(empty($_FILES['filefoto']['name'])){
				$filefoto=$this->input->post('fotolama');
			}else{
				$path=$this->pathfoto;$var='filefoto';
				$uploadfile=$this->uploadfile($path,$var);
				if($uploadfile==true){
					$file=$this->$var->data();
					$filefoto=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->$var->display_errors());
					redirect(site_url('User/datadiri'));
				}
			}
			$idpeg=$this->session->userdata('id_pegawai');
			$gajiterakhir=$this->matauang($this->input->post('gajiawal'));			
			$data=array(
					'gajiawal'=>$gajiterakhir,
					'homebase'=>$this->input->post('homebase'),
					'kategori'=>$this->input->post('kategori'),
					'idgolongan'=>$this->input->post('golongan'),
					'awaltetap'=>date('Y-m-d',strtotime($this->input->post('tgltetap'))),
					'jabatan'=>$this->input->post('jabatan'),
					'fingerprintid'=>$this->input->post('idfingerprint'),
					'foto'=>$filefoto,
			);
			$query=$this->mpegawai->update_pegawai($idpeg,$data);
			if($query==true){
				$this->session->set_flashdata('success',$this->msg_updatesuccess);
				redirect(site_url('User/datadiri'));
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
				redirect(site_url('User/datadiri'));	
			}
			//print_r($data);
		}
		//-------Download----			
		function downloadsktetap($namafile){
			$path=$this->pathsktetap;	
			$this->downloadfile($path,$namafile);		
		}		
		function downloadskmasuk($namafile){
			$path=$this->pathskmasuk;
			$this->downloadfile($path,$namafile);
		}
		function downloadktp($namafile){
			$path=$this->pathktp;
			$this->downloadfile($path,$namafile);
		}
		function downloadkk($namafile){
			$path=$this->pathkk;
			$this->downloadfile($path,$namafile);
		}								
	}
?>