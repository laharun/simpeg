<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Riwayatpendidikan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->Model(array('Crud'));
		if(!$this->session->userdata('login')){
			redirect(site_url('Login'));
		}		
	}
	//private $path_cek="http://personalia.akprind.ac.id/simpeg/fileijazah/"; //UNLINK  FILE	
	//private $path="../simpeg/fileijazah/";
	private	$path='./berkas/ijazah/';

	public function alertmessage(){
		$data=array(
			'simpansuccess'=>'Data berhasil disimpan',
			'simpanerror'=>'Data tidak berhasil disimpan',
			'hapussuccess'=>'Hapus berhasil',
			'hapuserror'=>'Hapus gagal',
			);
		return $data;
	}
	public function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
	}
	public function downloadfile($file){
			$link=$this->path.$file;	
			if(file_exists($link)){
				$url=file_get_contents($link);
				force_download($file,$url);
			}else{
				$this->session->set_flashdata('error','File tidak ditemukan');
				redirect(site_url("User/riwayatpendidikan"));	
			}	
								
	}			
	public function index()
	{
		redirect(site_url('User/riwayatpendidikan'));
	}
	public function simpan(){
		$alert=$this->alertmessage();
		$file='fileijazah';
		$path=$this->path;
		if($this->fileupload($path,$file)){
			$fileijazah=$this->upload->data();
			$data=array(
				'idjenjang'=>$this->input->post('idjenjang'),
				'namasekolah'=>$this->input->post('namasekolah'),
				'namajur'=>$this->input->post('namajur'),
				'noijazah'=>$this->input->post('noijazah'),
				'tanggalijazah'=>date('Y-m-d',strtotime($this->input->post('tanggalijazah'))),
				'fileijazah'=>$fileijazah['file_name'],
				'idpeg'=>$this->input->post('idpeg'),
			);
			$query=array(
				'data'=>$data,
				'tabel'=>"db_riwayatpendidikan",
				);
			$res=$this->Crud->insert($query);
			if($res==1){
				$this->session->set_flashdata('success',$alert['simpansuccess']);
			}else{
				$this->session->set_flashdata('error',$res);
			}
			redirect(site_url('User/riwayatpendidikan'));						
		}else{
			$this->session->set_flashdata('error',$this->upload->display_errors());
			redirect(site_url('User/riwayatpendidikan'));			
		}
	}
	function hapus($id){
		$alert=$this->alertmessage();
		$path=$this->path;
		$file=array(
			'select'=>'fileijazah',
			'tabel'=>'db_riwayatpendidikan',
			'where'=>array('idriwayatpen'=>$id),
		);
		$file=$this->Crud->read($file)->row();
		$query=array(
			'where'=>array('idriwayatpen'=>$id),
			'tabel'=>'db_riwayatpendidikan',
		);		
		$res=$this->Crud->delete($query);
		if($res==1){
			unlink($path.trim($file->fileijazah));
			$this->session->set_flashdata('success',$alert['hapussuccess']);
		}else{
			$this->session->set_flashdata('error',$res);
		}
		redirect(site_url('User/riwayatpendidikan'));
	}
	function edit_data(){
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			$data=array(
				'idjenjang'=>$this->input->post('idjenjang'),
				'namasekolah'=>$this->input->post('namasekolah'),
				'namajur'=>$this->input->post('namajur'),
				'noijazah'=>$this->input->post('noijazah'),
				'tanggalijazah'=>date('Y-m-d',strtotime($this->input->post('tanggalijazah'))),
			);
			//JIKA FILE BARU DIUPLOAD
			$file='fileijazah';
			$path=$this->path;
			if($_FILES['fileijazah']['name']){
				if($this->fileupload($path,$file)){
					$fileijazah=$this->upload->data();
					$data['fileijazah']=$fileijazah['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/riwayatpendidikan'));					
				}
			}			
			$query=array(
				'where'=>array('idriwayatpen'=>$id),
				'data'=>$data,
				'tabel'=>'db_riwayatpendidikan',
			);
			$res=$this->Crud->update($query);
			$alert=$this->alertmessage();
			if($res==1){
				$this->session->set_flashdata('success',$alert['simpansuccess']);
			}else{
				$this->session->set_flashdata('error',$res);
			}
			redirect(site_url('User/riwayatpendidikan'));
		}else{
			$query=array(
				'where'=>array('idriwayatpen'=>$id),
				'tabel'=>'db_riwayatpendidikan',
			);
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
				);
			$this->load->view('riwayatpendidikan/edit',$data);			
		}
		//print_r($data['data']);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */