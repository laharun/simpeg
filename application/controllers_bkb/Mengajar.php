<?php
	Class Mengajar extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->load->model(array('mmengajar','mtahunakad'));
		}

		//DEKLARASI VAR
		private $msg_simpansuccess="Data berhasil disimpan";
		private $msg_hapussuccess="Data berhasil dihapus";
		//var $path='../simpeg/FileNgajar/';
		private $path='./berkas/riwayatmengajar/';

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000,
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}		
		function index(){
			$this->form_validation->set_rules('thnakademik','Tahun Akademik','required');
			if($this->form_validation->run()==true){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					//------AMBIL NAMA FILE UPLOAD
					$fileupload=$this->upload->data();					
					$data=array(
						'idpeg'=>$this->input->post('idpeg'),
						'tahunakademik'=>$this->input->post('thnakademik'),
						'matakuliah'=>$this->input->post('matakuliah'),
						'file'=>$fileupload['file_name'],
						'save_date'=>date('Y-m-d'),
					);	
					//print_r($data);	
					$query=$this->mmengajar->simpan_mengajar($data);
					if($query==true){
						$this->session->set_flashdata('success',$this->msg_simpansuccess);
						redirect(site_url('User/riwayatmengajar'));
					}			
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/riwayatmengajar'));
				}
			}
		}
		function edit_data(){
			$id=$this->input->post('id');
			$data=array(
				'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
				'riwayatmengajar'=>$this->mmengajar->get_mengajar_byid($id)->row(),
			);
			//print_r($data['riwayatmengajar']);
			$this->load->view('riwayatmengajar/edit',$data);			
		}
		function update_data(){
			$id=$this->input->post('id');
			if(!empty($_FILES['fileupload']['name'])){
				$path=$this->path;$file='fileupload';
				if($this->fileupload($path,$file)){
					unlink($this->path.$this->input->post('filelama'));
					$file=$this->upload->data();
					$fileupload=$file['file_name'];
				}else{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(site_url('User/riwayatmengajar'));
				}
			}else{
				$fileupload=$this->input->post('filelama');
			}
			$data=array(
				'idpeg'=>$this->input->post('idpeg'),
				'tahunakademik'=>$this->input->post('thnakademik'),
				'matakuliah'=>$this->input->post('matakuliah'),
				'file'=>$fileupload,
				'update_date'=>date('Y-m-d'),
			);	
			//print_r($data);
			$query=$this->mmengajar->update_mengajar($id,$data);
			if($query){
				$this->session->set_flashdata('success',$this->msg_simpansuccess);
				redirect(site_url('User/riwayatmengajar'));	
			}else{
				$error=$this->db->error();
				$this->session->set_flashdata('error',$error['message']);
				redirect(site_url('User/riwayatmengajar'));				
			}			
		}		
		function hapus($id){
			//AMBIL DATA SEMINAR BY ID SEMINAR
			$row=$this->mmengajar->get_mengajar_byid($id)->row();
			
			//FILE DIHAPUS
			$query=$this->mmengajar->hapus_mengajar($id);
			if($query){
				$this->session->set_flashdata('success',$this->msg_hapussuccess);
				unlink($this->path.$row->file);
				redirect(site_url('User/riwayatmengajar'));				
			}else{
				$msg_error=$this->db->error();
				$this->session->set_flashdata('error',$msg_error['message']);
				redirect(site_url('User/riwayatmengajar'));				
			}								
		}
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan');
				redirect(site_url('User/riwayatmengajar'));	
			}			
			// $link=$this->path.$file;
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/riwayatmengajar"));	
			// }			
		}
	}
?>