<?php
	Class Seminar2 extends CI_Controller{
		function __construct(){
			parent::__construct();
			$this->idpegawai=$this->session->userdata('id_pegawai');
			$this->load->model(array('mseminarpenelitian','mtahunakad','Crud'));
			if($this->session->userdata('login')!=true){
				redirect(site_url('Login/logout'));
			}			
		}
		//DEKLARASI VAR
		private $master_tabel='db_seminartendik';
		private $master_id='seminar_id';
		private $msg_Updatesuccess="Data berhasil diUpdate";
		private $msg_hapussuccess="Data berhasil dihapus";
		private $default_url='seminar2';
		private $default_view='seminar2/';
		//private $path="./berkas/seminar/";
		//DUMMY
		private $path="./berkas/seminartendik/";
		//private $path="http://php4.akprind.ac.id/simpeg/FileSeminar/";

		function fileupload($path,$file){
			$config=array(
				'upload_path'=>$path,
				'allowed_types'=>'pdf',
				'max_size'=>5000, //5mb
				'encrypt_name'=>true,
			);
			$this->load->library('upload',$config);
			return $this->upload->do_upload($file);
		}			
		function index(){
			$this->form_validation->set_rules('seminar_idpeg','Pegawai','required');
			if($this->form_validation->run()==true){
				//$path=$this->path;$file='fileupload';
				$data=array(
					'seminar_idpeg'=>$this->input->post('seminar_idpeg'),
					'seminar_nama'=>$this->input->post('seminar_nama'),
					'seminar_keikutsertaan'=>$this->input->post('seminar_keikutsertaan'),
					'seminar_lembaga'=>$this->input->post('seminar_lembaga'),
					'seminar_tglkegiatan'=>date('Y-m-d',strtotime($this->input->post('seminar_tglkegiatan'))),
					'seminar_tersimpan'=>date('Y-m-d'),
				);				
				$file='fileupload';		
				if($_FILES[$file]['name']){
					if($this->fileupload($this->path,$file)){
						$uploaddata=$this->upload->data();
						$data['seminar_file']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error',$dt['error']);
						redirect(site_url($this->default_url));
					}
				}	
				$query=array(
					'data'=>$data,
					'tabel'=>$this->master_tabel,
				);				
				$insert=$this->Crud->insert($query);
				if($insert){
					$this->session->set_flashdata('success','Update berhasil');
				}else{
					$msg='Update gagal, msg :'.$insert;
					$this->session->set_flashdata('error','Update berhasil');
				}
				redirect(site_url($this->default_url));
				//print_r($data);				
			}else{
				$query=array(
					'select'=>'a.*,b.nama,b.gelardepan,b.gelarbelakang',
					'tabel'=>'db_seminartendik a',
					'where'=>array(array('a.seminar_idpeg'=>$this->idpegawai)),
					'join'=>array(array('tabel'=>'db_pegawai b','ON'=>'a.seminar_idpeg=b.idpeg','jenis'=>'INNER')),
					'orderby'=>array('kolom'=>'b.nama','orderby'=>'ASC'),				
				);
				//$var=$this->variabel();
				$data=array(
					'menu'=>'seminar2',
					//'submenu'=>'seminar2',
					'headline'=>'seminar',
					'data'=>$this->Crud->join($query)->result(),
					'thnakademik'=>$this->mtahunakad->get_tahunakademik()->result(),
					);
				//print_r($data['data']);
				$this->load->view('template',$data);				
			}
		}
		private function get_file($id){
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array('seminar_id'=>$id)),
			);
			$read=$this->Crud->read($query)->row();
			if($read->seminar_file){
				unlink($this->path.$read->seminar_file);
			}
		}
		function hapus($id){
			$this->get_file($id);
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array('seminar_id'=>$id),
			);
			$delete=$this->Crud->delete($query);
			if($delete){
				$this->session->set_flashdata('success','Hapus berhasil');
			}else{
				$mssg='Hapus error, msg : '.$delete;
				$this->session->set_flashdata('error',$msg);
			}
			redirect(site_url($this->default_url));		
			//print_r($query);
		}
		function edit(){
			$id=$this->input->post('id');
			$query=array(
				'tabel'=>$this->master_tabel,
				'where'=>array(array('seminar_id'=>$id)),
			);		
			$data=array(
				'data'=>$this->Crud->read($query)->row(),
			);
			//print_r($data);
			$this->load->view('seminar2/form_edit',$data);			
		}
		function update(){
			$id=$this->input->post('id');
			$this->form_validation->set_rules('seminar_idpeg','Pegawai','required');
			if($this->form_validation->run()==true){
				//$path=$this->path;$file='fileupload';
				$data=array(
					'seminar_idpeg'=>$this->input->post('seminar_idpeg'),
					'seminar_nama'=>$this->input->post('seminar_nama'),
					'seminar_keikutsertaan'=>$this->input->post('seminar_keikutsertaan'),
					'seminar_lembaga'=>$this->input->post('seminar_lembaga'),
					'seminar_tglkegiatan'=>date('Y-m-d',strtotime($this->input->post('seminar_tglkegiatan'))),
					'seminar_tersimpan'=>date('Y-m-d'),
				);				
				$file='fileupload';		
				if($_FILES[$file]['name']){
					if($this->fileupload($this->path,$file)){
						$uploaddata=$this->upload->data();
						$data['seminar_file']=$uploaddata['file_name'];
					}else{
						$dt['error']=$this->upload->display_errors();
						$this->session->set_flashdata('error',$dt['error']);
						redirect(site_url($this->default_url));
					}
				}	
				$query=array(
					'data'=>$data,
					'tabel'=>$this->master_tabel,
					'where'=>array('seminar_id'=>$id),
				);				
				$insert=$this->Crud->update($query);
				if($insert){
					$this->session->set_flashdata('success','Update berhasil');
				}else{
					$msg='Update gagal, msg :'.$insert;
					$this->session->set_flashdata('error','Update berhasil');
				}
				//print_r($data);
				redirect(site_url($this->default_url));
			}		
		}
		// function detail(){
		// 	$id=$this->input->post('id');
		// 	$query=array(
		// 		'where'=>array('id'=>$id),
		// 		'tabel'=>'db_seminar',
		// 	);
		// 	$data=array(
		// 		'data'=>$this->Crud->read($query)->row(),
		// 		);
		// 	$this->load->view('seminar/detail',$data);
		// }
		function downloadfile($file){
			$file=str_replace('%20',' ', $file);
			$link=$this->path.$file;
			$url=file_get_contents($link);
			$download=force_download($file,$url);
			if(!$download){
				$this->session->set_flashdata('error','File tidak ditemukan,silahkan upload ulang');
				redirect(site_url('User/seminarpemakalah'));	
			}			
			// if(file_exists($link)){
			// 	$url=file_get_contents($link);
			// 	force_download($file,$url);
			// }else{
			// 	$this->session->set_flashdata('error','File tidak ditemukan');
			// 	redirect(site_url("User/seminarpemakalah"));	
			// }			
		}		
	}
?>