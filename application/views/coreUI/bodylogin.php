<?php
 include(APPPATH.'views/coreUI/header.php');
?>
<body class="login-page" style="background-color:#d2d6de">
    <div class="login-box">  
        <div class="logo">
            <a href="javascript:void(0);"><img src="<?php echo base_url()?>assets/images/logos.png" class="img-responsive" style="width:100%"></a>
            <br>
            <small>Sistem Kepegawaian</small>
        </div>
        <!--KONFIRMASI AKSI-->
        <?php
            if($this->session->flashdata("success")==true){
                echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
                ';
            }elseif($this->session->flashdata("error")==true){
                echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
                ';              
            }   
        ?>          
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="<?php echo site_url()?>">
                    <div class="msg"></div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-block btn-block btn-info waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="text-center legal">
        <div class="copyright">
            &copy; 2018 <a href="javascript:void(0);">bp3si - p3si@akprind.ac.id</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.5<br>
            <?= 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>'?>
        </div>
    </div>
<?php
 include(APPPATH.'views/coreUI/footerlogin.php');
?>