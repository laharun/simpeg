<form action="<?= base_url('keluarga/edit_data')?>" method="POST" enctype="multipart/form_data">
	<div class="row clearfix">
		<div class="col-sm-12">
            <div class="form-group form-float hide">
                <div class="form-line">
                    <label class="form-label">Id</label>
                    <input type="text" readonly class="form-control" name="id" value="<?=$data->id?>" />
                 </div>
            </div>
            <div class="form-group form-float ">
                <div class="form-line">
                    <p>Hubungan</p>
                      <select id="edithubungan" required name="hubungan" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih hubungan keluarga">
				        <option value="Ayah" <?= trim($data->hubungan)=='Ayah' ? 'Selected':''?>>Ayah</option>
				        <option value="Ibu" <?= trim($data->hubungan)=='Ibu' ? 'Selected':''?>>Ibu</option>
				        <option value="Ayah Mertua" <?= trim($data->hubungan)=='Ayah Mertua' ? 'Selected':''?>>Ayah Mertua</option>
				        <option value="Ibu Mertua" <?= trim($data->hubungan)=='Ibu Mertua' ? 'Selected':''?> >Ibu Mertua</option>
				        <option value="Suami" <?= trim($data->hubungan)=='Suami' ? 'Selected':''?>>Suami</option>
				        <option value="Istri" <?= trim($data->hubungan)=='Istri' ? 'Selected':''?>>Istri</option>
				        <option value="Anak Kandung" <?= trim($data->hubungan)=='Anak Kandung' ? 'Selected':''?>>Anak Kandung</option>
					    <option value="Saudara Kandung" <?= trim($data->hubungan)=='Saudara Kandung' ? 'Selected':''?>>Saudara Kandung</option>
                	</select>
                 </div>
            </div>
            <div id="editanakke" class="form-group form-float" <?= trim($data->hubungan)!='Anak Kandung' ? 'style="display:none"':''?>>
               	<div class="form-line">
                    <input type="text" class="form-control" name="anak_ke" value="<?= $data->anak_ke?>" />
                    <label class="form-label">Anak ke</label>
                </div>
            </div>
            <div id="edittglmenikah" class="form-group form-float"  <?= (trim($data->hubungan)!='Istri' AND trim($data->hubungan)!='Suami') ? 'style="display:none"':''?>>
               	<div class="form-line">
               		<p>Tanggal menikah</p>
                    <input type="text" class="datepicker form-control" name="tgl_menikah" value="<?= $data->tgl_menikah?>" />
                </div>
            </div>	                                	                                	                                									
            <div class="form-group form-float">
                <div class="form-line"> 
                    <input type="text" class="form-control" name="anggota" value="<?= ucwords($data->anggota)?>" />
                    <label class="form-label">Nama</label>
                 </div>
            </div>
            <div class="form-group form-float">
               	<div class="form-line">
               		<p>Tanggal Lahir</p>
                    <input type="text" class="datepicker form-control" name="tgl_lahir" value="<?= date('Y-m-d',strtotime($data->tgl_lahir))?>" />
                </div>
            </div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input type="text" class="form-control" name="kotakelahiran" value="<?= ucwords($data->kotakelahiran)?>" />
        			<label class="form-label">Kota kelahiran</label>
        		</div>
        	</div>		                                
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input type="text" class="form-control" name="telp" value="<?= ucwords($data->telp)?>" />
        			<label class="form-label">No Telepon</label>
        		</div>
        	</div>	
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input type="text" class="form-control" name="pekerjaan" value="<?= ucwords($data->pekerjaan)?>"/>
        			<label class="form-label">Pekerjaan</label>
        		</div>
        	</div>	
        	<div class="form-group form-float">
        		<div class="form-line">
        			<textarea rows="4" class="form-control" name="alamat"><?=  ucwords($data->alamat)?></textarea>
        			<label class="form-label">Alamat</label>
        		</div>
        	</div>		                            		                            	                            	 									
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" name="submit"  value="submit" class="btn btn-primary btn-lg m-t-15 waves-effect btn-block">SIMPAN</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>	
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$(".tutup_card").click(function(){
		       $("#edit_card").hide();
               $("#tampildata").show();
		});
		$("#edithubungan").change(function(){
		var dt=$(this).val();
		if(dt=="Anak Kandung"){
			//alert('anak');
			$('#editanakke').show();
			$('#edittglmenikah').hide();
		}else if((dt=='Istri')||(dt=='Suami')){
		  //alert('istri / suami');
			$('#edittglmenikah').show();
			$('#editanakke').hide();
		}else{
		 //alert('lainnya');
			$('#editanakke').hide();
			$('#edittglmenikah').hide();
		}
		});    		
	})
</script>