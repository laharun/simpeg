<section class="content">
	<div class="container-fluid">
        <!--ALERT-->
		<?php
			if($this->session->flashdata('error')==true){
				echo '
		        <div class="alert bg-red alert-dismissible">
		        	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            <b>Perhatian !</b> '.$this->session->flashdata('error').'
		        </div>
				';
			}elseif($this->session->flashdata('success')==true){
				echo '
		        <div class="alert bg-green alert-dismissible">
		        	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            <b>Perhatian !</b> '.$this->session->flashdata('success').'
		        </div>
				';				
			}
		?>	
		<div class="block-header">
			<h2>DATA KELUARGA</h2>
		</div>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-3">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>
		<!--INPUT FORM-->
		<div class="row clearfix" >
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput" style="display:none">
					<div class="header">
		                <h2>
                          	Form Keluarga
                        </h2>				
					</div>				
					<div class="body">
						<form action="<?= base_url('Keluarga/simpan_data')?>" method="POST" enctype="multipart/form_data">
							<div class="row clearfix">
								<div class="col-sm-12">
	                                <div class="form-group form-float hide">
	                                    <div class="form-line">
	                                        <label class="form-label">IdPeg</label>
	                                        <input type="text" readonly class="form-control" name="idpeg" value="<?=$this->session->userdata('id_pegawai')?>" />
	                                     </div>
	                                </div>
	                                <div class="form-group form-float ">
	                                    <div class="form-line">
	                                        <p>Hubungan</p>
	                                          <select id="hubungan" required name="hubungan" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih hubungan keluarga">
										        <option value="Ayah">Ayah</option>
										        <option value="Ibu">Ibu</option>
										        <option value="Ayah Mertua" >Ayah Mertua</option>
										        <option value="Ibu Mertua" >Ibu Mertua</option>
										        <option value="Suami" >Suami</option>
										        <option value="Istri" >Istri</option>
										        <option value="Anak Kandung" >Anak Kandung</option>
											    <option value="Saudara Kandung" >Saudara Kandung</option>
	                                    	</select>
	                                     </div>
	                                </div>
	                                <div id="anakke" class="form-group form-float" style="display:none">
	                                   	<div class="form-line">
	                                        <input type="text" class="form-control" name="anak_ke" />
	                                        <label class="form-label">Anak ke</label>
	                                    </div>
	                                </div>
	                                <div id="tglmenikah" class="form-group form-float"  style="display:none">
	                                   	<div class="form-line">
	                                   		<p>Tanggal menikah</p>
	                                        <input type="text" class="datepicker form-control" name="tgl_menikah" />
	                                    </div>
	                                </div>	                                	                                	                                									
	                                <div class="form-group form-float">
	                                    <div class="form-line">
	                                        <input type="text" class="form-control" name="anggota"/>
	                                        <label class="form-label">Nama</label>
	                                     </div>
	                                </div>
	                                <div class="form-group form-float">
	                                   	<div class="form-line">
	                                   		<p>Tanggal Lahir</p>
	                                        <input type="text" class="datepicker form-control" name="tgl_lahir" />
	                                    </div>
	                                </div>
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input type="text" class="form-control" name="kotakelahiran" />
	                            			<label class="form-label">Kota kelahiran</label>
	                            		</div>
	                            	</div>		                                
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input type="text" class="form-control" name="telp" />
	                            			<label class="form-label">No Telepon</label>
	                            		</div>
	                            	</div>	
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input type="text" class="form-control" name="pekerjaan" />
	                            			<label class="form-label">Pekerjaan</label>
	                            		</div>
	                            	</div>	
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<textarea rows="4" class="form-control" name="alamat"></textarea>
	                            			<label class="form-label">Alamat</label>
	                            		</div>
	                            	</div>		                            		                            	                            	 									
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" name="submit"  value="submit" class="btn btn-primary btn-lg m-t-15 waves-effect btn-block">SIMPAN</button>
									<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
								</div>	
							</div>
						</form>
					</div>					
				</div>
			</div>
		</div>
		<!--EDIT FORM-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card" id="edit_card" style="display:none">
					<div class="header bg-orange">
		                <h2>
                          Edit Data
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!---->
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class="tutup_card">Tutup</a></li>
                                    </ul>
                                </li>
                         </ul>                        				
					</div>				
					<div class="body" id="edit_form">
						<!--AJAX LOAD HERE-->
					</div>
				</div>				
			</div>
		</div>			
		<!--TAMPIL DATA-->
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
		                <h2>
                           Daftar Keluarga
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!--
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                    -->
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix" >
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" width="100%">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Hubungan</th>
												<th width="40%">Anggota</th>
												<th width="20%">Tgl.Lahir</th>
												<th width="15%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $i=1;foreach($data AS $row):?>
												<tr>
													<td><?= $i ?></td>
													<td><?= ucwords($row->hubungan)?></td>
													<td><?= ucwords($row->anggota)?></td>
													<td><?= date('d-m-Y',strtotime($row->tgl_lahir))?></td>
													<td class="text-center">
														<a href="#" id='<?= $row->id?>' url="<?= base_url('Keluarga/detail_data')?>" style='width:30px' class='detail btn btn-xs btn-info waves-effect'><i class='material-icons'>account_circle</i></a> 
														<a href="<?= site_url('Keluarga/hapus/'.$row->id)?>" style='width:30px' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 													
														<a href="#" id='<?= $row->id?>' link='<?= base_url('Keluarga/edit_data')?>' style='width:30px' class='edit_data btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> 
													</td>
												</tr>
											<?php $i++;endforeach;?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>
<div class="modal fade" id="detail" tabindex="-1" role="dialog">
</div>