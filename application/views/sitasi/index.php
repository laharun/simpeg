<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Sitasi</h2>
		</div>
		<!--KONFIRMASI AKSI-->
		<?php
			if($this->session->flashdata('success')){
				echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
				';
			}elseif($this->session->flashdata('error')){
				echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
				';				
			}	
		?>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-2">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>		
		<!--TAMBAH DATA FORM-->		
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput" style="display:none">
					<div class="body">
						<form  action="<?php echo base_url('Sitasi')?>" method="POST" enctype="multipart/form-data">
							<h2 class="card-inside-title">Form Sitasi</h2>
							<div class="row clearfix">
								<div class="col-sm-12">
									<div class="form-group form-float hide">
										<div class="form-line">
											<input type="text"  class="form-control" readonly name="sitasi_idpegawai" value="<?php echo $this->session->userdata('id_pegawai')?>">
											<label class="form-label">Id Pegawai</label>
										</div>
									</div>								
									<div class="form-group">
	                                    <b>Sitasi</b>
	                                    <select required name="sitasi_kodekategori" class="form-control show-tick" data-size="5" data-live-search="true" title="Kategori Sitasi">
	                                    	<?php
	                                    		foreach ($kategorisitasi as $row) {
	                                    			echo "<option value='".$row->kategorisitasi_id."'>".ucwords($row->kategorisitasi_kategori)."</option>";
	                                    		}
	                                    	?>
	                                    </select>

									</div>
                                    <div class="input-group form-float">
                                        <span class="input-group-addon">http://</span>
                                        <div class="form-line">
                                            <input type="text" required class="form-control" placeholder="Url" name="sitasi_url">
                                        </div>
                                    </div>	
                                    <p class="help-block">Penulisan url tanpa menggunakan http</p>									                            										
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
									<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
								</div>
							</div>							 	
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--EDIT FORM-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card" id="edit_card" style="display:none">
					<div class="header bg-orange">
		                <h2>
                          Edit Data
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!---->
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class="tutup_card">Tutup</a></li>
                                    </ul>
                                </li>
                         </ul>                        				
					</div>				
					<div class="body" id="edit_form">
						<!--AJAX LOAD HERE-->
					</div>
				</div>				
			</div>
		</div>		
		<!--TABEL DATA-->
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header">
		                <h2>
                           Sitasi Dosen
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="20%">Kategori</th>
												<th width="65%">Url</th>
												<th class="text-center" width="10%">Aksi</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i=1;
											foreach ($sitasi as $val) {
												echo "<tr>";
													echo "<td>".$i."</td>";
													echo "<td>".ucwords($val->kategori)."</td>";
													echo "<td>".$val->url."</td>";
													echo "<td class='text-center'>
														<a href='".site_url('Sitasi/hapus/'.$val->sitasi_id)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
														<a href='#' id='".$val->sitasi_id."' link='".site_url('Sitasi/edit')."' class='edit_data btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> 
														<a href='".$val->url."' target='_blank' class='btn btn-xs btn-success waves-effect'><i class='material-icons'>language</i></a> 
													</td>";
												echo "</tr>";
												$i++;
											}											
										?>																					
										</tbody>
									</table>
									<p>Keterangan :</p>
									<a href='#' style="width:30px; margin:2px" class='btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> : Tombol Hapus <br>
									<a href='#' style="width:30px;margin:2px" class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> : Tombol Edit <br>								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
</section>