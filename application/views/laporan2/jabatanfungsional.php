<style type="text/css">
	.biru{
		font-weight: 800;
	}
</style>
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?= ucwords($judul)?> <small> </small></h2>
		</div>		
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header bg-blue">
						<h2>
							<?= ucwords($judul)?><br>
							<small>Jabatan fungsional yang diambil adalah yang terbaru</small>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
							</li>
						</ul>				
					</div>
					<div class="body">


						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in table-responsive active" id="dosen"> 			
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
										<thead class="bg-blue">
											<tr>
												<th width="5%">No</th>
												<th width="10%">Id</th>
												<th width="25%">Nama</th>
												<th width="25%">Jabatan</th>
												<th width="30%">kredit</th>
												<th width="5%">file</th>
											</tr>
										</thead>
										<tbody>
											<?php $i=1;foreach($dosen AS $row):?>
												<tr class="<?= $row->idjafa ? 'bg-green':'bg-red' ?>">
													<td><?=$i?></td>
													<td><?= $row->idpeg?></td>
													<td><?= $row->gelardepan.' '.ucwords($row->nama).' '.$row->gelarbelakang?></td>
													<td><?= $row->namajafa?></td>
													<td><?= $row->angkakredit?></td>
													<td class="text-center"> <a href="<?=base_url('Jafa/download/'.$row->idpeg)?>"><i class='material-icons'>details</i></a> </td>
												</tr>												
											<?php $i++;endforeach;?>
										</tbody>
									</table>								
								</div>
							</div>
						</div>		
					</div>
				</div>
			</div>		
		</div>		
	</div>			
</section>