<?php
    if($this->session->flashdata('success')){
        echo'
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                '.$this->session->flashdata("success").'
            </div>
        ';
    }elseif($this->session->flashdata('error')){
        echo'
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                '.$this->session->flashdata("error").'
            </div>
        ';              
    }   
?> 