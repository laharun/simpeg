<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>PASSWORD</h2>
		</div>
		<!--KONFIRMASI AKSI-->
		<?php
			if($this->session->flashdata('success')){
				echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
				';
			}elseif($this->session->flashdata('error')){
				echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
				';				
			}	
		?>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-2">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Ubahh Password</button>
				</div>
			</div>
		</div>
		<!--ADD DATA-->			
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput" style="display:none">
					<div class="body">
						<form id="formpassword" action="<?php echo site_url('Password/update_data')?>" method="POST">
							<h2 class="card-inside-title">Form Perubahan Password</h2>
							<div class="row clearfix">
								<div class="col-sm-12">								
	                                <div class="form-group form-float ">
	                                    <div class="form-line">
	                                        <input type="text" class="form-control" name="username" id="username" value="<?php echo $data->username ?>" required />
	                                        <label class="form-label">Username</label>
	                                     </div>
	                                </div>
	                                <div class="form-group form-float">
	                                   	<div class="form-line">
	                                        <input type="password" class="form-control" name="password" id="password" value="<?php echo $data->password ?>" required />
	                                        <label class="form-label">Password</label>
	                                    </div>
	                                </div>
	                                <div class="form-group form-float">
	                                	<div class="form-line">
	                                        <input type="password" class="form-control" name="ulangipassword" id="ulangipassword"/>
	                                        <label class="form-label">Tulis Ulang Password</label>	                                		
	                                	</div>
	                                </div>                           		                            	                            	 									
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">Update</button>
								</div>	
							</div>
						</form>
					</div>					
				</div>
			</div>
		</div>
		<!--TABEL DATA-->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
		                <h2>
                           Riwayat Password Penggguna
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">	
							<p>Username : <?php echo $data->username?></p>
							<p>Password : <?php echo $data->password?></p>
							</div>
						</div>
					</div>
				</div>
		</div>		
	</div>	
</section>