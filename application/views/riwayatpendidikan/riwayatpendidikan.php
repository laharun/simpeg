<section class="content">
	<div class="container-fluid">
        <!--ALERT-->
		<?php
			if($this->session->flashdata('error')==true){
				echo '
		        <div class="alert bg-red alert-dismissible">
		        	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            <b>Perhatian !</b> '.$this->session->flashdata('error').'
		        </div>
				';
			}elseif($this->session->flashdata('success')==true){
				echo '
		        <div class="alert bg-green alert-dismissible">
		        	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		            <b>Perhatian !</b> '.$this->session->flashdata('success').'
		        </div>
				';				
			}
		?>		
		<div class="block-header">
			<h2>RIWAYAT PENDIDIKAN</h2>
		</div>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-3">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>
		<!--TABEL ADD DATA-->		
		<div class="row clearfix ">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput" style="display:none">
					<div class="body">
						<form action="<?= base_url('Riwayatpendidikan/simpan')?>" method="POST" enctype="multipart/form-data">
							<h2 class="card-inside-title">Riwayat Pendidikan</h2>
							<div class="row clearfix">
								<div class="col-sm-12">
	                                <div class="form-group form-float">
	                                   	<div class="hide form-line">
	                                        <input readonly type="text" class="form-control" name="idpeg" value="<?= $this->session->userdata('id_pegawai')?>" />
	                                        <label class="form-label">Id Pegawai</label>
	                                    </div>
	                                </div>								
	                                <div class="form-group form-float ">
	                                    <div class="form-line">
	                                    	<p>Jenjang Pendidikan</p>
		                                    <select required name="idjenjang" class="form-control show-tick" data-size="5" data-live-search="true" title="Jenjang">
		                                    	<option value="SD">SD</option>
		                                    	<option value="SMP">SMP</option>
		                                    	<option value="SMA/SMK">SMA/SMK</option>
		                                    	<option value="D3">D3</option>
		                                    	<option value="S1">S1</option>
		                                    	<option value="S2">S2</option>
		                                    	<option value="S3">S3</option>
		                                    </select>	                                        
	                                        
	                                     </div>
	                                </div>
	                                <div class="form-group form-float">
	                                   	<div class="form-line">
	                                        <input type="text" class="form-control" name="namasekolah" />
	                                        <label class="form-label">Asal Sekolah</label>
	                                    </div>
	                                </div>
	                                <div class="form-group form-float">
	                                   	<div class="form-line">
	                                        <input type="text" class="form-control" name="namajur" />
	                                        <label class="form-label">Jurusan/Prodi</label>
	                                    </div>
	                                </div>
	                            	<div class="form-group form-float" id="anak">
	                            		<div class="form-line">
	                            			<input type="text" class="form-control" name="noijazah" />
	                            			<label class="form-label">Nomor Ijazah</label>
	                            		</div>
	                            	</div>
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                   						<p>Tanggal Ijazah</p>						
	                            			<input type="text" class="form-control datepicker " name="tanggalijazah" />
	        								
	                            		</div>
	                            	</div>
	                            	<div class="form-group form-float">
										<input type="file" name="fileijazah" />
	                            		<p class="help-block">Format PDF, ukuran max 5 mb</p>
	                            	</div>	                            			                            		                            	                            	 									
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" name="submit" value="submit" class="btn btn-primary btn-lg  m-t-15 waves-effect btn-block">SIMPAN</button>
									<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
								</div>	
							</div>
						</form>
					</div>					
				</div>
			</div>
		</div>
		<!--EDIT FORM-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card" id="edit_card" style="display:none">
					<div class="header bg-orange">
		                <h2>
                          Edit Data
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!---->
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class="tutup_card">Tutup</a></li>
                                    </ul>
                                </li>
                         </ul>                        				
					</div>				
					<div class="body" id="edit_form">
						<!--AJAX LOAD HERE-->
					</div>
				</div>				
			</div>
		</div>		
		<!--TABEL DATA-->
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
		                <h2>
                           Daftar Riwayat Pendidikan
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!--
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                    -->
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" width="100%">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="50%">Sekolah</th>
												<th width="20%">Jurusan</th>
												<th width="10%">Tanggal</th>
												<th width="15%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $i=1;foreach($data AS $row):?>
												<tr>
													<td><?= $i?></td>
													<td>
													Nomor Ijazah : <?= $row->noijazah?>	
													<br><b><?= ucwords($row->namasekolah)?><b></td>
													<td><?= ucwords($row->namajur)?></td>
													<td><?= date('d-m-Y',strtotime($row->tanggalijazah))?></td>
													<td class='text-center'>
														<a href="<?= site_url('Riwayatpendidikan/downloadfile/'.trim($row->fileijazah))?>"style='width:30px' class='<?= !$row->fileijazah? 'btn-default disabled':'btn-info'?> btn btn-xs  waves-effect'><i class='material-icons'>file_download</i></a> 
														<a href="<?= site_url('Riwayatpendidikan/hapus/'.$row->idriwayatpen)?>" style='width:30px' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 													
														<a href='#' id='<?= $row->idriwayatpen?>' link='<?= base_url('Riwayatpendidikan/edit_data')?>' style='width:30px' class='edit_data btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> 
													</td>
												</tr>
											<?php $i++;endforeach;?>											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>	
</section>