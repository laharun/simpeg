        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card forminput">
                    <div class="header bg-orange">
                        <h2>
                            User Pengguna
                        </h2>
                    </div>                    
                    <div class="body">
                        <form id="formpassword" action="<?php echo base_url('Password/update_password')?>" method="POST" enctype="multipart/form-data">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Id</label>
                                        <div class="form-line">
                                            <input readonly type="text" name="id" class="form-control" value="<?=$data->id?>">
                                        </div>
                                    </div>                              
                                    <div class="form-group">
                                        <b>Pegawai</b>
                                        <select required name="idpeg" class="form-control show-tick" data-size="5" data-live-search="true">
                                            <?php foreach($pegawai AS $row):?>
                                                <option value="<?= $row->idpeg?>" <?= $data->idpeg==$row->idpeg?'selected':''?>><?= ucwords($row->nama)?></option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <div class="form-group form-float">
                                        <b>
                                            Username
                                        </b>
                                        <div class="form-line">
                                            <input required type="text" class="form-control" name="username" value="<?= $data->username?>">
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <b>
                                            Password
                                        </b>
                                        <div class="form-line">
                                            <input required id="password" required type="password" class="form-control" name="password" value="<?= $data->password?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Level</label>
                                        <select class="form-control show-tick" name="user_level">
                                            <option value="dosen" <?= $data->level=='dosen'? 'selected':''?>>Tendik</option>
                                            <option value="administrasi" <?= $data->level=='administrasi'? 'selected':''?>>Administrasi</option>
                                        </select>
                                    </div>                                                                       
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-warning btn-lg waves-effect btn-block">Update</button>
                                </div>
                            </div>                              
                        </form>
                    </div>
                </div>
            </div>
        </div>