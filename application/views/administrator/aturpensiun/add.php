<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="card forminput" style="display:none">
        <div class="header bg-light-blue">
            <h2>
                Form Atur pensiun
            </h2>
        </div>		
		<div class="body">
			<form  action="<?php echo base_url('Aturpensiun')?>" method="POST" enctype="multipart/form-data">
				<div class="row clearfix">
					<div class="col-sm-12">
                        <div class="form-group form-float">
                        	<label class="form-label">Nama</label>
                        	<div class="form-line">
                        		<input type="text" name="nama" class="form-control">
                        	</div>
                        </div>
                        <div class="form-group form-float">
                        	<label class="form-label">Tahun</label>
                        	<div class="form-line">
                        		<input type="text" name="tahun" class="form-control">
                        	</div>
                        </div>
                        <div class="form-group form-float">
                            <label class="form-label">Jenis</label>
                            <div class="form-line">
                                <select name="jenis" class="form-control">
                                    <option></option>
                                    <option value="dosen">Dosen</option>
                                    <option value="tendik">Tendik</option>
                                </select>
                            </div>
                        </div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-12">
						<button type="submit" value="serdos" name="serdos" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
						<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
					</div>
				</div>							 	
			</form>
		</div>
	</div>
</div>