<section class="content">
	<div class="container-fluid">		
        <!--KONFIRMASI AKSI-->
        <?php
            if($this->session->flashdata('success')){
                echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
                ';
            }elseif($this->session->flashdata('error')){
                echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
                ';              
            }   
        ?> 
        <div class="block-header">
            <h2>Daftar Pengguna</h2>
        </div>        
        <!--FORM TAMBAH DATA-->
        <div class="row clearfix formtambah">
            <div class="col-sm-2">
                <div class="form-group">
                    <button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
                </div>
            </div>
        </div>
        <!--TAMBAH DATA FORM-->     
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card forminput" style="display:none">
                    <div class="header bg-light-blue">
                        <h2>
                            User Pengguna
                        </h2>
                    </div>                    
                    <div class="body">
                        <form id="formpassword" action="<?php echo base_url('Password/simpan_passwordx')?>" method="POST" enctype="multipart/form-data">
                            <div class="row clearfix">
                                <div class="col-sm-12">                              
                                    <div class="form-group">
                                        <b>Pegawai</b>
                                        <select required name="idpeg" class="form-control show-tick" data-size="5" data-live-search="true">
                                            <option disabled="" value="" selected=""></option>
                                            <?php
                                                foreach ($pegawai as $value) {
                                                    echo "<option value='".$value->idpeg."'>".ucwords($value->nama)."</option>";
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group form-float">
                                        <b>
                                            Username
                                        </b>
                                        <div class="form-line">
                                            <input required type="text" class="form-control" name="username">
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <b>
                                            Password
                                        </b>
                                        <div class="form-line">
                                            <input required id="password" required type="password" class="form-control" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Level</label>
                                        <select class="form-control show-tick" name="user_level">
                                            <option value="dosen">Tendik</option>
                                            <option value="administrasi">Administrasi</option>
                                        </select>
                                    </div>                                                                       
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
                                </div>
                            </div>                              
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--TAMPIL DATA-->
        <div id="tampildata">                                      		
    		<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                                Akun Pengguna <small>Daftar Akun Pengguna Sistem, jika ada username/pegawai yang sama tidak akan tersimpan</small>
                            </h2>
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-col-pink" role="tablist">
                                <li role="presentation" class="active"><a href="#tendik" data-toggle="tab">Dosen/Tendik
                                </a></li>
                                <li role="presentation"><a href="#kependidikan" data-toggle="tab">Kependidikan</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="tendik">
                                <!--PEGAWAI TENDIK/DOSEN-->
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th width="5%">No</th>
                                                    <th width="30%">Nama</th>
                                                    <th width="25%">Username</th>
                                                    <th width="30%">Password</th>
                                                    <th class="text-center" width="10%">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $i=1;
                                                    foreach ($dosen as $val) {
                                                        echo "<tr>";
                                                            echo "<td>".$i."</td>";
                                                            echo "<td>".ucwords($val->nama)."</td>";
                                                            echo "<td>".$val->username."</td>";
                                                            echo "<td>".$val->password."</td>";
                                                            echo "<td class='text-center'>
                                                                <div class='btn-group'>
                                                                    <a href='".site_url('password/hapus_password/'.$val->id)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
                                                                    <a href='#' id='".$val->id."' link='".site_url('Password/edit_password')."' class='edit_data btn btn-xs btn-info waves-effect'><i class='material-icons'>mode_edit</i></a>
                                                                </div>
                                                            </td>";                                
                                                        echo "</tr>";
                                                        $i++;
                                                    }
                                                ?>
                                            </tbody>                                            
                                        </table>
                                        <p>Keterangan :</p>
                                        <a href="#" class="btn btn-xs btn-danger waves-effect" style="width=25px"><i class='material-icons'>delete</i></a> : Hapus Data
                                        <br>
                                        <a href="#" class="btn btn-xs btn-info waves-effect" style="width=25px"><i class='material-icons'>mode_edit</i></a> : Edit Data
                                    </div>
                                </div>
                                <!--PEGAWAI KEPENDIDIKAN-->
                                <div role="tabpanel" class="tab-pane fade" id="kependidikan">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th width="5%">No</th>
                                                    <th width="30%">Nama</th>
                                                    <th width="25%">Username</th>
                                                    <th width="30%">Password</th>
                                                    <th class="text-center" width="10%">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1;foreach($administrasi AS $row):?>
                                                    <tr>
                                                        <td><?= $i?></td>
                                                        <td><?= ucwords($row->nama)?></td>
                                                        <td><?= $row->username?></td>
                                                        <td><?= $row->password?></td>
                                                        <td class="text-center">
                                                            <div class='btn-group'>
                                                                <a href='<?= site_url('password/hapus_password/'.$row->id)?>' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
                                                                <a href='#' id='<?= $row->id?>' link='<?= site_url('Password/edit_password')?>' class='edit_data btn btn-xs btn-info waves-effect'><i class='material-icons'>mode_edit</i></a>
                                                            </div>                                                            
                                                        </td>
                                                    </tr>
                                                <?php $i++;endforeach;?>
                                            </tbody>                                            
                                        </table>
                                        <p>Keterangan :</p>
                                        <a href="#" class="btn btn-xs btn-danger waves-effect" style="width=25px"><i class='material-icons'>delete</i></a> : Hapus Data
                                        <br>
                                        <a href="#" class="btn btn-xs btn-info waves-effect" style="width=25px"><i class='material-icons'>mode_edit</i></a> : Edit Data                            
                                    </div>
                                </div>
                            </div>
                        </div>               
                	</div>                      			
                </div>
    		</div>
        </div>
	</div>
    <div id="edit_form"></div>  
</section>