<div class="modal-dialog">  
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title">Detail Serkom</h1>
      </div>
      <table class="table table-bordered table-hover">
        <thead>
        <tr>
          <th>No</th>
          <th>Lembaga</th>
          <th>Kompetensi</th>
          <th>Tahun</th>
          <th>File</th>
        </tr>
        </thead>
        <?php
        $no=1;
        foreach ($serkom as $dt) {
          ?>
          <tr>
            <td><?=$no++?></td>
            <td><?=$dt->serkom_lembaga?></td>
            <td><?=$dt->serkom_kompetensi?></td>
            <td><?=$dt->serkom_tahunkeluar.'-'.$dt->serkom_tahunkadaluarsa?></td>
            <td><a href='<?= site_url('serkomadmin/downloadfile/'.$dt->serkom_file)?>' class='btn btn-xs btn-primary waves-effect'><i class='material-icons'>archive</i></a> </td>
          </tr>
          <?php
        }
        ?>
      </table>
    </div>
  </div> 