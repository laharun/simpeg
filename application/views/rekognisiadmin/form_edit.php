<form  action="<?php echo base_url('Rekognisiadmin/update')?>" method="POST" enctype="multipart/form-data">
    <div class="row clearfix">
        <div class="col-sm-12">
            <div class="form-group hide">
                <label>Id</label>
                <div class="form-line">
                    <input type="text"  class="form-control" readonly name="id" value="<?= $data->rekognisi_id?>">
                </div>
            </div>                                                      
            <div class="form-group">
                <p>
                    <b>Tahun Akademik</b>
                </p>
                <select required name="rekognisi_tahunakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                    <?php foreach ($thnakademik as $row):?>
                        <option value="<?=$row->tahun?>" <?= $row->tahun==$data->rekognisi_tahunakademik ? 'selected':''?>><?= $row->tahun ?></option>
                    <?php endforeach;?>
                </select>
            </div>
            <div class="form-group">
                <label>Nama Pegawai</label>
                <div class="form-line">
                    <select class="form-control" name="rekognisi_idpegawai" data-size="10" data-live-search="true">
                        <?php foreach($pegawai AS $row):?>
                            <option value="<?=$row->idpeg?>" <?= $row->idpeg==$data->rekognisi_idpeg ? 'selected':''?>><?=ucwords($row->nama)?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>                        
            <div class="form-group">
                <label>Rekognisi</label>
                <div class="form-line">
                    <input type="text" name="rekognisi_rekognisi" class="form-control" value="<?= $data->rekognisi_rekognisi?>">    
                </div>
            </div>
            <div class="form-group">
                <label>Tempat</label>
                <div class="form-line">
                    <textarea type="text" name="rekognisi_tempat" class="form-control" rows="5"><?= $data->rekognisi_tempat?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">Tanggal Kegiatan</label>
                <div class="form-line">
                   <input required type="text" name="rekognisi_tglkegiatan" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($data->rekognisi_tglkegiatan))?>">
                </div>
            </div>                          
            <div class="form-group">
                <label>Upload File</label>
                <input type="file" name="fileupload">
                <p class="help-block">Format file pdf, ukuran max 5mb</p>  
                <span class="label label-info">File : <?= $data->rekognisi_file?></span>                         
            </div>                                                                                                                    
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-sm-12">
            <button type="submit" value="submit" name="submit" class="btn btn-warning btn-lg waves-effect btn-block">Update</button>
            <button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
        </div>
    </div>  
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker2').bootstrapMaterialDatePicker({
            format: "YYYY",
            //year:true,
            time:false,
            //date:true,
            //monthPicker:true
        });         
    })
</script>