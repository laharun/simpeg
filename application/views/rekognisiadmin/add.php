<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="card forminput" style="display:none">
        <div class="header bg-light-blue">
            <h2>
                Form Tambah Rekognisi
            </h2>
        </div>        
		<div class="body">
			<form  action="<?php echo base_url('Rekognisiadmin')?>" method="POST" enctype="multipart/form-data">
				<div class="row clearfix">
					<div class="col-sm-12">							
						<div class="form-group">
                            <label>Tahun Akademik</label>
                            <select required name="rekognisi_tahunakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                            	<?php
                            		foreach ($thnakademik as $value) {
                            			echo "<option>".$value->tahun."</option>";
                            		}
                            	?>
                            </select>
						</div>
                        <div class="form-group">
                            <label>Nama Pegawai</label>
                            <div class="form-line">
                                <select class="form-control" name="rekognisi_idpegawai" data-size="10" data-live-search="true">
                                    <?php foreach($pegawai AS $row):?>
                                        <option value="<?=$row->idpeg?>"><?=ucwords($row->nama)?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Rekognisi</label>
                            <div class="form-line">
                                <input type="text" name="rekognisi_rekognisi" class="form-control">    
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tempat</label>
                            <div class="form-line">
                                <textarea type="text" name="rekognisi_tempat" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Tanggal Kegiatan</label>
                        	<div class="form-line">
                        	   <input required type="text" name="rekognisi_tglkegiatan" class="form-control datepicker">
                        	</div>
                        </div>                       
                        <div class="form-group">
                        	<label>Upload File</label>
                        	<input required type="file" name="fileupload">
                        	<p class="help-block">Format file pdf, ukuran max 5mb</p>                           
                        </div>                                                                              										
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-12">
						<button type="submit" value="submit" name="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
						<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
					</div>
				</div>							 	
			</form>
		</div>
	</div>
</div>