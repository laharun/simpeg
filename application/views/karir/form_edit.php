<form  action="<?php echo base_url('karir/update')?>" method="POST" enctype="multipart/form-data">
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text"  class="form-control" readonly name="id" value="<?= $data->karir_id?>">
					<label class="form-label">Id Pegawai</label>
				</div>
			</div>			
			<div class="form-group">
				<label>Karir</label>
				<div class="form-line">
                  	<select required name="karir_karir" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih karir">
						<option value="1" <?= $data->karir_karir==1 ? 'selected':''?>>Kepala Lab</option>	
						<option value="2" <?= $data->karir_karir==2 ? 'selected':''?>>Kepala Lab</option>	
						<option value="3" <?= $data->karir_karir==3 ? 'selected':''?>>Kepala Lab</option>	
						<option value="4" <?= $data->karir_karir==4 ? 'selected':''?>>Kepala Lab</option>				
					</select>
				</div>
			</div>							
            <div class="form-group">
            	<label class="form-label">Tanggal diperoleh</label>
            	<div class="form-line">
            	<input type="text" name="karir_tanggalditerima" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($data->karir_tanggalditerima))?>">
        
            	</div>
            </div>
            <div class="form-group">
            	<label class="form-label">Tanggal purna jabatan</label>
            	<div class="form-line">
            	<input type="text" name="karir_tanggalpurnajabatan" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($data->karir_purnajabatan))?>">
            	
            	</div>
            </div>                        
            <div class="form-group">
            	<label>Upload File</label>
            	<input type="file" name="fileupload">
            	<p class="help-block">Format file pdf, ukuran max 5mb</p>                           
            </div>                                                                              							
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" value="karir" name="karir" class="btn btn-warning btn-lg waves-effect btn-block">UPDATE</button>
			<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>							 	
</form>
<?php include 'action.php';?>