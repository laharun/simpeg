<script>
    $(document).ready(function(){
        $('.count').countTo();
        //--------------PRICE FORMAT----------------
        $(".price").priceFormat({
          prefix:'Rp ',
          thousandsSeparator:'.',
          centsLimit:'0',
        });
        //---------------DATE PICKER-----------------
        $('.datepicker').bootstrapMaterialDatePicker({
          time:false,
          format: "DD-MM-YYYY",
        });
      
    });
</script>