<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="card forminput" style="display:none">
        <div class="header bg-light-blue">
            <h2>
                Form Sertifikasi Dosen
            </h2>
        </div>		
		<div class="body">
			<form  action="<?php echo base_url('karir')?>" method="POST" enctype="multipart/form-data">
				
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="form-group form-float hide">
							<div class="form-line">
								<input type="text"  class="form-control" readonly name="karir_idpegawai" value="<?php echo $this->session->userdata('id_pegawai')?>">
								<label class="form-label">Id Pegawai</label>
							</div>
						</div>	
						<div class="form-group">
							<label>Karir</label>
							<div class="form-line">
		                      	<select required name="karir_karir" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih karir">
									<option value="1">Kepala Lab</option>	
									<option value="1">Kepala Lab</option>	
									<option value="1">Kepala Lab</option>	
									<option value="1">Kepala Lab</option>				
								</select>
							</div>
						</div>							
                        <div class="form-group">
                        	<label class="form-label">Tanggal diperoleh</label>
                        	<div class="form-line">
                        	<input type="text" name="karir_tanggalditerima" class="form-control datepicker">
                    
                        	</div>
                        </div>
                        <div class="form-group">
                        	<label class="form-label">Tanggal purna jabatan</label>
                        	<div class="form-line">
                        	<input type="text" name="karir_tanggalpurnajabatan" class="form-control datepicker">
                        	
                        	</div>
                        </div>                        
                        <div class="form-group">
                        	<label>Upload File</label>
                        	<input type="file" name="fileupload">
                        	<p class="help-block">Format file pdf, ukuran max 5mb</p>                           
                        </div>                                                                              										
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-12">
						<button type="submit" value="karir" name="karir" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
						<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
					</div>
				</div>							 	
			</form>
		</div>
	</div>
</div>