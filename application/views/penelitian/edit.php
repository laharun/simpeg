<form  enctype="multipart/form-data" action="<?php echo site_url('Penelitian/update_data')?>" method="POST" >
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group hide">
				<div class="form-line">
					<b>Id</b>
					<input type="text" class="form-control" name="id" value="<?php echo $penelitian->id?>">
				</div>				
			</div>
			<div class="form-group">			
				<div class="form-line">
					<b>Id Pegawai</b>
					<input type="text" class="form-control" name="idpeg" readonly value="<?php echo $penelitian->idpeg?>">
				</div>
			</div>								
			<div class="form-group">
	            <p>
	               	<b>Tahun Akademik</b>
	            </p>
	            <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
	                <?php
	                    foreach ($thnakademik as $value) {
	                    echo "<option value='".$value->tahun."'";
	                    if($penelitian->tahunakademik==$value->tahun){echo "selected";}
	                    echo ">".$value->tahun."</option>";
	               		}
	                ?>
	            </select>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<textarea rows="4" class="form-control" name="judul" required><?= ucwords($penelitian->judulpenelitian)?></textarea>
					<label class="form-label">Judul</label>
				</div>
			</div>
        	<div class="form-group">
                <b>Peran</b>
                <select class="form-control show-tick" data-live-search="true" title="Pilih Peran" name="peran" required>
                    <option value="ketua" <?php if($penelitian->peran=='ketua'){echo 'selected';}?>>Ketua</option>
                    <option value="anggota" <?php if($penelitian->peran=='anggota'){echo 'selected';}?>>Anggota</option>
                </select>	
        	</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input type="text" class="form-control" name="jumlahtim" value="<?= $penelitian->tim ?>" required>
        			<label class="form-label">Jumlah TIM</label>
        		</div>
        		<p class="help-block col-red">Jika Peran Ketua isikan 0</p>
        	</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input type="text" class="form-control" name="skema" value="<?= $penelitian->skema?>" required>
        			<label class="form-label">Skema</label>
        		</div>
        	</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input type="text" class="form-control" name="asaldana" value="<?= $penelitian->asaldana?>" required>
        			<label class="form-label">Asal Dana</label>
        		</div>
        	</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input type="text" class="form-control price" name="besaranggaran" value="<?= $penelitian->dana?>" required>
        			<label class="form-label">Besar Anggaran</label>
        		</div>
        	</div> 
			<div class="form-group form-float">
				<div class="form-line">
					<textarea class="form-control" name="abstrak" rows="4" required><?= ucwords($penelitian->abstrak)?></textarea>
					<label class="form-label">Abstrak</label>
				</div>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<textarea class="form-control" name="tempat" rows="4" required><?= ucwords($penelitian->tempat)?></textarea>
					<label class="form-label">Tempat</label>
				</div>
			</div>        	
        	<div class="form-group form-float">
        		<div class="form-line">
        			<textarea class="form-control" rows="4" name="keterangan" required><?= $penelitian->keterangan?></textarea>
        			<label class="form-label">Keterangan Tambahan</label>
        		</div>
        	</div> 
			<h2 class="card-inside-title">Upload</h2>
			<div class="form-group form-float">
				<input type="file" name="fileupload">
				<p class="help-block col-red">Ukuran Maksimal 5mb, format PDF</p>
			</div>
			<div class="form-group form-float hide">
				<label class="form-label">File Lama</label>
				<input type="text" class="form-control" value="<?= $penelitian->file?>" name="filelama" required>
			</div>																
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-warning btn-lg waves-effect btn-block">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
        $("#edit_card").hide();
        $("#tampildata").toggle();
    })
    $(".price").priceFormat({
      prefix:'Rp ',
      thousandsSeparator:'.',
      centsLimit:'0',
    });     
</script>