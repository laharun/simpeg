<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>KURSUS</h2>
		</div>
		<!--KONFIRMASI AKSI-->
		<?php
			if($this->session->flashdata('success')){
				echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
				';
			}elseif($this->session->flashdata('error')){
				echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
				';				
			}	
		?>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-2">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>
		<!--ADD DATA-->			
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card forminput" style="display:none">
					<div class="body">
						<form action="<?php echo site_url('Kursus/simpan_data')?>" method="POST" enctype="multipart/form-data">
							<h2 class="card-inside-title">Riwayat Mengikuti Kursus</h2>
							<div class="row clearfix">
								<div class="col-sm-12">
	                            	<div class="form-group form-float hide">
	                                    <div class="form-line">
	                                        <input type="text" readonly class="form-control" name="idpeg" value="<?= $this->session->userdata('id_pegawai')?>" />
	                                        <label class="form-label">Id</label>
	                                     </div>
	                            	</div>
	                            	<div class="form-group form-float">
	                                    <div class="form-line">
	                                        <input type="text" class="form-control" name="tahun" />
	                                        <label class="form-label">Tahun</label>
	                                     </div>
	                            	</div>								
	                                <div class="form-group form-float">
	                                    <div class="form-line">
	                                   		<textarea class="form-control no-resize" name="kursus" rows="4"></textarea>	                                    
	                                        <label class="form-label">Kursus/Keterampilan</label>
	                                     </div>
	                                </div>
	                                <div class="form-group form-float">
	                                   	<div class="form-line">
	                                        <input type="text" class="form-control" name="penyelenggara" />
	                                        <label class="form-label">Penyelenggara/lembaga</label>
	                                    </div>
	                                </div>
	                            	<div class="form-group">
	                                    <p>
	                                        <b>Level</b>
	                                    </p>
	                                    <select name="level" class="form-control show-tick" data-live-search="true">
	                                        <option>Lanjut</option>
	                                        <option>Menengah</option>
	                                        <option>Dasar</option>
	                                    </select>	
	                            	</div>                            		                            	                            	 									
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
									<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
								</div>	
							</div>
						</form>
					</div>					
				</div>
			</div>
		</div>
		<!--EDIT FORM-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card" id="edit_card" style="display:none">
					<div class="header bg-orange">
		                <h2>
                          Edit Data
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!---->
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class="tutup_card">Tutup</a></li>
                                    </ul>
                                </li>
                         </ul>                        				
					</div>				
					<div class="body" id="edit_form">
						<!--AJAX LOAD HERE-->
					</div>
				</div>				
			</div>
		</div>		
		<!--TABEL DATA-->
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
		                <h2>
                           Kursus
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!--
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                    -->
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" width="100%">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="10%">Tahun</th>
												<th width="20%">Kurus</th>
												<th width="35%">Penyelenggara</th>
												<th width="15%">Level</th>
												<th width="15%" class="text-center">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$i=1;
												foreach ($val as $key) {
													echo "<tr>";
													echo "<td>".$i."</td>";
													echo "<td>".$key->tahun."</td>";
													echo "<td>".$key->namakursus."</td>";
													echo "<td>".$key->penyelenggara."</td>";
													echo "<td>".$key->level."</td>";
													echo "<td class='text-center'>";
														echo "<a href='".site_url('Kursus/hapus/'.$key->id)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a>  
															<a href='#' id='".$key->id."' link='".base_url('Kursus/edit')."' class='edit_data btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a>"; 
													echo "</td>";
													echo "</tr>";
													$i++;		
												}
											?>											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>		
	</div>	
</section>