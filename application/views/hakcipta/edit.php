<form id="formhaki" action="<?php echo base_url('Hakcipta/update')?>" method="POST" enctype="multipart/form-data">
	<h2 class="card-inside-title">Form Isian HAKI</h2>
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" name="id" value="<?= $haki->id?>">
					<label class="form-label">Id</label>
				</div>
			</div>		
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" name="idpeg" value="<?= $haki->idpeg?>">
					<label class="form-label">Id Pegawai</label>
				</div>
			</div>								
			<div class="form-group">
                <p>
                    <b>Tahun Akademik</b>
                </p>
                <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                	<?php
                		foreach ($thnakademik as $value) {
                			echo "<option value='".$value->tahun."'";
                				if($haki->thnakademik==$value->tahun){echo "selected";}
                			echo ">".$value->tahun."</option>";
                		}
                	?>
                </select>

			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<textarea required rows="4" class="form-control" name="judul"><?= $haki->haki?></textarea>
					<label class="form-label">Judul Ciptaan</label>
				</div>
			</div>	                            		
			<div class="form-group form-float">
				<div class="form-line">
					<input required type="text" class="form-control" name="nomor" value="<?= $haki->nomor?>">
					<label class="form-label">Nomor Pencatatan</label>
				</div>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<input required name="tgl" type="text" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($haki->tgl)) ?>" placeholder="Tanggal Penetapan/diumumkan"/>
				</div>
			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<input required type="text" class="form-control" name="nomor" value="<?= $haki->nomor?>">
					<label class="form-label">File Lama</label>
				</div>
			</div>			
			<div class="form-group form-float">
				<div class="form-line">
					<textarea required rows="4" class="form-control" name="tempat"><?= $haki->tempat?> </textarea>
					<label class="form-label">Di Umumkan di</label>
				</div>
			</div>																			
        	<div class="form-group">
                <p>
                    <b>Peran</b>
                </p>
                <select required class="form-control show-tick" data-live-search="true" title="Pilih Peran" name="peran" required>
                    <option value="Ketua" <?php if(strtolower($haki->peran)=='ketua'){echo "selected";}?>>Ketua</option>
                    <option value="Anggota" <?php if(strtolower($haki->peran)=='anggota'){echo "selected";}?>>Anggota</option>
                </select>	
        	</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input required type="text" class="form-control" name="jmltim" value="<?= $haki->jumtim?>">
        			<label class="form-label">Jumlah TIM</label>
        		</div>
        		<p class="help-info col-red">Jika Peran Sebagai Ketua Isikan 0</p>
        	</div>	 
        	<div class="form-group form-float hide">
        		<div class="form-line">
        			<input required type="text" class="form-control" name="filelama" value="<?= $haki->file?>">
        			<label class="form-label">File</label>
        		</div>
        	</div>        	                           		                            		                            		                            	
			<h2 class="card-inside-title">Upload </h2>
			<div class="form-group form-float">
				<div class="">
					<input type="file" name="fileupload" >
					<p class="help-block col-red">Ukuran Maksimal 5mb</p>
				</div>
			</div>									
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>							 	
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
        $("#edit_card").hide();
        $("#tampildata").show();
    })
</script>