<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?= ucwords($judul)?> <small> </small></h2>
		</div>		
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header bg-blue">
						<h2>
							<?= ucwords($judul)?>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
							</li>
						</ul>				
					</div>
					<div class="body">
						<ul class="nav nav-tabs tab-nav-right" role="tablist">
							<li role="presentation" class="active"><a href="#dosen" data-toggle="tab">Dosen</a></li>
							<li role="presentation"><a href="#tendik" data-toggle="tab">Tendik</a></li>
						</ul>	                	

						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in table-responsive active" id="dosen"> 			
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
										<thead class="bg-blue">
											<tr>
												<th width="5%">No</th>
												<th width="10%">Id</th>
												<th width="70%">Nama</th>
												<th width="10%">Upload</th>
												<th class="text-center" width="5%">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $i=1;foreach($dosen AS $row):?>
											<tr class="<?=$row->jumlah!=0 ? 'bg-green':''?>">
												<td><?=$i?></td>
												<td><?= $row->idpeg?></td>
												<td><?= $row->gelardepan.' '.ucwords($row->nama).' '.$row->gelarbelakang?></td>
												<td><?= $row->jumlah?></td>
												<td class="text-center">
													<a href='#' link="<?=site_url('Serkom/detail')?>" id="<?=$row->idpeg?>" class='homebase btn btn-xs btn-info waves-effect'><i class='material-icons'>visibility</i></a>
												</td>
											</tr>
											<?php $i++;endforeach;?>									
										</tbody>
									</table>								
								</div>
							</div>
							<div role="tabpanel" class="tab-pane fade in table-responsive" id="tendik"> 			
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
										<thead class="bg-blue">
											<tr>
												<th width="5%">No</th>
												<th width="10%">Id</th>
												<th width="70%">Nama</th>
												<th width="10%">Upload</th>
												<th class="text-center" width="5%">Aksi</th>
											</tr>
										</thead>
										<tbody>
											<?php $i=1;foreach($tendik AS $row):?>
											<tr class="<?=$row->jumlah!=0 ? 'bg-green':''?>">
												<td><?=$i?></td>
												<td><?= $row->idpeg?></td>
												<td><?= $row->gelardepan.' '.ucwords($row->nama).' '.$row->gelarbelakang?></td>
												<td><?= $row->jumlah?></td>
												<td class="text-center">
													<a href='#' link="<?=site_url('Serkom/detail')?>" id="<?=$row->idpeg?>" class='homebase btn btn-xs btn-info waves-effect'><i class='material-icons'>class</i></a>
												</td>
											</tr>
											<?php $i++;endforeach;?>									
										</tbody>
									</table>								
								</div>
							</div>				
						</div>		
					</div>
				</div>
			</div>		
		</div>		
	</div>			
</section>
<div id="homebase" class="modal fade" role="dialog"></div>   