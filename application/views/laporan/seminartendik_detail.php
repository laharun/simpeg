<div class="modal-dialog">  
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title">Detail Seminar Tendik</h1>
      </div>
      <table class="table table-bordered table-hover">
        <tr>
          <th>No</th>
          <th>Nama seminar</th>
          <th>Lembaga</th>
          <th>Sebagai</th>
          <th>Jenis</th>
          <th>File</th>
        </tr>
        <?php
        $no=1;
        foreach ($seminar as $dt) {
          ?>
          <tr>
            <td><?=$no++?></td>
            <td><?=$dt->seminar_nama?></td>
            <td><?=$dt->seminar_lembaga?></td>
            <td><?=$dt->seminar_keikutsertaan?></td>
            <td><?=$dt->seminar_jenis?></td>
            <td><a href='<?= site_url('Seminar2/downloadfile/'.$dt->seminar_file)?>' class='btn btn-xs btn-primary waves-effect'><i class='material-icons'>file_download</i></a> </td>
          </tr>
          <?php
        }
        ?>
      </table>
    </div>
  </div> 