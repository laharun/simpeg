<style type="text/css">
	.biru{
		font-weight: 800;
	}
</style>
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?= ucwords($judul)?> <small> </small></h2>
		</div>		
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header bg-blue">
						<h2>
							<?= ucwords($judul)?>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
							</li>
						</ul>				
					</div>
					<div class="body">


						<div class="tab-content">
							<div role="tabpanel" class="tab-pane fade in table-responsive active" id="dosen"> 			
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
										<thead class="bg-blue">
											<tr>
												<th width="5%">No</th>
												<th width="10%">Id</th>
												<th width="25%">Nama</th>
												<th width="25%">nomorsk</th>
												<th width="25%">Tanggal</th>
												<th width="5%">file</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$i=1;
											$nama='';
											foreach($dosen AS $row):

											if (trim($nama)==trim($row->nama)) 
											{
												?>
										<!-- 		<tr>
													<td><?=$i?></td>
													<td> </td>
													<td> </td>
													<td class="biru"><?= $row->nomorsk?></td>
													<td> <a href="<?=base_url('berkas/jabatan/'.$row->file)?>"><i class='material-icons'>archive</i></a> </td>
												</tr> -->

												
												<?php
											}
											else
											{
												?>
												<tr>
													<td><?=$i?></td>
													<td><?= $row->idpeg?></td>
													<td><?= $row->gelardepan.' '.ucwords($row->nama).' '.$row->gelarbelakang?></td>
													<td><?= $row->nomorsk?></td>
													<td><?= date('d-m-Y',strtotime($row->tmt))?></td>
													<td> <a href="<?=base_url('berkas/jabatan/'.$row->file)?>"><i class='material-icons'>archive</i></a> </td>
												</tr>
												<?php
											}
											$i++;
											$nama=$row->nama;
											endforeach;
											?>
										</tbody>
									</table>								
								</div>
							</div>
						</div>		
					</div>
				</div>
			</div>		
		</div>		
	</div>			
</section>