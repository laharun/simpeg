<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?= ucwords($judul)?> <small>
				<div style="line-height: 20px;">
					Masa Kerja Golongan, Adalah masa kerja yang dihitung dari TMT SK Tetap<br>
					Masa Kerja Keseluruhan, Adalah masa kerja yang dihitung dari TMT SK Capeg<br>
					<label class="label label-success">&nbsp;&nbsp;&nbsp;</label> : Pensiun kurang X tahun<br>
					<label class="label label-primary">&nbsp;&nbsp;&nbsp;</label> : Saatnya pensiun Y <br>
					<label class="label label-danger">&nbsp;&nbsp;&nbsp;</label> : Pensiun Y sudah lewat X tahun<br>
					X == lama tahun<br>
					Y == jenis pensiun<br>
				</div>


			</small></h2>
		</div>		
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header bg-blue">
						<h2>
							<?= ucwords($judul)?>
						</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
							</li>
						</ul>				
					</div>
					<div class="body">
						<ul class="nav nav-tabs tab-col-pink" role="tablist">
							<li role="presentation" class="active"><a href="#tendik" data-toggle="tab">Dosen
							</a></li>
							<li role="presentation"><a href="#kependidikan" data-toggle="tab">Tendik</a></li>
						</ul>
						<div class="tab-content">
 								<div role="tabpanel" class="tab-pane fade in active" id="tendik">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="20%">Nama</th>
													<th width="10%">Lahir</th>
													<th width="10%">Umur</th>
													<th width="10%">Masuk</th>
													<th width="10%">Tetap</th>
													<th width="10%">MKK</th>	
													<th width="10%">MKG</th>
													<?php $pensiun=$this->Crud->read(array('tabel'=>'db_aturpensiun','select'=>'nama,tahun','order'=>array('kolom'=>'tahun','orderby'=>'ASC'),'where'=>array(array('jenis'=>'dosen'))))->result();
													foreach ($pensiun as $dt) { ?> <th width="10%"><?=$dt->nama?><b><br>(<?=$dt->tahun?> Th)</b></th> <?php } ?>
												</tr>
											</thead>
											<tbody>
												<?php $i=1;foreach($data_dosen AS $row):?>
												<tr>
													<td><?=$i?></td>
													<td><?=$row->gelardepan.' '.ucwords($row->nama).' '.$row->gelarbelakang?><br><small class="label label-info"><?= !$row->statuspegawai ? 'Aktif':$row->statuspegawai ?></small></td>
													<td><?= date('d-m-Y',strtotime($row->tanggallahir))?></td>
													<td><?=$row->umur['tahun'].', '.$row->umur['bulan'].' Bulan'?></td>
													<td><?= date('d-m-Y',strtotime($row->awalmasuk))?></td>
													<td><?= date('d-m-Y',strtotime($row->awaltetap))?></td>
													<td><?=$row->mkk['tahun'].' Th, '.$row->mkk['bulan'].' Bulan'?></td>
													<td><?=$row->mkg['tahun'].' Th, '.$row->mkg['bulan'].' Bulan'?></td>

													<?php 
													$pensiun=$this->Crud->read(array('tabel'=>'db_aturpensiun','select'=>'tahun','order'=>array('kolom'=>'tahun','orderby'=>'ASC'),'where'=>array(array('jenis'=>'dosen'))))->result();
													foreach ($pensiun as $dt) {
														$y = ((($row->umur['tahun']*12)+$row->umur['bulan']) - ($dt->tahun*12));
														if ($y<0) {
															$x = "<label class='label label-success'>".str_replace('-', 'min ', $y)." bulan</label>";
														}elseif ($y==0) {
															$x = "<label class='label label-primary'>".str_replace('-', '', $y)."</label>";
														}elseif ($y>0) {
															$x = "<label class='label label-danger'>over ".$y." bulan</label>";
														}
														?><td><?=$x?></td><?php
													}
													?>

												</tr>
												<?php $i++;endforeach;?>
											</tbody>
										</table>								
									</div>
								</div>
								<div role="tabpanel" class="tab-pane fade" id="kependidikan">
									<div class="table-responsive">
										<table class="table table-bordered table-striped table-hover tabelsimple" style="width:100%">
											<thead>
												<tr>
													<th width="5%">No</th>
													<th width="20%">Nama</th>
													<th width="10%">Lahir</th>
													<th width="10%">Umur</th>
													<th width="10%">Masuk</th>
													<th width="10%">Tetap</th>
													<th width="10%">MKK</th>	
													<th width="10%">MKG</th>
													<?php $pensiun=$this->Crud->read(array('tabel'=>'db_aturpensiun','select'=>'nama,tahun','order'=>array('kolom'=>'tahun','orderby'=>'ASC'),'where'=>array(array('jenis'=>'tendik'))))->result();
													foreach ($pensiun as $dt) { ?> <th width="10%"><?=$dt->nama?><b><br>(<?=$dt->tahun?> Th)</b></th> <?php } ?>
												</tr>
											</thead>
											<tbody>
												<?php $i=1;foreach($data_tendik AS $row):?>
												<tr>
													<td><?=$i?></td>
													<td><?=$row->gelardepan.' '.ucwords($row->nama).' '.$row->gelarbelakang?><br><small class="label label-info"><?= !$row->statuspegawai ? 'Aktif':$row->statuspegawai ?></small></td>
													<td><?= date('d-m-Y',strtotime($row->tanggallahir))?></td>
													<td><?=$row->umur['tahun'].', '.$row->umur['bulan'].' Bulan'?></td>
													<td><?= date('d-m-Y',strtotime($row->awalmasuk))?></td>
													<td><?= date('d-m-Y',strtotime($row->awaltetap))?></td>
													<td><?=$row->mkk['tahun'].' Th, '.$row->mkk['bulan'].' Bulan'?></td>
													<td><?=$row->mkg['tahun'].' Th, '.$row->mkg['bulan'].' Bulan'?></td>

													<?php 
													$pensiun=$this->Crud->read(array('tabel'=>'db_aturpensiun','select'=>'tahun','order'=>array('kolom'=>'tahun','orderby'=>'ASC'),'where'=>array(array('jenis'=>'tendik'))))->result();
													foreach ($pensiun as $dt) {
														$y = ((($row->umur['tahun']*12)+$row->umur['bulan']) - ($dt->tahun*12));
														if ($y<0) {
															$x = "<label class='label label-success'>".str_replace('-', 'min ', $y)." bulan</label>";
														}elseif ($y==0) {
															$x = "<label class='label label-primary'>".str_replace('-', '', $y)."</label>";
														}elseif ($y>0) {
															$x = "<label class='label label-danger'>over ".$y." bulan</label>";
														}
														?><td><?=$x?></td><?php
													}
													?>

												</tr>
												<?php $i++;endforeach;?>
											</tbody>
										</table>								
									</div>
								</div>
						 
						</div>
					</div>
				</div>
			</div>		
		</div>		
	</div>			
</section>