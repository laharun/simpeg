<div class="modal-dialog">  
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title">Detail Serdos</h1>
      </div>
      <table class="table table-bordered table-hover">
        <tr>
          <th>No</th>
          <th>Tahun diperoleh</th>
          <th>File</th>
        </tr>
        <?php
        $no=1;
        foreach ($serdos as $dt) {
          ?>
          <tr>
            <td><?=$no++?></td>
            <td><?=$dt->serdos_tahundiperoleh?></td>
            <td><a href='<?= site_url('serdosadmin/downloadfile/'.$dt->serdos_file)?>' class='btn btn-xs btn-primary waves-effect'><i class='material-icons'>archive</i></a> </td>
          </tr>
          <?php
        }
        ?>
      </table>
    </div>
  </div> 