<div class="col-sm-12">
	<div class="card" id="edit_card" style="display:none">
		<div class="header bg-orange">
            <h2>
              Edit Data
            </h2>
            <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <!---->
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);" class="tutup_card">Tutup</a></li>
                        </ul>
                    </li>
             </ul>                        				
		</div>				
		<div class="body" id="edit_form">
			<!--AJAX LOAD HERE-->
		</div>
	</div>				
</div>