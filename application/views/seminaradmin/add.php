<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="card forminput" style="display:none">
        <div class="header bg-light-blue">
            <h2>
                Form Sertifikasi Dosen
            </h2>
        </div>		
		<div class="body">
			<form  action="<?php echo base_url('Seminaradmin')?>" method="POST" enctype="multipart/form-data">
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="form-group">
							<label>Pegawai</label>
							<div class="form-line">
								<select name="seminar_idpeg" data-size="10" data-live-search="true" class="form-control">
									<?php foreach($pegawai AS $row):?>
										<option value="<?=$row->idpeg?>"><?= ucwords($row->nama)?></option>
									<?php endforeach;?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label>Jenis</label>
							<div class="form-line">
								<select class="form-control" name="seminar_jenis" data-size="10" data-live-search="true">
									<option>Seminar</option>
									<option>Pelatihan</option>
									<option>Studi Banding</option>
									<option>Workshop</option>
								</select>
							</div>
						</div>								
                        <div class="form-group">
                        	<label class="form-label">Nama Kegiatan</label>
                        	<div class="form-line">
                        		<input type="text" name="seminar_nama" class="form-control">
                        	</div>
                        </div>
                        <div class="form-group">
                        	<label class="form-label">Tanggal Kegiatan</label>
                        	<div class="form-line">
                        		<input type="text" name="seminar_tglkegiatan" class="form-control datepicker">
                        	</div>
                        </div>                        
                       <div class="form-group">
                        	<label class="form-label">Keikutsertaan</label>
                        	<div class="form-line">
								<select class="form-control" name="seminar_keikutsertaan" data-size="10" data-live-search="true">
									<option>Peserta</option>
									<option>Pembicara</option>
								</select>                        		
                        	</div>
                        </div>                         
                       <div class="form-group">
                        	<label class="form-label">Lembaga/Penyelenggara</label>
                        	<div class="form-line">

                        		<textarea type="text" name="seminar_lembaga" class="form-control" rows="4"></textarea>
                        	</div>
                        </div>
                        <div class="form-group">
                        	<label>Upload File</label>
                        	<input required type="file" name="fileupload">
                        	<p class="help-block">Format file pdf, ukuran max 5mb</p> 
                        	<p class="col-red">File dapat berupa sertifikat atau surat tugas</p>                          
                        </div>                                                                              										
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-12">
						<button type="submit" value="serdos" name="serdos" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
						<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
					</div>
				</div>							 	
			</form>
		</div>
	</div>
</div>