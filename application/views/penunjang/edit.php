<form id="formpenunjang" action="<?php echo base_url('Penunjang/update')?>" method="POST" enctype="multipart/form-data">
	<h2 class="card-inside-title">Form Data Penunjang</h2>
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" name="id" value="<?= $penunjang->id?>">
					<label class="form-label">Id</label>
				</div>
			</div>		
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" name="idpeg" value="<?= $penunjang->idpeg?>">
					<label class="form-label">Id Pegawai</label>
				</div>
			</div>								
			<div class="form-group">
                <p>
                    <b>Tahun Akademik</b>
                </p>
                <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                	<?php
                		foreach ($thnakademik as $value) {
                			echo "<option value='".$value->tahun."'";
                				if($penunjang->thnakademik==$value->tahun){echo "selected";}
                			echo ">".$value->tahun."</option>";
                		}
                	?>
                </select>

			</div>
			<div class="form-group form-float">
				<div class="form-line">
					<textarea rows="4" class="form-control" name="kegiatan" required><?= $penunjang->namakeg?></textarea>
					<label class="form-label">Nama Kegiatan</label>
				</div>
			</div>	                            		
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input required name="tglmulai" type="text" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($penunjang->tglmulai))?>"> 
        		</div>
        	</div>	
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input required name="tglselesai" type="text" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($penunjang->tglselesai))?>">
        		</div>
        	</div>
        	<div class="form-group form-float hide">
        		<div class="form-line">
        			<input required name="filelama" type="text" class="form-control" value="<?= $penunjang->file?>">
        			<label class="form-label">File Lama</label>
        		</div>
        	</div>        	
			<h2 class="card-inside-title">Upload </h2>
			<div class="form-group form-float">
				<div class="">
					<input type="file" name="fileupload">
					<p class="help-block col-red">Ukuran Maksimal 5mb</p>
				</div>
			</div>									
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">Update</button>
			<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>							 	
</form>