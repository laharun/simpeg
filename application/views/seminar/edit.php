<form  enctype="multipart/form-data" action="<?php echo site_url('Seminar/update_data')?>" method="POST" >
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" name="id" value="<?= $seminar->id?>">
					<label class="form-label">Id</label>
				</div>
			</div>		
			<div class="form-group form-float">
				<div class="form-line">
					<input type="text" readonly class="form-control" name="idpeg" value="<?= $seminar->idpeg ?>">
					<label class="form-label">Id Pegawai</label>
				</div>
			</div> 								
			<div class="form-group">
                <p>
                    <b>Tahun Akademik</b>
                </p>
                <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                	<?php
                		foreach ($thnakademik as $value) {
                			echo "<option value='".$value->tahun."'";
                                if($seminar->tahunakademik==$value->tahun){echo "selected";}
                            echo ">".$value->tahun."</option>";
                		}
                	?>
                </select>
			</div>
            <div class="form-group">
                <b class="text-danger">Jenis</b>
                <select  name="jenis" class="form-control show-tick" data-size="5" data-live-search="true" title="Jenis Seminar">
                    <option value="seminar hasil penelitian" <?= trim($seminar->jenis)=='seminar hasil penelitian' ? 'selected':''?> >Seminar Hasil Penelitian</option>
                    <option value="buku luaran abdimas" <?= trim($seminar->jenis)=='buku hasil pengabdian' ? 'selected':''?>>Seminar Hasil Pengabdian</option>
                </select>
            </div> 
            <div class="form-group">
                <b class="text-danger">Status</b>
                <select  name="status" class="form-control show-tick" data-size="5" data-live-search="true" title="Jenis Seminar">
                    <option value="nasional" <?= strtolower($seminar->status)=='nasional' ? 'selected':''?>>Nasional</option>
                    <option value="internasional" <?= strtolower($seminar->status)=='internasional' ? 'selected':''?>>Internasional</option>
                </select>
            </div>                         
			<div class="form-group form-float">
				<div class="form-line">
					<textarea rows="4" class="form-control" name="seminar" required><?= $seminar->namaseminar?></textarea>
					<label class="form-label">Nama Seminar</label>
				</div>
			</div>
            <div class="form-group form-float">
                <div class="form-line">
                    <input type="text"  class="form-control" name="isbn" value="<?= $seminar->isbn?>">
                    <label class="form-label">ISBN</label>
                </div>
            </div>            
            <div class="form-group form-float">
                <div class="form-line">
                    <textarea rows="4" class="form-control" name="makalah" required ><?= ucwords($seminar->judulmakalah)?></textarea>
                    <label class="form-label">Judul Makalah</label>
                </div>
            </div>            
        	<div class="form-group">
                <p>
                    <b>Peran</b>
                </p>
                <select class="form-control show-tick" data-live-search="true" title="Pilih Peran" name="peran" required>
                    <option value="Ketua" <?php if(trim(strtolower($seminar->peran))=='ketua'){echo "selected";}?>>Ketua</option>
                    <option value="Anggota" <?php if(trim(strtolower($seminar->peran))=='anggota'){echo "selected";}?>>Anggota</option>
                </select>	
        	</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input type="text" class="form-control" name="tim" value="<?= $seminar->jumlahtim?>" required>
        			<label class="form-label">Jumlah TIM</label>
        		</div>
        		<p class="help-block col-red">Jika Peran Ketua isikan 0</p>
        	</div>
            <div class="form-group form-float">
                <div class="form-line">
                    <textarea rows="4" class="form-control" required name="penyelenggara"><?= $seminar->penyelenggara?></textarea>
                    <label class="form-label">Penyelenggara</label>
                </div>
            </div>
            <div class="form-group form-float">
                <div class="form-line">                                         
                    <input type="text" class="form-control datepicker" required name="tgl" value="<?= date('d-m-Y',strtotime($seminar->tglpelaksana))?>" placeholder="Tanggal kegiatan">
                    <label class="form-label">Penyelenggara</label>
                </div>
            </div>                                  
            <div class="form-group form-float">
                <div class="form-line">
                    <textarea class="form-control" rows="4" name="keterangan"><?= $seminar->keterangan?></textarea>
                    <label class="form-label">Keterangan Tambahan</label>
                </div>
            </div>
            <div class="form-group form-float hide">
                <div class="form-line">                                         
                    <input type="text" class="form-control datepicker" required name="filelama" value="<?= $seminar->file?>" placeholder="Tanggal kegiatan">
                    <label class="form-label">File lama</label>
                </div>
            </div>             	                            		                            		                            		                            	
			<div class="form-group form-float">
                <p>Upload</p>
				<div class="">
					<input type="file" name="fileupload">
					<p class="help-block col-red">Ukuran Maksimal 5mb,format PDF</p>
				</div>
			</div>																												
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-warning btn-lg waves-effect btn-block">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
        $("#edit_card").hide();
        $("#tampildata").show();
    })
</script>