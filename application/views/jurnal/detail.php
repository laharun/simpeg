<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="defaultModalLabel">Detail Jurnal</h4>
        </div>
        <div class="modal-body">
            <table class="table table-bordered table-striped" width="100%">
                <tr>
                    <th width="20%">Tahun Akademk</th>
                    <td width="80%"><?= ucwords($data->thakademik)?></td>
                </tr>
                <tr>
                    <th >Jenis</th>
                    <td ><?= ucwords($data->jenisjurnal)?></td>
                </tr> 
                <tr>
                    <th >Status</th>
                    <td ><?= ucwords($data->status)?></td>
                </tr>                                                 
                <tr>
                    <th >Jurnal</th>
                    <td ><?= ucwords($data->namajurnal)?></td>
                </tr>  
                <tr>
                    <th>Makalah</th>
                    <td><?= ucwords($data->judmakalah)?></td>
                </tr>
                <tr>
                    <th>Lembaga</th>
                    <td><?= ucwords($data->lembaga)?></td>
                </tr>  
                <tr>
                    <th>ISSN</th>
                    <td><?= ucwords($data->issn)?></td>
                </tr>                
                <tr>
                    <th>Tanggal Terbit</th>
                    <td><?= date('d-M-Y',strtotime($data->tglterbit))?></td>
                </tr>
                <tr>
                    <th>Peran</th>
                    <td><?= ucwords($data->peran)?></td>
                </tr>
                <tr>
                    <th>Tim</th>
                    <td><?= ucwords($data->jmltim)?></td>
                </tr>                                                                                                                                                                          
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info waves-effect btn-block" data-dismiss="modal">Tutup</button>
        </div>
    </div>
</div>  