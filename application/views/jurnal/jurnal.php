<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>JURNAL</h2>
		</div>
		<!--KONFIRMASI AKSI-->
		<?php
			if($this->session->flashdata('success')){
				echo'
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("success").'
                    </div>
				';
			}elseif($this->session->flashdata('error')){
				echo'
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$this->session->flashdata("error").'
                    </div>
				';				
			}	
		?>
		<!--FORM TAMBAH DATA-->
		<div class="row clearfix formtambah">
			<div class="col-sm-3">
				<div class="form-group">
					<button type="button" class="tomboltambah btn btn-primary btn-lg m-t-15 waves-effect btn-block">Tambah</button>
				</div>
			</div>
		</div>		
		<!--TAMBAH DATA FORM-->		
		<div class="row clearfix forminput" style="display:none">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="body">
						<form id="formjurnal" action="<?php echo base_url('Jurnal')?>" method="POST" enctype="multipart/form-data">
							<h2 class="card-inside-title">Form Isian Jurnal</h2>
							<div class="row clearfix">
								<div class="col-sm-12">
									<div class="form-group form-float hide">
										<div class="form-line">
											<input type="text" class="form-control" name="idpeg" value="<?php echo $this->session->userdata('id_pegawai')?>">
											<label class="form-label">Id Pegawai</label>
										</div>
									</div>								
									<div class="form-group">
	                                    <p>
	                                        <b class="text-danger">Tahun Akademik</b>
	                                    </p>
	                                    <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
	                                    	<?php
	                                    		foreach ($thnakademik as $value) {
	                                    			echo "<option>".$value->tahun."</option>";
	                                    		}
	                                    	?>
	                                    </select>
									</div>
									<div class="form-group">
	                                    <b class="text-danger">Status</b>
	                                    <select required name="status" class="form-control show-tick" data-size="5" data-live-search="true" title="Status">
	                                    	<option value="nasional tidak terakreditasi">Nasional tidak terakreditasi</option>
	                                    	<option value="nasional terakreditasi">Nasional terakreditasi</option>
	                                    	<option value="nasional berreputasi">Nasional berreuptasi</option>
	                                    	<option value="internasional">Internasional</option>
	                                    </select>
									</div>									
	                            	<div class="form-group">
	                            		<b class="text-danger">Jenis Jurnal</b>
	                                    <select required class="form-control show-tick" data-live-search="true" title="Pilih Jenis Jurnal" name="jenisjurnal" required>
	                                        <option value="jurnal penelitian">Jurnal Penelitian</option>
	                                        <option value="jurnal abdimas">Jurnal Abdimas</option>
	                                    </select>	
	                            	</div>									
									<div class="form-group form-float">
										<div class="form-line">
											<textarea required rows="4" class="form-control" name="jurnal"></textarea>
											<label class="form-label">Nama Jurnal</label>
										</div>
									</div>	                            		
									<div class="form-group form-float">
										<div class="form-line">
											<textarea required rows="4" class="form-control" name="makalah"></textarea>
											<label class="form-label">Judul Makalah</label>
										</div>
									</div>
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input required type="text" class="form-control" name="issn">
	                            			<label class="form-label">ISSN/Volume/Edisi</label>
	                            		</div>
	                            	</div>
	                            	<div class="form-group">
	                            		<div class="form-line">
	                            			<input required type="text" class="form-control datepicker" placeholder="Tanggal Terbit" name="tglterbit">
	                            		</div>
	                            	</div>	
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<textarea required rows="4" class="form-control" name="lembaga"></textarea>
	                            			<label class="form-label">Lembaga</label>
	                            		</div>
	                            	</div>	                            	                            										
	                            	<div class="form-group">
	                                    <p>
	                                        <b>Peran</b>
	                                    </p>
	                                    <select required class="form-control show-tick" data-live-search="true" title="Pilih Peran" name="peran" required>
	                                        <option value="Ketua">Ketua</option>
	                                        <option value="Anggota">Anggota</option>
	                                    </select>	
	                            	</div>
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<input required value="0" type="text" class="form-control" name="jmltim">
	                            			<label class="form-label">Jumlah TIM</label>
	                            		</div>
	                            		<p class="help-info col-red">Jika Peran Sebagai Ketua Isikan 0</p>
	                            	</div>								
	                            	<div class="form-group form-float">
	                            		<div class="form-line">
	                            			<textarea class="form-control" rows="4" name="keterangan"></textarea>
	                            			<label class="form-label">Keterangan Tambahan</label>
	                            		</div>
	                            	</div>	                            	                            		                            		                            		                            	
									<h2 class="card-inside-title">Upload</h2>
									<div class="form-group form-float">
										<div class="">
											<input required type="file" name="fileupload" >
											<p class="help-block col-red">Ukuran Maksimal 5mb, format PDF</p>
										</div>
									</div>									
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">SIMPAN</button>
									<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
								</div>
							</div>							 	
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--EDIT FORM-->
		<div class="row clearfix">
			<div class="col-sm-12">
				<div class="card" id="edit_card" style="display:none">
					<div class="header bg-orange">
		                <h2>
                          Edit Data
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!---->
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class="tutup_card">Tutup</a></li>
                                    </ul>
                                </li>
                         </ul>                        				
					</div>				
					<div class="body" id="edit_form">
						<!--AJAX LOAD HERE-->
					</div>
				</div>				
			</div>
		</div>		
		<!--TABEL DATA-->
		<div class="row clearfix" id="tampildata">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card" >
					<div class="header">
		                <h2>
                           Daftar Penulisan Jurnal
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <!--
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                    -->
                                </li>
                         </ul>				
					</div>
					<div class="body">
						<div class="row clearfix">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-hover tabelsimple " style="width:100%">
										<thead>
											<tr>
												<th width="5%">No</th>
												<th width="5%">Tahun</th>
												<th width="30%">Jurnal</th>
												<th width="35%">Makalah</th>
												<th class="text-center" width="15%">Aksi</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i=1;	
											foreach ($jurnal as $val) {
												echo "<tr>";
													echo "<td>".$i."</td>";
													echo "<td>".$val->thakademik."</td>";
													echo "<td>".$val->namajurnal."</td>";
													echo "<td>".$val->judmakalah."</td>";
													echo "<td class='text-center'>
														<a href='#' url='".site_url('Jurnal/detail/')."' id='".$val->id."' style='width:30px' class='detail btn btn-xs btn-info waves-effect'><i class='material-icons'>label</i></a>
														<a href='".site_url('Jurnal/downloadfile/'.$val->file)."' style='width:30px' class='btn btn-xs btn-info waves-effect'><i class='material-icons'>file_download</i></a> 
														<a href='".site_url('Jurnal/hapus/'.$val->id)."' class='hapus btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> 
														<a href='#' id='".$val->id."' link='".site_url('Jurnal/edit_data')."' class='edit_data btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> 
													</td>";
												echo "</tr>";
												$i++;
											}
										?>
										</tbody>
									</table>
									<p>Keterangan :</p>
									<a href='#' style="width:30px; margin:2px;" class='btn btn-xs btn-info waves-effect'><i class='material-icons'>label</i></a> : Detail <br>
									<a href='#' style="width:30px; margin:2px;" class='btn btn-xs btn-info waves-effect'><i class='material-icons'>file_download</i></a> : Download File <br>
									<a href='#' style="width:30px; margin:2px" class='btn btn-xs btn-danger waves-effect'><i class='material-icons'>delete</i></a> : Tombol Hapus <br>
									<a href='#' style="width:30px;margin:2px" class='btn btn-xs btn-warning waves-effect'><i class='material-icons'>mode_edit</i></a> : Tombol Edit <br>								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
</section>
<div class="modal fade" id="detail" tabindex="-1" role="dialog">
</div>