<form id="formpenulisanbuku" action="<?php echo base_url('Penulisanbuku/update')?>" method="POST" enctype="multipart/form-data">
	<h2 class="card-inside-title">Edit Penulisan Buku</h2>
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" name="id" value="<?= $penulisanbuku->id?>">
					<label class="form-label">Id</label>
				</div>
			</div>		
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text" class="form-control" name="idpeg" value="<?= $penulisanbuku->idpeg?>">
					<label class="form-label">Id Pegawai</label>
				</div>
			</div>								
			<div class="form-group">
                <p>
                    <b>Tahun Akademik</b>
                </p>
                <select required name="thnakademik" class="form-control show-tick" data-size="5" data-live-search="true" title="Pilih Tahun Akademik">
                	<?php
                		foreach ($thnakademik as $value) {
                			echo "<option value='".$value->tahun."'";
                				if($penulisanbuku->thnakademik==$value->tahun){echo "selected";}
                			echo ">".$value->tahun."</option>";
                		}
                	?>
                </select>
			</div>
			<div class="form-group">
                <b class="text-danger">Jenis</b>
                <select required name="jenis" class="form-control show-tick" data-size="5" data-live-search="true" title="Jenis">
                	<option value="buku luaran penelitian" <?= strtolower($penulisanbuku->jenis)=='buku luaran penelitian' ? 'selected':''?>>Buku Luaran Penelitian</option>
                	<option value="buku luaran abdimas" <?= strtolower($penulisanbuku->jenis)=='buku luaran abdimas' ? 'selected':''?>>Buku Luaran Abdimas</option>
                	<option value="bahan ajar" <?= strtolower($penulisanbuku->jenis)=='buku ajar' ? 'selected':''?>>Bahan Ajar</option>
                </select>
			</div>			
			<div class="form-group form-float">
				<div class="form-line">
					<textarea required rows="4" class="form-control" name="judul"><?=ucwords($penulisanbuku->judulbuku)?></textarea>
					<label class="form-label">Judul Buku</label>
				</div>
			</div>	                            		
			<div class="form-group form-float">
				<div class="form-line">
					<textarea required rows="4" class="form-control" name="penerbit"><?= trim($penulisanbuku->penerbit)?>
					</textarea>
					<label class="form-label">Penerbit</label>
				</div>
			</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input required type="text" class="form-control" name="isbn" value="<?= $penulisanbuku->isbn?>">
        			<label class="form-label" >ISBN</label>
        		</div>
        	</div>
        	<div class="form-group form-float">
        		<div class="form-line">
        			<input required type="text" class="form-control datepicker" name="tglterbit" placeholder="Tanggal terbit" value="<?= date('d-m-Y',strtotime($penulisanbuku->tglterbit))?>">
        		</div>
        	</div>
        	<div class="form-group form-float hide">
        		<div class="form-line">
        			<input required type="text" class="form-control" name="filelama" value="<?= $penulisanbuku->file?>">
        			<label class="form-label" >File Lama</label>
        		</div>
        	</div>									
			<h2 class="card-inside-title">Upload </h2>
			<div class="form-group form-float">
				<div class="">
					<input type="file" name="fileupload">
					<p class="help-block col-red">Ukuran Maksimal 5mb, format PDF</p>
				</div>
			</div>									
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">Update</button>
			<button type="button" class="tutup_card btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>							 	
</form>
<script type="text/javascript">
	$(".tutup_card").click(function(){
        $("#edit_card").hide();
        $("#tampildata").show();
    })
</script>