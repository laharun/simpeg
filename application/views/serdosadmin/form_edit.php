<form  action="<?php echo base_url('Serdosadmin/update')?>" method="POST" enctype="multipart/form-data">
	<div class="row clearfix">
		<div class="col-sm-12">
			<div class="form-group form-float hide">
				<div class="form-line">
					<input type="text"  class="form-control" readonly name="id" value="<?= $data->serdos_id?>">
					<label class="form-label">Id</label>
				</div>
			</div>			
			<div class="form-group">
				<label>Pegawai</label>
				<div class="form-line">
					<select name="serdos_idpegawai" data-size="10" data-live-search="true" class="form-control">
						<?php foreach($pegawai AS $row):?>
							<option value="<?=$row->idpeg?>" <?= $row->idpeg==$data->serdos_idpegawai ? 'selected':''?>><?= ucwords($row->nama)?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>											
            <div class="form-group form-float">
            	<div class="form-line">
            	<input type="text" name="serdos_tahundiperoleh" class="form-control datepicker2" value="<?=$data->serdos_tahundiperoleh?>">
            	<label class="form-label">Tahun diperoleh</label>
            	</div>
            </div>
            <div class="form-group">
            	<label>Upload File</label>
            	<input type="file" name="fileupload">
            	<p class="help-block">Format file pdf, ukuran max 5mb</p>                           
            </div>                                                                              										
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-sm-12">
			<button type="submit" value="serdos" name="serdos" class="btn btn-warning btn-lg waves-effect btn-block">UPDATE</button>
			<button type="button" class="tutup_input btn btn-danger btn-lg waves-effect btn-block">Tutup</button>
		</div>
	</div>							 	
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker2').bootstrapMaterialDatePicker({
            format: "YYYY",
            //year:true,
            time:false,
            //date:true,
            //monthPicker:true
        });         
    })
</script>