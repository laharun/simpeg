<?php
	class mseminarpenelitian extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		function get_seminar_byidpeg($idpeg){
			$result=$this->db->query("SELECT * FROM db_seminar WHERE idpeg='$idpeg' ORDER BY tahunakademik DESC");
			return $result;
		}
		//------------------------------SIMPAN SEMINAR-----------------------------
		function simpan_seminar($data){
			return $this->db->insert('db_seminar',$data);
		}
		//------------------------------AMBIL NAMA FILE---------------------------
		function get_seminar_byid($id){
			$row=$this->db->query("SELECT * from db_seminar WHERE id='$id'");
			return $row;
		}
		//-----------------------------HAPUS SEMINAR------------------------------
		function hapus_seminar($id){
			$this->db->where('id',$id);
			if($this->db->delete("db_seminar")==true){
				return true;
			}else{
				return false;
			}
		}
		function update_seminar($id,$data){
			$this->db->where('id',$id);
			$query=$this->db->update('db_seminar',$data);
			if($query){
				return true;
			}else{
				return false;
			}
		}
	}
?>