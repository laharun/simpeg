<?php
	Class mpegawai extends CI_Model{
		function __construct(){
			parent::__construct();
		}
		//--------------------------GET DEPARTMENT NAME----------------------
		function get_departmen(){
			return $this->db->get('department');
		}
		//------------------------------GET GOLONGAN-----
		function get_golongan(){
			$this->db->order_by('id','ASC');
			return $this->db->get('db_golongan');
		}
		//------------------------------GET PASSWORD------------
		function get_password($idpeg){
			return $this->db->get_where('db_userdosen',array('idpeg'=>$idpeg));
		}
		//-----------------------------UPDATE PASSWORD----------
		function update_password($idpeg,$data){
			$this->db->where('idpeg',$idpeg);
			if($this->db->update('db_userdosen',$data)){
				return true;
			}else{
				return false;
			}
		}
		//-----------------------------UPDATE DATA PEGAWAI--------------------------
		function update_pegawai($id,$data){
			$this->db->where('idpeg',$id);
			if($this->db->update('db_pegawai',$data)){
				return true;
			}else{
				return false;
			}
		}
		//--------------------------------DETAIL PEGAWAI----------------------------
		function get_detail_pegawai($idpeg){
			$result=$this->db->query("SELECT * FROM db_pegawai WHERE idpeg='$idpeg'");
			return $result;
		}
		//--------------------------------DATA HOMEBASE-------------------------------
		// function get_homebase(){
		// 	$query=$this->db->query("SELECT * FROM db_homebase ORDER BY kode ASC");
		// 	return $query;
		// }
		function get_homebase(){
			$query=$this->db->query("SELECT * FROM prodi ORDER BY kode ASC");
			return $query;
		}
		//--------------------------------GET UPDATE-----------------------------------
		function update_homebase($data,$idpeg){
			$this->db->where('idpeg',$idpeg);
			$this->db->update('db_pegawai',$data);
			return true;
		}
		//--------------------------------DATA PEGAWAI---------------------------------
		function get_pegawai_bykategori($kategori){
			$this->db->where('kategori',$kategori);
			return $this->db->get('db_pegawai');
		}
		//--------------------------------DASHBOARD------------------------------------
		//
		//
		//-----------------------------------------------------------------------------
		function get_pegawai_dashboard(){
			//CATATAN!! PROGRAMER MALES NULIS KOLOM JADI PILIH *
			$query=$this->db->query("SELECT a.idpeg, 
									a.nik,
									a.nama,
									a.idjeniskelamin,
									a.email,
									a.kategori,
									b.jenjang,
									b.jurusan AS homebase
									FROM db_pegawai a LEFT JOIN prodi b ON a.homebase=b.kode");
			return $query;
		}
		function jum_pegawai_byhomebase(){
			return $this->db->query("SELECT COUNT(b.nama) AS jumlah,b.nama FROM db_pegawai a JOIN db_homebase b ON b.kode=a.homebase GROUP BY b.nama ORDER BY jumlah DESC" );
		}
		//---------------------------JUMLAH JENJANG PENDIDIKAN
		function jum_jenjangpendidikan(){
			return $this->db->query("SELECT COUNT(idpendidikantertinggi) AS jumlah,idpendidikantertinggi AS jenjang FROM db_pegawai GROUP BY idpendidikantertinggi ORDER BY jumlah DESC");
		}
		//-----------------------------EDIT PEGAWAI---------------------------------------
		function edit_pegawai($id){
			return $this->db->get_where('db_pegawai',array('md5(idpeg::text)'=>$id));
		}
		//-----------------------------GET DATA PEGAWAI-----------------------------------
		function get_data_pegawai(){
			return $this->db->get('db_pegawai');
		}
		//----------------------------GET DATA PEGAWAI BY ID------------------------------
		function get_data_pegawai_byid($id){
			$query=$this->db->query("SELECT a.noskmasuk,
									a.nosktetap,
									a.nik,
									a.nama,
									a.kategori,
									a.gajiawal,
									a.idstatuspeg,
									a.tanggallahir,
									a.tempatlahir,
									a.homebase,
									a.gelarbelakang,
									a.gelardepan,
									a.prodi,
									a.tahunprodi,
									a.idjeniskelamin,
									a.idpendidikantertinggi,
									a.nobpjs,
									a.nonpwp,
									a.fingerprintid,
									a.idagama,
									a.idgoldarah,
									a.idgolongan,
									a.awaltetap,
									a.jabatan,
									a.statusnikah,
									a.email,
									a.noktp,
									a.nokk,
									a.kodepos,
									a.alamatjalan,
									a.kota,
									a.nohp,
									a.filekk,
									a.filektp,
									a.fileskmasuk,
									a.filesktetap,
									b.nama AS homebase_jurusan,
									b.program AS homebase_program,
									a.foto FROM db_pegawai a 
									LEFT JOIN db_homebase b ON b.kode=a.homebase WHERE a.idpeg=$id");
			return $query;
		}
		//-------------------------------GET PASSWORD USER/PEGAWAI------------------
		function get_password_all(){
			$query=$this->db->query("SELECT a.id,b.nama,a.username,a.password,a.idpeg FROM db_userdosen a JOIN db_pegawai b ON b.idpeg=a.idpeg");
			return $query;
		}
		//-------------------------------GET PASSWORD USER/PEGAWAI BY IDPEG-------------
		function get_password_byidpeg($id){
			$query=$this->db->query("SELECT a.id,b.nama,a.username,a.password FROM db_userdosen a JOIN db_pegawai b ON b.idpeg=a.idpeg WHERE a.idpeg='$id'");
			return $query;
		}
		//-------------------------------SIMPAN PASSWORD----------------------------
		function simpan_password($data){
			if($this->db->insert('db_userdosen',$data)){
				return true;
			}else{
				return false;
			}
		}
	}
?>