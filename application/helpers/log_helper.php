<?php
function helper_log($tipe = "", $str = ""){
    $CI =& get_instance();
 
    if (strtolower($tipe) == "login"){
        $log_tipe   = 0;
    }
    elseif(strtolower($tipe) == "logout")
    {
        $log_tipe   = 1;
    }
    elseif(strtolower($tipe) == "add"){
        $log_tipe   = 2;
    }
    elseif(strtolower($tipe) == "edit"){
        $log_tipe  = 3;
    }
    else{
        $log_tipe  = 4;
    }
 
    // paramter
    $param['log_user']      = $CI->session->userdata('username');
    $param['date']          = date('d-m-Y');
    $param['log_tipe']      = $log_tipe;
    $param['log_desc']      = $str;
 
    //load model log
    //$CI->load->model('m_log');
 
    //save to database
    //$CI->m_log->save_log($param);
    $data ="Tanggal: ".$param['date']."/".trim($param['log_user'])." Kode: ".$param['log_tipe']." Desc: ".$param['log_desc'].";\n";
    if ( !write_file('./log.txt', $data,'a+'))
    {
        echo 'Unable to write the file';
    }
    else
    {
        echo 'File written!';
    }
}
?>